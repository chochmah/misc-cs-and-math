#include <iostream>
#include <vector>
#include <set>
#include <numeric>

std::vector<bool> natNumbers;
std::vector<int> primes;
int limit = 1000000000;


void CreatePrimeList(std::vector<int>& list, const int upperBound) {	
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true) {
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

std::vector<long long int> pfn;

void CreatePFN(long long int pNr, long long int count, long long int total)
{
	while (total * pow(primes[pNr],count)< limit) {
		//std::cout << primes[pNr] << " " <<  " count " << count << " "<< total *pow(primes[pNr],count) << "\n";
		pfn.push_back(total * (int)pow(primes[pNr],count));
		CreatePFN(pNr+1, 1, total * (int)pow(primes[pNr],count));
		count++;
	}
}


int main()
{	
	std::set<int> dist;
	int margin = 1000;

	CreatePrimeList(primes, limit+margin);	
	CreatePFN(0, 1, 1);

	int count = 0;

	std::cout << "# admissible nmbrs " << pfn.size() <<  "\n";
	for (int i=0;i<pfn.size();i++) {		
		for (size_t k = pfn[i]+2; k < limit+margin; k++) {		
			if (natNumbers[k]) {
				dist.insert((int)(k - pfn[i]));
				break;
			}
		}		
	}
	std::cout << "toal: " << std::accumulate(dist.begin(), dist.end(),0)<< "\n";

 	std::getchar();
}