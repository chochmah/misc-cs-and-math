// Euler Project Problem 57 Square Root Conv.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
::python

import math

count = 0
for l in range(1001) :
	a = 2
	b = 1
	for i in range(l) :
		tmp = a
		a = 2 * a + b
		b = tmp
	tmp = a
	a = a + b
	b = tmp
	if math.floor(math.log10(a)) - math.floor(math.log10(b)) > 0:
		count = count + 1
print(count)