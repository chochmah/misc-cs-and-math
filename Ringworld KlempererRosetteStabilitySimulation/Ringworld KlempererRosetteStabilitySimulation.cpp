// Ringworld KlempererRosetteStabilitySimulation.cpp 
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <regex>

#include "..\PIniReadWrite\PIniReadWrite.h"


struct vec2 {
public:
	double x, y;
};

struct BodyStruct {
public:
	double mass;
	vec2 pos, v;
};

void InitializeConstellation(std::vector<BodyStruct>& body) {
	uint32_t	bodyNum		= body.size();
	double		radius		= 1.496e11; // in m
	double		speed		= 3.0e4; // m/s

	for (auto &b : body) 
		b = { 5.972e24, // earth mass in kg		
		{ radius	* sin(360 / (double)bodyNum), radius	* cos(360 / (double)bodyNum) },
		{ speed		* cos(360 / (double)bodyNum), speed		* sin(360 / (double)bodyNum) }};	
}

int main() {
    std::cout << "Ringworld - Klemperer Rosette Stability Simulation\n"; 

	uint32_t bodyNum = 5;
	std::vector<BodyStruct> bodyInitial(bodyNum);
	
	InitializeConstellation(bodyInitial);

	std::vector<BodyStruct> body[2];
	body[0].resize(bodyNum);
	body[1].resize(bodyNum);	
	body[0] = bodyInitial;

	bool current = 0;

	do {
		for (auto &b : body[!current]) {
			for (auto &b2 : body[!current]) {
				vec2 gravCenter{ 0,0 };
				if (&b2 != &b) {
					//gravCenter += b2.pos;					
				}

			}
		}


		current = !current;
	} while (true);
}

