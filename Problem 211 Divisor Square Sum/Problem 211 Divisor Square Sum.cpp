#include "pch.h"
#include <iostream>
#include <vector>
#include <set>

typedef unsigned long long myInt;
std::vector<myInt> field;
const myInt fieldsize = 64000000;

int main() {
	myInt total = 0;
	field.resize(fieldsize);
	for (myInt i = 1; i < fieldsize; i++) 
		for (myInt l = i; l < fieldsize; l+=i) 
			field[l] += i * i;
	
	for (myInt l = 1; l < fieldsize; l++) {
		if ((myInt)sqrt(field[l])*(myInt)sqrt(field[l]) == field[l]) {
			std::cout << l << "	" << field[l] << "\n";
			total += l;
		}
	}

	std::cout << total << "\n";
}