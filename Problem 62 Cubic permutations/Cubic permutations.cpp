#include <iostream>
#include <sstream>

int permut(long long  int xc, long long  int yc)
{
	std::stringstream ss1,ss2;
	ss1 << xc;
	std::string xc_s = ss1.str();
	ss2 << yc;
	std::string yc_s = ss2.str();
	
	if (xc_s.length() != yc_s.length())
		return 2;
	
	for (int i = 0; i < 9; i++) {
		int c1 = 0;
		int c2 = 0;
		for (int k = 0; k < xc_s.length(); k++) {			
			if (xc_s[k] == 48+i) c1++;
			if (yc_s[k] == 48+i) c2++;
		}
		if (c1 != c2)
			return false;
	}
	return true;
}

int main() {
	std::cout.precision(12);

	long long int upper = 9999;
	long long int lower = 1000;

	for (long long int l = lower; l < upper; l++)
	{
		long long int x = l;
		long long int xc = pow(x, 3);		

		long long int count = 0;
		for (long long int i = l; i < upper; i++) {
			if (i != l)
			{
				if (permut(xc, pow(i, 3)) == 2)
					break;
				if (permut(xc, pow(i, 3)) == 1)				
					count++;				
			}
		}		
		
		if (count >= 4)
			std::cout << x << "^3 " << pow(x, 3) << " ";		
	}	
	std::cout << "done\n";
	getchar();
}