#include <iostream>
#include <vector>

std::vector<int> primes;
std::vector<bool> natNumbers;
int total = 0;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

void recursion(int nr, int pcount) {
	int i = pcount;
	while (nr >= primes[i]) {
		recursion(nr - primes[i], i);
		i++;
	}	
	if (nr == 0)
		total++;		
}

int main()
{
	CreatePrimeList(primes, 100);

	int nr = 0;
	do {
		nr++;
		total = 0;
		recursion(nr, 0);
	} while (total < 5000);
	std::cout << nr << "\n";
	
	getchar();
}