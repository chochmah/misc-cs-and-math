#include <iostream>

int main()
{
	// https://projecteuler.net/problem=6

	size_t limit = 100;
	long long int sum = 0;
	long long int square = 0;

	for (size_t i = 1; i <= limit; i++)
		sum += i * i;

	square = (long long)pow(limit*(limit + 1) / 2, 2);

	std::cout << "Answer is " << square - sum << "\n";

	getchar();
}
