/*
	Works but is slow due to tracking of the whole number!
*/

#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeIntegerClass.h"
#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeFloatClass.h"

#include <iostream>
#include <sstream>


void add(std::vector<uint8_t>& x, std::vector<uint8_t>& y) {
	uint8_t overdraft = 0;
	for (int i = 0; i < std::max(x.size(), y.size()); i++) {
		auto x_i = (i < x.size()) ? x[i] : 0;
		auto y_i = (i < y.size()) ? y[i] : 0;
		auto m = x_i + y_i + overdraft;
		if (m >= 10)
			overdraft = (m - (m % 10)) / 10;
		else
			overdraft = 0;
		if (i < x.size())
			x[i] = m % 10;
		else
			x.push_back(m % 10);
	}
	if (overdraft > 0)
		x.push_back(overdraft);
}

void print(std::vector<uint8_t>& x) {
	for (int i = x.size()-1; i >=0;i--)
		std::cout << (int)x[i];
	std::cout << "\n";
}

void Euler104_faster() {
	std::vector<uint8_t> x,y;
	x.push_back(1);
	y.push_back(1);
	
	for (int i = 3; i < 400000; i++) {
		if (i % 1000 == 0)
			std::cout << i << "	" <<"\n";

		auto buf = x;
		add(x,y);
		y = buf;
		//print(x);

		if (x.size() > 18)
		{
			uint64_t res2 = 0;

			for (size_t k = x.size() - 9; k < x.size(); k++) {
				if (x[k] == 1) res2 += 1;
				if (x[k] == 2) res2 += 10;
				if (x[k] == 3) res2 += 100;
				if (x[k] == 4) res2 += 1000;
				if (x[k] == 5) res2 += 10000;
				if (x[k] == 6) res2 += 100000;
				if (x[k] == 7) res2 += 1000000;
				if (x[k] == 8) res2 += 10000000;
				if (x[k] == 9) res2 += 100000000;
			}

			uint64_t res = 0;
			for (int k = 0; k < 9; k++) {
				if (x[k] == 1) res += 1;
				if (x[k] == 2) res += 10;
				if (x[k] == 3) res += 100;
				if (x[k] == 4) res += 1000;
				if (x[k] == 5) res += 10000;
				if (x[k] == 6) res += 100000;
				if (x[k] == 7) res += 1000000;
				if (x[k] == 8) res += 10000000;
				if (x[k] == 9) res += 100000000;
			}

			//std::cout << res2 <<"	"<<res <<"\n";

			if (res == 111111111 && res2 == 111111111)
			{
				std::cout << "found: " << i << "\n";
				getchar();
			}
		}
	}
}

void Euler104() {
	typedef intLarge<uint64_t> IntL;

	IntL::setDisplayDigits(200);

	IntL x = 1;
	IntL y = 1;

	for (int i = 3; i < 400000; i++) {		
		IntL buf = x;
		x = x + y;
		y = buf;
	
		
		std::stringstream ss;
		ss << x;
		std::string str = ss.str();

		if (i % 1000 == 0)
			std::cout << i << "	" << x.getBinaryDigits() << "\n";

		if (str.length() > 18)
		{
			uint64_t res2 = 0;			
			
			for (size_t k = str.size() - 9; k < str.size(); k++) {
				if (str[k] == '1') res2 += 1;
				if (str[k] == '2') res2 += 10;
				if (str[k] == '3') res2 += 100;
				if (str[k] == '4') res2 += 1000;
				if (str[k] == '5') res2 += 10000;
				if (str[k] == '6') res2 += 100000;
				if (str[k] == '7') res2 += 1000000;
				if (str[k] == '8') res2 += 10000000;
				if (str[k] == '9') res2 += 100000000;
			}

			uint64_t res = 0;
			for (int k = 0; k < 9; k++) {
				if (str[k] == '1') res += 1;
				if (str[k] == '2') res += 10;
				if (str[k] == '3') res += 100;
				if (str[k] == '4') res += 1000;
				if (str[k] == '5') res += 10000;
				if (str[k] == '6') res += 100000;
				if (str[k] == '7') res += 1000000;
				if (str[k] == '8') res += 10000000;
				if (str[k] == '9') res += 100000000;
			}

			if (res2 == 111111111 && res == 111111111)
			{
				std::cout << "found: " << i << "\n";
				getchar();
			}
		}
		
	}
}

//-----------------------------------------------------------------------------
int main() 
{
	const char *s = "Hello, World!";

	std::string my_str = s;
	transform(my_str.begin(), my_str.end(), my_str.begin(), ::tolower);

	std::cout << my_str << "\n";

	return 0;


	Euler104_faster();
	getchar();

	return 0;
}
