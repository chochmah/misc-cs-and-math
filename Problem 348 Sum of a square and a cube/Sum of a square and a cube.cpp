#include <iostream>
#include <string>
#include <map>

std::map<long long int, int> data;

bool checkPalin(long long unsigned int x) {
	std::string x_s = std::to_string(x);
	for (int i = 0; i < x_s.size() / 2; i++)
		if (x_s[i] != x_s[x_s.size() - i - 1])
			return false;
	return true;
}

int main() {
	for (long int i = 2; i < 50000; i++)
		for (long int l = 2; l < 1000; l++) {
			long long unsigned int nmbr = i*i + l*l*l;
			if (checkPalin(nmbr))
				data[nmbr]++;
		}

	long long int count = 0;
	int sum = 0;
	std::map<long long int, int>::iterator it = data.begin();
	while (it != data.end())
	{
		if (it->second == 4) {
			std::cout << it->first << " :: " << it->second << std::endl;
			if (count < 5)
				sum += it->first;
			count++;
		}
		it++;
	}

	std::cout << "\n" << sum << "\n";

	std::getchar();
}