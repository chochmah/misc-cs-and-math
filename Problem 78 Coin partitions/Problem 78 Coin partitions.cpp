#include "pch.h"
#include <iostream>
#include <vector>

#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeIntegerClass.h"

/*
	Avoid Big integer by only saving the last couple of numbers
	
*/


	typedef intLarge<uint64_t> IntL;

//typedef long long int lint;
	typedef IntL lint;

std::vector<lint> partitionNr(1E7);

lint GeneratePartitionNr(int n) {	
	if (partitionNr[n] != 0)
		return partitionNr[n];
	if (n == 0 || n == 1)
		return 1;	
	
	int range = 1+(sqrt(24 * n + 1) - 1) / 6;

	lint sum = 0;
	for (int k = -range; k <= +range; k++) {
		if (k != 0)
		{
			int m = n - k * (3 * k - 1) / 2;			
			if (m >= 0) {
			//	std::cout << " " << k * (3 * k - 1) / 2;
				sum = sum + (lint)pow(-1, k + 1) * GeneratePartitionNr(m);
			}
		}
	}	
	return sum;
}


int main()
{
	for (int i = 0; i < 1E7; i++) {
	
		if (i % 1000 == 0)
			std::cout << i << "\n";
		

		partitionNr[i] = (lint)0;		
		partitionNr[i] = GeneratePartitionNr(i) % 10000000;

		auto u = partitionNr[i].AbsoluteConvertToVectorDigits();

		if (u.size() >= 6 && (int)u[0] == 0 && (int)u[1] == 0 && (int)u[2] == 0 && (int)u[3] == 0 && (int)u[4] == 0 && (int)u[5] == 0)
		//if (partitionNr[i] == 0)
		{
			std::cout << "n = " << i << "	" << "	pNr: " << partitionNr[i] << "\n";
			break;
		}
	}
}



/*
	Works but is much, much too slow!

*/

/*
struct heapstruct {
	int coins;
	int groups;

};

struct coinpartition {
	std::vector<heapstruct> heap;

	void add(int group, int coin) {
		for (int i = 0; i < heap.size(); i++) {
			if (heap[i].coins == coin) {
				heap[i].groups += group;
				return;
			}
		}
		heap.push_back(heapstruct{ coin,group });
	}

	void addCoinToHeap(int h) {				
		int count = heap[h].coins + 1;
		for (int j = 0; j < heap.size(); j++) {			
			if (heap[j].coins == count) {
				heap[j].groups++;

				heap[h].groups--;
				if (heap[h].groups == 0)
					heap.erase(heap.begin() + h);

				return;
			}
		}
		if (heap[h].groups == 1 && heap[h].coins == count)
			heap[h].coins++;
		else {
			heap[h].groups--;
			if (heap[h].groups==0)
				heap.erase(heap.begin() + h);
			add(1, count);
		}
	}

};

struct coinpartitioning {
	std::vector<coinpartition> partition;	

	void AddPartition(coinpartition cp) {
		// check if partition already exists, if not add to partitions
		for (int i = 0; i < partition.size(); i++) {
			int found = 0;
			for (int l = 0; l < cp.heap.size(); l++) {
				for (int k = 0; k < partition[i].heap.size(); k++) {
					if (cp.heap[l].coins == partition[i].heap[k].coins && cp.heap[l].groups == partition[i].heap[k].groups) {
						found++;
						break;						
					}
				}
			}
			if (found == cp.heap.size())
				return;
		}

		// not found
		partition.push_back(cp);

	}

	void Print() {
		std::cout << "partitioning:\n";
		for (int i = 0; i < partition.size(); i++) {
			for (int l = 0; l < partition[i].heap.size(); l++) {
				std::cout << partition[i].heap[l].groups << "," << partition[i].heap[l].coins << "|";
			}			
			std::cout << "\n";
		}
	}
};


int main()
{
	heapstruct h{1,1};
	coinpartition p;
	p.heap.push_back(h);
	coinpartitioning Ping;
	Ping.partition.push_back(p);

	Ping.Print();

	for (int i = 2; i < 60; i++) {
		// add coin to existing partitioning;

		coinpartitioning newPing;

		// just add a single coin as a group to the partition, trivial case
		for (int l = 0; l < Ping.partition.size(); l++) {
			// iterate through all existing partitions
			coinpartition p2 = Ping.partition[l];
		
			p2.add(1, 1);
			newPing.partition.push_back(p2);
		}
		
		for (int l = 0; l < Ping.partition.size(); l++) {
		// iterate through all existing partitions		
			for (int k = 0; k < Ping.partition[l].heap.size(); k++) {
				// iterate through all heaps

				// add a coin to every single group
				// check if partition already exists in partitioning, if not add new group
				auto newPartition = Ping.partition[l];				
				newPartition.addCoinToHeap(k);
				
				newPing.AddPartition(newPartition);
			}

		}
		

		Ping = newPing;
		//Ping.Print();
		std::cout << i << "	" << Ping.partition.size() << "\n";
	}

}
*/