// Problem 203 Squarefree Binomial Coefficients.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <set>


typedef unsigned long long int mint ;
std::set<mint> h;

bool checkSquareFree(mint x) {
	for (mint i = 2; i <= sqrt(x); i++) {
		if (x % (i*i) == 0)
			return false;
	}
	return true;
}

int main()
{
	std::vector<std::vector<mint>> pascal;

	pascal.push_back(std::vector<mint>(1));
	pascal.push_back(std::vector<mint>{1, 1});

	h.insert(1);

	for (int i = 2; i < 51; i++) {
		std::vector<mint> newline;
		newline.push_back(1);
		for (int l = 0; l < i-1; l++) {			
			mint n = pascal[i - 1][l]+ pascal[i - 1][l+1];
			newline.push_back(n);
			if (checkSquareFree(n))
				h.insert(n);
		}
		newline.push_back(1);
		pascal.push_back(newline);
	}

	mint total = 0;
	for (auto e : h)		
		total += e;

	std::cout << total << "\n";
}