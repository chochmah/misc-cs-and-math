// Problem 40 Champernownes constant.cpp :
/*
	Idea: we just step through the long number 

*/
#include "pch.h"
#include <iostream>
#include <string> 

//-----------------------------------------------------------------------------
int main() {
	long int carrier	= 1;	
	long int nextTarget = 1;
	long int i			= 1;
	long int result		= 1;
	do {
		std::string number = std::to_string(i);				
		if (carrier <= nextTarget && nextTarget < carrier + number.length()) {			
			result *= int(number.at(nextTarget - carrier)) - 48;
			nextTarget *= 10;
		}
		carrier += number.length();		
		i++;		
	} while (nextTarget != 1E7);
    
	std::cout << result << "\n";
}
