#include <stdio.h>
#include <iostream>
#include <string>

// Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
// where each �_� is a single digit.

typedef unsigned long long myInt;

int main() {
	for (myInt x = 1010101010; x < 1920101010; x++)
	{	
		std::string str = std::to_string(x*x);
		//std::cout << str.length() << "\n";
		if (str[0] == '1' && str[2] == '2' && str[4] == '3' &&
				str[6] == '4' && str[8] == '5' && str[10] == '6' &&
				str[12] == '7' && str[14] == '8' && str[16] == '9' && str[18] == '0') 
		{
			std::cout << x*x << "	" << x << "\n";
			std::getchar();
			return 0;
		}
	}	
}