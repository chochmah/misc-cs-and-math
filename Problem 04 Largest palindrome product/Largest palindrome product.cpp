#include <iostream>

int main()
{
	/*
	std::cout <<
	"https://projecteuler.net/problem=4\n" <<
	"A palindromic number reads the same both ways.The largest palindrome\n" <<
	"made from the product of two 2 - digit numbers is 9009 = 91 � 99.\n" <<
	"Find the largest palindrome made from the product of two 3-digit numbers.\n\n";
	*/
	int large = 0;
	for (int a = 9; a >= 1; a--)
		for (int b = 9; b >= 0; b--)
			for (int c = 9; c >= 0; c--)
			{
				int n = a * 100000 + b * 10000 + c * 1000 + c * 100 + b * 10 + a;
				for (int i = 100; i < 999; i++)
					if (n % i == 0 && n / i < 1000 && n > large)
						large = n;
			}

	std::cout << "The largest palindrome made from the product of two 3-digit numbers is " << large << "\n\n";
	
	getchar();
}