#pragma once

#define NOTSET			0
#define FORSIZE			for (size_t i = 0; i < SIZE; i++) 
#define FORALLFIELDS	for (size_t x = 0; x < SIZE; x++) for (size_t  y = 0; y < SIZE; y++) 

#define FIELDSIZE		3
#define SIZE			FIELDSIZE * FIELDSIZE


typedef unsigned int VALUETYPE;

//-----------------------------------------------------------------------------

struct FieldStructType {
	VALUETYPE	value{ 0 };				// 0 = not set, 1..SIZE set 
	bool		possibilitys[SIZE + 1];		// 0 = possible, 1 = impossible
	size_t		openPossibilites{ SIZE };	// #num of possibilities left for value

											//-----------------------------------------------------------------------------
	void resetPossibilities() {
		openPossibilites = SIZE;
		FORSIZE possibilitys[i + 1] = true;
	}

	//-----------------------------------------------------------------------------
	void countPossibilities() {
		openPossibilites = SIZE;
		FORSIZE if (possibilitys[i + 1] == false) openPossibilites--;
	}
};


//-----------------------------------------------------------------------------
static size_t adv_mod(const size_t x, const size_t y) {
	size_t o2 = x % y;
	size_t r2 = x - o2;
	return (size_t)(r2 / y);
}


//-----------------------------------------------------------------------------
struct BoadStructType {
#define cfv board[x][y].value
#define cfp board[x][y].possibilitys
#define cfo board[x][y].openPossibilites

	FieldStructType		board[SIZE][SIZE];
	size_t				openValues;

	//-----------------------------------------------------------------------------
	void clear() {
		FORALLFIELDS{ cfv = 0; board[x][y].resetPossibilities(); }
	}

	//-----------------------------------------------------------------------------
	void set(const size_t x, const size_t y, const VALUETYPE value) {
		cfv = value;
	}

	//-----------------------------------------------------------------------------
	void calcOpenValues() {
		openValues = SIZE * SIZE;
		FORALLFIELDS if (cfv != NOTSET) openValues--;
	}

	//-----------------------------------------------------------------------------
	bool calcPossibilities()
	{
		FORALLFIELDS{
			if (cfv == NOTSET) {

				//check the row and column
				FORSIZE{
					if (board[i][y].value != NOTSET)
					cfp[board[i][y].value] = false;
				if (board[x][i].value != NOTSET)
					cfp[board[x][i].value] = false;
				}

					//check the quadrant
				size_t start_x = adv_mod(x, FIELDSIZE);
				size_t start_y = adv_mod(y, FIELDSIZE);
				for (auto i = start_x*FIELDSIZE; i < (start_x + 1) * (FIELDSIZE); i++)
					for (auto l = start_y*FIELDSIZE; l < (start_y + 1) * (FIELDSIZE); l++)
						if (board[i][l].value != NOTSET)
							cfp[board[i][l].value] = false;
			}

		board[x][y].countPossibilities();
		if (cfo == 0) return false;		// value not set and possibilities
		}
		return true;
	}

	//-----------------------------------------------------------------------------
	void pickPivot(size_t& pivotX, size_t& pivotY) const {
		size_t		minValue = SIZE;
		bool		erfolg{ false };

		FORALLFIELDS{
			if (cfo < minValue) {
				minValue = cfo;
				pivotX = x;
				pivotY = y;
				erfolg = true;
			}
		}

			if (!erfolg) {
				std::cout << "can't pick pivot!\n";
				exit(1);
			}
	}

	//-----------------------------------------------------------------------------
	void print() const {
		FORALLFIELDS{
			if (board[y][x].value != 0)
			std::cout << board[y][x].value << " ";
			else std::cout << "  ";
			if (y == SIZE - 1) std::cout << "\n";
		}
		std::cout << "\n";
	}

	//-----------------------------------------------------------------------------
	void printOpenPossibilities() const {
		FORALLFIELDS{
			if (board[y][x].value == NOTSET)
			std::cout << board[y][x].openPossibilites << " ";
			else std::cout << "  ";
			if (y == SIZE - 1) std::cout << "\n";
		}
		std::cout << "\n";
	}
};
