//	solves sudokus of arbitrary size

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <time.h>       /* time */

#include "board-struct.h"
#include "sudoku-problems.h"


struct BoadStructType;
std::vector<BoadStructType*> board_set[2];

bool cur_board_set{ 0 };


//-----------------------------------------------------------------------------
struct metricStruct {
	float time1 = 0.0, tstart;
} metrics;


//-----------------------------------------------------------------------------
void solve(BoadStructType& board, bool count_solutions)
{
	size_t boards_checked = 0;
	size_t maxSetSize = 0;
	size_t solution_count = 0;

	metrics.tstart = (float)clock();
	board.calcOpenValues();
	board_set[cur_board_set].push_back(&board);

	for (size_t qom = 0; qom < 1000; qom++) {
		for (size_t k = 0; k < board_set[cur_board_set].size(); k++) {

			if (!board_set[cur_board_set][k]->calcPossibilities())
				continue;

			size_t pivotX, pivotY;
			board_set[cur_board_set][k]->pickPivot(pivotX, pivotY);

			// create new boards for all possible solutions for the Pivot Element
			// and put them into the search-index
			for (int i = 0; i < SIZE; i++) {
				if (board_set[cur_board_set][k]->board[pivotX][pivotY].possibilitys[i + 1] == true) {
					BoadStructType* newBoard = new BoadStructType;
					*newBoard = *board_set[cur_board_set][k];
					newBoard->board[pivotX][pivotY].value = i + 1;
					newBoard->board[pivotX][pivotY].resetPossibilities();
					newBoard->calcOpenValues();

					if (newBoard->openValues == 0) {
						if (solution_count == 0) {
							metrics.time1 += clock() - metrics.tstart; metrics.time1 = metrics.time1 / CLOCKS_PER_SEC;
							newBoard->print();
							std::cout << "max setsize: " << maxSetSize << " boards checked: " << boards_checked << " time: " << metrics.time1 << "\n\n";
							if (!count_solutions) exit(0);
						}
						solution_count++;
					}
					else {
						board_set[!cur_board_set].push_back(newBoard);
						boards_checked++;
					}
				}
			}
		}

		// delete old boardset and swap
		for (std::vector<BoadStructType*>::iterator it = board_set[cur_board_set].begin(); it != board_set[cur_board_set].end(); ++it)
			delete (*it);
		board_set[cur_board_set].clear();
		cur_board_set = !cur_board_set;

		// sort the bords in the search index by open values (bubble)
		for (size_t i = 0; i < board_set[cur_board_set].size(); i++)
			for (size_t l = i + 1; l < board_set[cur_board_set].size(); l++)
				if (board_set[cur_board_set][l]->openValues < board_set[cur_board_set][i]->openValues)
					std::swap(board_set[l], board_set[i]);

		maxSetSize = std::max(maxSetSize, board_set[cur_board_set].size());
		if (maxSetSize > 10000) {
			std::cout << "too many potential solutions\n";
			exit(0);
		}
		//std::cout << "setsize: " << board_set[cur_board_set].size() << " openvalues: " << board_set[cur_board_set][0]->openValues << "\n";
	}

	if (!count_solutions)
		std::cout << "No Solution found\n";
	else
		std::cout << "solutions: " << solution_count << "\n\n";
}


//-----------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	BoadStructType *board = new BoadStructType;

	setInitialProblemExtreme(*board);
	board->print();

	solve(*board, true);

	return 0;
}

