#pragma once

#include "board-struct.h"

//-----------------------------------------------------------------------------
void setInitialProblemEasy(BoadStructType& board) {
	// http://puzzles.about.com/library/sudoku/blprsudokue19.htm

	board.clear();
	board.set(1, 0, 2);
	board.set(3, 0, 1);
	board.set(4, 0, 7);
	board.set(5, 0, 8);
	board.set(7, 0, 3);

	board.set(1, 1, 4);
	board.set(3, 1, 3);
	board.set(5, 1, 2);
	board.set(7, 1, 9);

	board.set(0, 2, 1);
	board.set(8, 2, 6);

	board.set(2, 3, 8);
	board.set(3, 3, 6);
	board.set(5, 3, 3);
	board.set(6, 3, 5);

	board.set(0, 4, 3);
	board.set(8, 4, 4);

	board.set(2, 5, 6);
	board.set(3, 5, 7);
	board.set(5, 5, 9);
	board.set(6, 5, 2);

	board.set(0, 6, 9);
	board.set(8, 6, 2);

	board.set(1, 7, 8);
	board.set(3, 7, 9);
	board.set(5, 7, 1);
	board.set(7, 7, 6);

	board.set(1, 8, 1);
	board.set(3, 8, 4);
	board.set(4, 8, 3);
	board.set(5, 8, 6);
	board.set(7, 8, 5);
}

void setInitialProblemHard(BoadStructType& board) {
	// http://www.7sudoku.com/view-puzzle?date=20160111

	board.clear();
	board.set(0, 0, 4);
	board.set(4, 0, 5);
	board.set(5, 0, 6);
	board.set(6, 0, 3);
	board.set(8, 0, 7);

	board.set(7, 1, 8);

	board.set(6, 2, 5);
	board.set(8, 2, 6);

	board.set(3, 3, 2);
	board.set(4, 3, 7);
	board.set(7, 3, 9);

	board.set(0, 4, 3);
	board.set(3, 4, 9);
	board.set(5, 4, 1);
	board.set(8, 4, 5);

	board.set(1, 5, 2);
	board.set(4, 5, 8);
	board.set(5, 5, 4);


	board.set(0, 6, 6);
	board.set(2, 6, 8);

	board.set(1, 7, 3);

	board.set(0, 8, 5);
	board.set(2, 8, 4);
	board.set(3, 8, 7);
	board.set(4, 8, 2);
	board.set(8, 8, 8);
}

void setInitialProblemExtreme(BoadStructType& board) {
	// http://www.extremesudoku.info/sudoku.html on Extreme

	board.clear();
	board.set(0, 0, 3);
	board.set(8, 0, 6);

	board.set(1, 1, 9);
	board.set(3, 1, 8);
	board.set(5, 1, 5);
	board.set(7, 1, 2);

	board.set(2, 2, 7);
	board.set(6, 2, 1);

	board.set(1, 3, 2);
	board.set(3, 3, 1);
	board.set(5, 3, 6);
	board.set(7, 3, 7);

	board.set(4, 4, 2);

	board.set(1, 5, 8);
	board.set(3, 5, 4);
	board.set(5, 5, 9);
	board.set(7, 5, 1);

	board.set(2, 6, 4);
	board.set(6, 6, 6);

	board.set(1, 7, 7);
	board.set(3, 7, 9);
	board.set(5, 7, 1);
	board.set(7, 7, 5);

	board.set(0, 8, 5);
	board.set(8, 8, 7);
}

void setInitialProblem4x4Sudoku(BoadStructType& board) {
	// http://1sudoku.net/play/sudoku-kids-free/sudoku-4x4/

	board.clear();
	board.set(3, 0, 3);

	board.set(1, 1, 1);
	board.set(2, 1, 4);

	board.set(1, 2, 2);
	board.set(2, 2, 3);

	board.set(0, 3, 1);
}
