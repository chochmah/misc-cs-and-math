#include <iostream>
#include <sstream>
#include <set>
#include <numeric>

long long int sumquad(long long int x) { return x * (x + 1)*(2 * x + 1) / 6; }

bool checkPalin(long long int x) {
	std::string x_s = std::to_string(x);
	for (int i = 0; i < x_s.size() / 2; i++)
		if (x_s[i] != x_s[x_s.size() - i - 1])
			return false;
	return true;
}

int main() {	
	std::set<unsigned  long long int> nmrs;
	const unsigned  long long int emax = 1E8;
	const unsigned  long long int imax = sqrt(emax) + 1;	

	for (int i = 0; i < imax; i++) {
		for (int l = i+2; l < imax; l++) {
			long long int x = sumquad(l) - sumquad(i);
			if (x < emax) {
				if (checkPalin(x)) {
					nmrs.insert(x);					
					std::cout << i+1 << "..." << l << "	=" << x <<  "\n";
				}
			}
			else
				break;
		}
	}

	unsigned long long int sum = 0;
	for (std::set<unsigned long long int>::iterator it = nmrs.begin(); it != nmrs.end(); ++it) {
		sum += *it;
	}
	std::cout << "Total: " << sum << "\n";
	std::getchar();
}