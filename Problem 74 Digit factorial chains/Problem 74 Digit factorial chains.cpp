#include "pch.h"
#include <iostream>
#include <string>

unsigned int fac(unsigned int n) {
	return (n == 1 || n == 0) ? 1 : n * fac(n - 1);
}

unsigned int fac2(unsigned int n) {
	unsigned int r = 1;
	for (int i = 1; i <= n; i++)
		r = r * i;
	return r;
}

unsigned int operate(unsigned int n) {
	unsigned int res=0;
	std::string s = std::to_string(n);
	for (int k = 0; k < s.length(); k++)
		res += fac2((int)s[k] - 48);
	return res;
}

unsigned int measureChain(unsigned int n) {
	unsigned int v[100];
	unsigned int counter = 1;
	v[0] = n;
	do {
		n = operate(n);
		for (int i=0;i<counter;i++)
			if (v[i] == n)
				return counter;			
		v[counter] = n;
		counter++;
	} while (true);
}

int main() {
	unsigned int count = 0;
	for (int i = 1; i < 1E6; i++)
		if (measureChain(i) == 60) count++;
	std::cout << count << "\n";
}
