#include "pch.h"
#include <iostream>
#include <vector>

const int m=100;
int counter = 0;

void rec(int n, int count)
{
	if (count > m)
		return;
	if (count == m)
		counter++;	
	for (int i = n; i > 0; i--)
		rec(i,count+i);
}

int main()
{
	rec(m, 0);
	std::cout << counter-1 << "<<<\n";
}