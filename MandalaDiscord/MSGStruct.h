#pragma once


#include "mstring.h"

//-----------------------------------------------------------------------------
typedef struct
{
	MString	nick;
	MString message;
	time_t	timestamp;
} MStruct;
