#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h> 
#include <crtdbg.h>  

#include "weiterlistclass.h"
#include "stackclass.h"
#include "userlistclass.h"
#include "ClientClass.h"

#include "MySQLAbstractionLayerClass.h"
#include "VariableStorageStruct.h"
#include "HighScoreListenClass.h"

#include "QuizStruct.h"
#include "structs.h"


time_t Startuptime;

bool gesilenct=0;
bool iniautokicknicksonsperrlist;
bool iniautoaddtosperrlistonspaming;
std::string iniftpwebpage;
std::string inibotName;
std::string inibotPass;
std::string inibotInitialChannel;
std::string iniSepName;
bool iniAddSepNumbers;
int inisqlPort;
bool inibotauto_quit;
std::string inisqlIP;
bool iniStartQuizOnExec;
bool inicomlern;
bool inicomlaudatio;
bool inicomsuche;
bool inicomzeigemeinefragen;
bool inicomvote;
bool inicomwunsch;
bool inicomcomment;
int inibotquizsize;
int inibotautoquittime;
int inifragenbisquizstopbeikeinerantwort;
bool keepalive;
bool zusammenhaengendefragen;
bool stringswitch=0;
int abcfragenhaeufigkeit;
int zwischenfragendelay;
MString letzteAntwort;


typedef struct
{
     std::string leadstring;
} textformatstruct;


//-----------------------------------------------------------------------------	
std::string tippstring() 
{ 
  stringswitch=!stringswitch;
  if (stringswitch) 
	  return(vs.get("TippStringOne"));
  else 
	  return(vs.get("TippStringTwo"));
}

//-----------------------------------------------------------------------------	
class fragestruct
{
public: 
	fragestruct() {}
	~fragestruct() {}
	fragestruct(MString Autor_, MString Frage_, MString Antwort_, int Time_, int Punkte_, int typ_, int Tippnum_, int ID_)
		: Autor(Autor_), Frage(Frage_), Antwort(Antwort_), Time(Time_), Punkte(Punkte_), typ(typ_), Tippnum(Tippnum_), ID(ID_)
	{}

	MString Autor;
	MString Frage;
	MString Antwort;
	int Time;
	int Punkte;
	int typ;
	int Tippnum;
	int ID;

	//-----------------------------------------------------------------------------
	static void SaveMCAntwort(MString antwort, int ID)
	{
		if (antwort == ("hm") || antwort == ("ka") || antwort.beginswith("!") || antwort.strlength() > 30 || antwort.beginswith("?")) return;
		antwort.erase("?");
		antwort.erase("*");
		antwort.erase("_");

		mySQL.sendQuery("insert into mcantworten (antwort,ID) VALUES('%s',%i)", antwort, ID);
	}
};

//-----------------------------------------------------------------------------	
typedef struct  
{
    int platz;
    MString name;
 } rlstruct;

//-----------------------------------------------------------------------------	
typedef struct
{
    int datum;
    MString frage;
    MString ID;

	void set(int datum_, MString frage_, MString ID_)
	{
		datum	= datum_;
		frage	= frage_;
		ID		= ID_;
	}		

}  datenordnenstruct;


//-----------------------------------------------------------------------------	

WeiterListClass weiterlist;
bool echtzeitrangliste=1;
int semaphore=0;
datenordnenstruct datenspeicher[3];


bool debug=0;
bool zeitautomatik=1;
MString LastAnswer;
int folgefragen = 0;


int zu[300];        //F�r die Tipps


bool Zahlenraten;
int zeitfuerrunde = 0;
unsigned long timer = 0;
void (*pfi) (messagestruct ms);
void (*pfiTEST) (messagestruct ms);

bool userinteraktion=1;
textformatstruct textformat;

UserListClass userbuffer;

int rangliste[maxspieler];	/*zuweisungstafel fuer aktuelle Rangliste */


int ids;			/* holds thread args */
int ids2;
int errcode;			/* holds pthread error code */
int fc;


int rl1platz[200];
int rl2platz[200];   
MString rl1name[200];
MString rl2name[200];   




float sqr(float x) {return(x*x);}
  

void WriteEwigeListeHTML ()
{
}

void SetFragenAttrib () 
{
}

std::string iniftpServer;
std::string iniftpName;
std::string iniftpPass;
std::string iniftpDir;

int vetonum=0;
int waitnum=0;
MString veto[maxspieler];
MString wait[maxspieler];

bool warteaufnachzuegler;
int warteaufnachzueglertimer;
MString warteaufnachzueglerersterspieler;
int warteaufnachzueglerdelay;

std::string themenschlagwort;


int FragenproRunde = 20;
fragestruct AFrage;
  
int gamespeed=1000;
float sleep(int s) { Sleep(s*gamespeed); return 0; }
ChatBotClass ChatBot;
ChatBotClass ChatBotTEST;
StackClass stack(&ChatBot);


//-----------------------------------------------------------------------------
void Write2Log(char *name,char *text) 
{
	time_t rawtime;
	time ( &rawtime );
	FILE *fd2;
	fd2 = fopen (name, "a+");
	//  fprintf (fd2,ctime (&rawtime));
	std::fprintf (fd2,text);
	fprintf (fd2,"\n");
	fclose(fd2);	
}

//-----------------------------------------------------------------------------
void Write2Log(std::string name, std::string text) 
{
	time_t rawtime;
	time(&rawtime);
	FILE *fd2;
	fd2 = fopen(name.c_str(), "a+");
	//  fprintf (fd2,ctime (&rawtime));
	fprintf(fd2, text.c_str());
	fprintf(fd2, "\n");
	fclose(fd2);
}

//-----------------------------------------------------------------------------
//implementiert am 6.Mai.2005 22:51

void write2log(std::string name, int id, int time) 
{	
	mySQL.sendQuery("insert into log (name,frageID,datum,zeit) Values ('%s',%i,%i,%i);",name.c_str(),id,0,time);  
}

//-----------------------------------------------------------------------------
void voteSQL(MString senderX, MString Message) 
{	
	MString x = Message;
	x.concat ("END");
	MString z = x.getbedfirst (vs.get("CMDVote ").c_str(), "END");
	
	mySQL.sendQuery("insert into revision (id, user, typ) Values ('%s','%s',%i);", z, senderX, 1);
}

//-----------------------------------------------------------------------------
void setveto(MString Sender) 
{     
     if (quizStruct.delayTimer<=570) 
	 {
        for (int i = 0; i < vetonum; i++) if ((Sender==veto[i])) goto ok;				
        veto[vetonum] = Sender;
        vetonum++;
		quizStruct.delayTimer+=120;
		if (quizStruct.delayTimer>570) 
			quizStruct.delayTimer=570;
		
		MString p;
        int h=rand()%5;
        if (h == 0) p="hat ein Veto eingelegt,";
        if (h == 1) p="sitzt wie auf hei�en Kohlen,";
        if (h == 2) p="hat heute noch was anderes vor,";
		if (h == 3) p = "bittet darum in die Puschen zu kommen,";
        if (h == 4) p="laesst man nicht warten,";                        

		botSay(vs.get("WaitTimeRemaining"), Sender.stt.c_str(), p.stt.c_str(), 60 - (int)(quizStruct.delayTimer / 10));        
     }
     ok:
     printf("");
}

//-----------------------------------------------------------------------------
void setwait(MString Sender) 
{
     MString p;     
     if (quizStruct.delayTimer<=570) {
        for (int i = 0; i < waitnum; i++) if ((Sender==wait[i])) goto ok;				
        wait[waitnum] = Sender;
        waitnum++;
		quizStruct.delayTimer-=200;
		p= "/me ";
		p+=Sender;
        p+=" ";
        int h=rand()%5;
        if (h==0) p+="hat ein Wait eingelegt,";
        if (h==1) p+="ist heute eher von der gemuetlichen Sorte,";
        if (h==2) p+="rollt schnell ne Zigarette,";
        if (h==3) p+="geht kurz Kippen holen,";                        
        if (h==4) p+="geht hier alles viel zu schnell,";                        
        p+=" Restzeit ";
        p+=60-(int)(quizStruct.delayTimer/10);
		p+=" Sekunden";            
		botSay(p.stt.c_str());          
     }
     ok:
     printf("");
}

bool checkOP (std::string Sender) 
{      
	mySQL_RES res = mySQL.sendQuery("select * from operatorenliste where operator='%s';", Sender.c_str());	
	
	if (res.count() == 0)
		return 0;

	mySQL_ROW row = res.rows[0];

	if (Sender.c_str() == row[0])		
		return(1);
	else
		botSay(vs.get("NotAllowedToExecuteCommand"), Sender.c_str());

	return(0);
}

//-----------------------------------------------------------------------------
void say(std::string SenderX, MString Message) 
{
	if (checkOP(SenderX)) 
	{
		MString x = Message;
		x.erase (vs.get("CMDSay ").c_str());
		botSay (x);
	}
}

//-----------------------------------------------------------------------------
void sucheSQL(MString SenderX, MString wortX) 
{
	mySQL_RES res; 
	MString x = wortX;

	x.concat ("END");
	MString z = x.getbedfirst ((vs.get("CMDSuche")+" ").c_str(), "END");
  	   
	res = mySQL.sendQuery("select count(*) from fragen where Frage like '%%%s%%';", z);
	mySQL_ROW rows = res.rows[0];

	res =	mySQL.sendQuery("select count(*) from fragen where Antwort like '%%%s%%';",z);
	mySQL_ROW rows2 = res.rows[0];

	botSay (vs.get("SearchResults"),z.stt.c_str(), rows[0].c_str(), rows2[0].c_str());
}

//-----------------------------------------------------------------------------
void zeigemeinefragenSQL(std::string sender) 
{
  std::string name;
  FILE *fd2;
  MString p;
  
  mySQL_RES res = mySQL.sendQuery("select * from fragen where Author='%s';",sender.c_str());

  int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);
  if (numrows>0) 
  {
  int g = rand () % 10000;
      
  name = g;
  if (NULL == (fd2 = fopen (name.c_str(), "w")))
	{
		printf ("Can't open it ejecting!\n");
	}
	else
	{
		printf ("file erzeugt\n");
	}
  
  	fprintf (fd2, "<HTML>\n");
	fprintf (fd2, "<H1>%s --- Fragen</H1><BR>",sender.c_str());
	fprintf (fd2, "<TABLE WIDTH=1000>");
	fprintf (fd2,"<TR><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD></TR>\n","ID","Frage","Antwort","Typ","Count","Diff");
    for (int i=0;i<numrows;i++) 
	{
		mySQL_ROW row=res.rows[0]; 
	 	if (i % 2 == 0)
			fprintf (fd2, "<TR BGCOLOR=\"#FFE6FF\">");
		else
			fprintf (fd2, "<TR>");
   	    
		fprintf (fd2,"<TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD><TD>%s</TD></TR>\n",row[4].c_str(),row[1].c_str(),row[2].c_str(),row[3].c_str(),row[5].c_str(),row[6].c_str());
	}
	fprintf (fd2, "</TABLE>");	
	fprintf (fd2, "</HTML>\n");	
    fclose (fd2);
    
    printf ("lade hoch\n");
/*
	netbuf *nControl;
	FtpInit ();
	
	FtpConnect (iniftpServer, &nControl);
	FtpLogin (iniftpName,iniftpPass , nControl);
	FtpChdir(iniftpDir, nControl);		
	FtpPut (name, name, FTPLIB_ASCII, nControl);
	FtpQuit (nControl);
    printf ("hochgeladen\n");	
   */ 
    p = "/w ";  
  	p += sender.c_str();
	p += " du findest deine Fragen jetzt unter http://www.ph2.net/Fragen/";
  	p += name.c_str();
    botSay (p.stt.c_str());		
        
  } else {
  		p = "/w ";  
   		p += sender.c_str();
		p += " Du hast noch keine Fragen beigetragen, jetzt aber schell...";
        botSay (p.stt.c_str());		
  }
}

//-----------------------------------------------------------------------------
void wunschSQL(MString Sender, MString Message) 
{	
	MString x = Message;
	x.concat ("END");
	MString z = x.getbedfirst (vs.get("CMDWunsch ").c_str(), "END");
	
	if (z.strlength() > 2) 
	{
		auto row = mySQL.sendQuery("select count(*) from wunschliste where name='%s';", Sender).rows[0];

		if (atoi(row[0].c_str())==0) 
		{	  
			row=mySQL.sendQuery("select count(*) from fragen where gestellt=0 and Frage like '%%%s%%';", z.stt.c_str()).rows[0];

			if (atoi(row[0].c_str()) < 25)
			{	
   				(atoi(row[0].c_str()) ==0) ?
					botSay(vs.get("CantComplyWithWishNotEnoughQuestions"), Sender.stt.c_str(), z.stt.c_str())	
				:
					botSay(vs.get("CantComplyWithWishNotEnoughUAQuestions"), Sender.stt.c_str(), z.stt.c_str(), row[0].c_str());
			} 
			else 
			{
				mySQL.sendQuery("insert into wunschliste (wunsch,name) Values ('%s','%s');", z.stt.c_str(), Sender.stt.c_str());
				botSay(vs.get("AnnouncementPlayerHasWished"), Sender.stt.c_str(), z.stt.c_str());
			}					
		} 
		else
			botSay(vs.get("AnnouncementCantWishMoreThanOnce"), Sender.stt.c_str());
	}
}

//-----------------------------------------------------------------------------
void comment(MString Sender,MString Message) 
{
	MString x = Message;
	x.concat ("END");
	MString z = x.getbedfirst (vs.get("CMDComment ").c_str(), "END");	

	mySQL.sendQuery("insert into comment (name,comment) Values ('%s','%s');", Sender.stt.c_str(), z.stt.c_str());

	botSay(vs.get("ThanksForYourComment"), Sender);
}

//-----------------------------------------------------------------------------
void deletefrageID(MString Sender, std::string id) 
{		
	mySQL_RES res = mySQL.sendQuery("select Author,Frage,Antwort,ID from fragen where ID='%s';", id.c_str());

	if (!res.count())
	{
		botSay(vs.get("NoQuestionWithThisID"), id.c_str());
		return;
	}

	mySQL_ROW row = res.rows[0];

	botSay (vs.get("UserHasDeletedQuestion"),Sender.stt.c_str(), row[3].c_str(),row[1].c_str(),row[0].c_str());
		
	mySQL.sendQuery("delete from fragen where ID='%s';", id.c_str());
	mySQL.sendQuery("delete from revision where ID='%s';", id.c_str());
}

//-----------------------------------------------------------------------------
// TODO: depreciated
void comverlaesstsep(MString Nachricht) {
     MString x;     
     x=Nachricht;
     x.preconcat("<MARKER>");
     MString name;
     name=x.getbedfirst("<MARKER>"," verlaesst das Separee");
     MString p;
     p="/w ";
     p+=name;
     p+=" Treulose Tomate! Bis bald.";
     botSay(p.stt.c_str());
//   printf("[%s]\n",p.stt.c_str());
}

//-----------------------------------------------------------------------------
// TODO: depreciated
void comneuerimsep(MString Nachricht) {
     MString x;     
     x=Nachricht;
     x.preconcat("<MARKER>");
     MString name;
     name=x.getbedfirst("<MARKER>"," betritt das Separee");
     MString p;
     p="/w ";
     p+=name;
     p+=" Hallo ";
     p+=name;
     p+=". Willkommen im Quizsep.";
	 if (!quizStruct.delayneuesquiz&&(quizStruct.niemandSpieltMit!=inifragenbisquizstopbeikeinerantwort)) {
        if (quizStruct.quizType== QuizStateStruct::QuizTypes::standart) 
		{
			p+=" Aktuelle Frage: ";
			p+=AFrage.Frage;
        }
     }
     if (quizStruct.delayneuesquiz) {
           p+=" (Wir sind in der Pause, in ";
           p+=60-(int)(quizStruct.delayTimer/10);
           p+=" Sekunden geht es weiter)";           
     }
     botSay(p.stt.c_str());
}


void showstats(std::string Sender) 
{
	tm *nun;    
    nun = localtime(&Startuptime);
	time_t now=time(0); 
	
	double	seconds	=	difftime(now, Startuptime);	
	int		minuten	=	(int(seconds)%3600) / 60;
	int		stunden	=	(int(seconds)-(int(seconds)%3600)) / 3600;
	
	char sIntString[32];
	char rIntString[32];		
    char hIntString[32];		
	char jIntString[32];
	char uIntString[32];
	char dIntString[32];


	mySQL_ROW row;

	row=mySQL.sendQuery("select count(*) from fragen;").rows[0];
	sprintf (sIntString, "%s", row[0].c_str());  
	row=mySQL.sendQuery("select count(distinct ID) from revision;").rows[0];
	sprintf (rIntString, "%s", row[0].c_str());
	row=mySQL.sendQuery("select count(*) from fragen where gestellt=1;").rows[0];
	sprintf (hIntString, "%s", row[0].c_str());
	row=mySQL.sendQuery("select count(*) from fragen where gestellt=0;").rows[0];
	sprintf (jIntString, "%s", row[0].c_str());

	std::string date;
	sprintf(dIntString, "%i.%i-%i:%i", nun->tm_mday, nun->tm_mon + 1, nun->tm_hour, nun->tm_min);
	sprintf(uIntString, "%ih%im", stunden, minuten);

	botSay(vs.get("ShowStats"), Sender.c_str(), sIntString, rIntString, hIntString, std::to_string(quizStruct.spielerAnzahl).c_str(), hIntString, jIntString, date.c_str(), uIntString);
}

//-----------------------------------------------------------------------------
void deletelast(std::string sender) 
{
	mySQL_ROW row = mySQL.sendQuery("select max(ID) from fragen where Author='%s';", sender.c_str()).rows[0];
	mySQL.sendQuery("delete from fragen where ID='%s';", row[0]);    
	botSay("DeletedLastQuestion", sender);		
}

//-----------------------------------------------------------------------------
void frage(MString Sender, MString Message) 
{
	if (!quizStruct.delayneuesquiz)
		botSay(vs.get("ShowCurrentQuestionToUser"), Sender.stt.c_str(), AFrage.Frage.stt.c_str());
	else
		botSay(vs.get("ShowCurrentQuestionToUser"), Sender.stt.c_str(), std::to_string(60 - (int)(quizStruct.delayTimer / 10)).c_str());
}

//-----------------------------------------------------------------------------
void invite(MString Sender, MString Message) 
{
     MString p;
     p="/sepinv ";
     p+=Sender;
     botSay(p.stt.c_str());
}

//-----------------------------------------------------------------------------
void revolt(std::string Sender, MString Message) 
{
   if (checkOP(Sender)) 
   {
		MString x, p, z;
		x = Message;
		x.concat ("END");
		z = x.getbedfirst (vs.get("CmdRevolt").c_str(), "END");
		int y = atoi (z.stt.c_str());
		
		p="Revolt Frage Nr. ";
		p+=z;
		p+=" durch ";
		p+=Sender.c_str();    
		p+=" Gegenstimmen: (";
    
		mySQL_RES res = mySQL.sendQuery("select * from revision where ID='%i';", y);
    
		int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);
		for (int i=0;i<numrows;i++) {
			mySQL_ROW row=res.rows[0];
			p+="[";        
			p += row[1].c_str();
			p += "]";
		}
	p += ")";
	botSay(p.stt.c_str());
	mySQL.sendQuery("delete from revision where ID='%i';", y);
   }
}

//-----------------------------------------------------------------------------
void setsize(std::string Sender, MString Message) 
{
   if (checkOP(Sender)) {  
		MString x, p, z;
		x = Message;
		x.concat ("END");
		z = x.getbedfirst (vs.get("CmdSetSize").c_str(), "END");
		int y = atoi (z.stt.c_str());
		FragenproRunde = y;
		botSay(vs.get("RoundLimitSetTo"), z.stt.c_str());
   }
}

//-----------------------------------------------------------------------------
void setzwischenfragendelay(std::string Sender, MString Message) {
   if (checkOP(Sender)) 
   {  
		MString x, z, p;
		x = Message;
		x.concat ("END");
		z = x.getbedfirst ("!setzwischenfragendelay ", "END");
		int y = atoi (z.stt.c_str());
		printf ("Wartezeit zwischen den Fragen auf %ims gesetzt\n", y);
		zwischenfragendelay= y;
		p = "Wartezeit zwischen den Fragen auf ";
		p+= z;
		p+= " Milisekunden gesetzt";
		botSay (p.stt.c_str());
   }
}

//-----------------------------------------------------------------------------
void setspeed(std::string Sender, MString Message) {
   if (checkOP(Sender)) {  
		MString x, z;
		x = Message;
		x.concat ("END");
		z = x.getbedfirst (std::string(vs.get("CmdSetSpeed") + std::string(" ")).c_str(), "END");
		int y = atoi (z.stt.c_str());
		
		gamespeed= y;

		botSay (vs.get("SecondHasNowMs"),z.stt.c_str());
   }
}

//-----------------------------------------------------------------------------
void setzusammenhaengendefragen(std::string Sender, MString Message) 
{
   if (checkOP(Sender)) 
   {  
        zusammenhaengendefragen=!zusammenhaengendefragen;                 
        botSay(vs.get("SetConnectedQuestions"), (zusammenhaengendefragen) ? vs.get("Active").c_str() : vs.get("Inactive").c_str());
   }
}

//-----------------------------------------------------------------------------
void setnachzueglerdelay(std::string Sender, MString Message) 
{
   if (checkOP(Sender)) {  
		MString x, p, z;
		x = Message;
		x.concat ("END");
		z = x.getbedfirst ("!setnachzueglerdelay ", "END");
		int y = atoi (z.stt.c_str());
		printf ("Zusaetzliche Antwortzeit auf %ims gesetzt\n", y);
		warteaufnachzueglerdelay= y;
		p = "Zusaetzliche Antwortzeit auf ";
		p+= z;
		p+= "ms gesetzt";
		botSay (p.stt.c_str());
   }
}

//-----------------------------------------------------------------------------
void setabcfragen(std::string Sender, MString Message) 
{
   if (checkOP(Sender)) {  
		MString x, p, z;
		x = Message;
		x.concat ("END");
		z = x.getbedfirst (std::string(vs.get("CmdSetABCFragenFreq") + " ").c_str(), "END");
		int y = atoi (z.stt.c_str());
		abcfragenhaeufigkeit= y;
		if (abcfragenhaeufigkeit!=0) {
		p = "Haeufigkeit der ABC-Fragen nun: 1/";
		p = p + z;
        } else {
        p = "ABC-Fragen abgeschaltet";       
        }
		botSay (p.stt.c_str());
   }
}

//-----------------------------------------------------------------------------	
void remfromsperrliste(std::string Sender, MString Kandidat) 
{
  if (checkOP(Sender)) {  
  MString x, z;
  x = Kandidat;
  x.concat ("END");
  z = x.getbedfirst ("!remfromsperrliste ", "END");	

  MString v;
  v="/w ";
  v+=Sender.c_str();

  if (ChatBot.remfromsperrlist(z)) {
  v+=" Du hast ";
  v+=z;
  v+=" von der Sperrliste geloescht.";
  } else {
  v+=" ";         
  v+=z;
  v+=" steht nicht auf der Sperrliste.";
  
  }
  botSay(v.stt.c_str());  
  }     
}

void addtosperrliste(std::string Sender, MString Kandidat) {
  if (checkOP(Sender)) {       
  MString x, z;
  x = Kandidat;
  x.concat ("END");
  z = x.getbedfirst ("!addtosperrliste ", "END");	
  ChatBot.addtosperrlist(z);
  MString v;
  v="/w ";
  v+= Sender.c_str();
  v+=" Du hast ";
  v+=z;
  v+=" auf die Sperrliste gesetzt.";
  botSay(v.stt.c_str());  
  }                        
}	
	
//-----------------------------------------------------------------------------	
void addOP(std::string Sender, MString Kandidat) {
	if (checkOP(Sender)) 
	{  		
		MString x = Kandidat;
		x.concat ("END");
		MString z = x.getbedfirst (vs.get("CMDAddop ").c_str(), "END");

		mySQL.sendQuery("insert into operatorenliste (macher, operator) VALUES('%s','%s');",Sender.c_str(),z.stt.c_str());

		botSay(vs.get("AddedToOPList"), Sender.c_str(), z.stt.c_str());
	}
}

//-----------------------------------------------------------------------------	
void HowManySQL (MString Sender, MString SenderX)
{ 
	mySQL_ROW row, row2, row3, rowp;

	mySQL_RES res = mySQL.sendQuery("select count(*) from fragen where author='%s'", SenderX.stt.c_str());
	if (res.count()) row = res.rows[0];
	res = mySQL.sendQuery("select count(distinct r.ID) from revision r where exists (select * from fragen f where r.ID=f.ID and f.Author='%s');", SenderX.stt.c_str());
	if (res.count()) row2 = res.rows[0];
	res = mySQL.sendQuery("select avg(zeit) from log where name='%s' and frageID not in (select ID from fragen where Fragetyp>0)",SenderX.stt.c_str());
	if (res.count()) row3 = res.rows[0];
	res = mySQL.sendQuery("select punkte from ewigeliste where name='%s';",SenderX.stt.c_str());
	if (res.count()) rowp = res.rows[0];

	char nIntString[32];
	if (rowp.size()>0) 
		sprintf (nIntString, "%s", rowp[0].c_str()); 
	else 
		sprintf (nIntString, "0");

	if (!row3.size())
		botSay(vs.get("HowManyQuestionsContributed"), Sender.stt.c_str(), SenderX.stt.c_str(), row[0].c_str(), row2[0].c_str(), row3[0].c_str(), nIntString);
	else 
		botSay(vs.get("HowManyQuestionsContributed") + vs.get("AvgResponseTimeIs"), Sender.stt.c_str(), SenderX.stt.c_str(), row[0].c_str(), row2[0].c_str(), row3[0].c_str(), nIntString, row3[0].c_str(), nIntString);
}

//-----------------------------------------------------------------------------	
void TransferSQL (MString Sender, MString SenderX)
{
  MString x,z;
  x = SenderX;
  x.concat ("END");
  z = x.getbedfirst (vs.get("CMDTransfer ").c_str(), "END");
  
  
  mySQL_ROW row = mySQL.sendQuery("select * from ewigeliste where name='%s';", Sender).rows[0];

  if (row.size()>0)   
  {  
		unsigned long Pkt,fragengehoert,fragenbeantwortet,Siege;
		Pkt=atoi(row[1].c_str());
		fragengehoert=atoi(row[2].c_str());
		fragenbeantwortet=atoi(row[3].c_str());
		Siege=atoi(row[4].c_str());

		mySQL.sendQuery("insert into ewigeliste (name, punkte, gehoert, beantwortet,siege) Values ('%s',%i,%i,%i,%i) ON DUPLICATE KEY UPDATE punkte=punkte+%i, gehoert=gehoert+%i, beantwortet=beantwortet+%i, siege=siege+%i",z.stt.c_str(),Pkt,fragengehoert,fragenbeantwortet,Siege,Pkt,fragengehoert,fragenbeantwortet,Siege);
		mySQL.sendQuery("delete from ewigeliste where name='%s';",Sender.stt.c_str());
  
  
		MString p;
		p= "/w ";
		p += Sender;
		p += " Deine Punkte wurden auf ";
		p += z;
		p += " uebertragen";
	
		botSay (p.stt.c_str());
	}		
}


void SetFrageThema (std::string sender, MString str) {
     if (checkOP(sender)) {
     MString t,IDtext,THEMA;
     int ID;
     t=str;
     IDtext=t.getbedfirst("!thema "," ");
     t.erase("!thema ");
     t.concat ("<END>");
     THEMA=t.getbedfirst(" ","<END>");
     ID=atoi(IDtext.stt.c_str());
     if ((ID<=0)||(ID>100000)) {
        fertig1:
        printf("ID out of range(%i)(%s)\n",ID,IDtext.stt.c_str());
        MString p;
        p="/w ";
        p+=sender.c_str();
        p+=" ID konnte nicht erkannt werden";
        botSay(p.stt.c_str());        
        return;
     } else 
	 {  
  mySQL_RES res = mySQL.sendQuery("select frage from fragen where ID=%i", ID);

  int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);
  if (numrows==0) goto fertig1;
  mySQL_ROW row = res.rows[0];
  MString af;
  af=row[0].c_str();
  MString endp;
  if (af.beginswith("(")) {
  MString k;
  k=af;
  k+="<END>";
  af=k.getbedfirst(")","<END>");                        
  } 
  endp="(";
  endp+=THEMA;
  endp+=") ";    
  endp+=af;      
  
  mySQL.sendQuery("update fragen set frage=('%s') where ID=%i;",endp.stt.c_str(),ID);
  
//  printf("neue frage: [%s]\n",endp.stt.c_str());        
        
        
        
        MString p;
        p="*Thema der Frage ";
        p+=ID;
        p+=" wurde auf (";
        p+=THEMA;
        p+=") geaendert";        
        botSay(p.stt.c_str());               
     }
     printf("ID: [%i], THEMA: [%s]\n",ID,THEMA.stt.c_str());
     } else {
       MString p;
       p="/w ";
       p+=sender.c_str();
       p+=" du bist nicht berechtigt das thema einer Frage zu aendern.";
       botSay(p.stt.c_str());
     }
}

//-----------------------------------------------------------------------------
void flush(const MString sender)
{
	if (checkOP(sender))
		vs.flushMappings();	
}

//-----------------------------------------------------------------------------
void LernFrageSQL(MString autor, MString str)
{
	if (str.includes("|"))
	{
		MString question;
		question = str;
		question.erase("!lern ");
		question.concat("<END>");
		MString answer;
		answer = question.getbedfirst("|", "<END>");
		question.erase(answer);
		question.erase("|<END>");

		question.replace("'", "''");
		answer.replace("'", "''");

		std::string a = autor.stt.c_str();
		std::string b = question.stt.c_str();
		std::string c = answer.stt.c_str();

					mySQL.sendQuery("insert into fragen Values ('%s', LTrim('%s'), LTrim('%s'), 0, 0, 0, 0, 1)", a.c_str(),b.c_str(),c.c_str());
		auto res =	mySQL.sendQuery("select count(*) from fragen where Author='%s'", a.c_str());
		auto row =	res.rows[0];

		botSay(vs.get("ThanksForYourQuestion"), std::string(row[0]).c_str());
	}
}

void EditFrageSQL (std::string autor, MString str)
{
	if (checkOP(autor)) 
	{		
		MString y = str;
		y.erase (vs.get("CMDEdit").c_str());
		y.concat ("<END>");
		MString x = y.getbedfirst ("|", "<END>");
		MString v = y.getbedfirst ("|", "<END>");

		y.erase (x);
		y.erase ("|<END>");
		x.erase (v);
		x.erase ("|");		
		x.replace("'","''");

		v.replace("'","''");

		//printf("[%s] [%s] [%s]\n",x.stt.c_str(),y.stt.c_str(),v.stt.c_str());
    
		mySQL_RES res = mySQL.sendQuery("select * from fragen where ID='%s';",y.stt.c_str());
		
		if ((int)MySQLAbstractionLayerClass::num_rows(&res) > 0)
		{
			mySQL.sendQuery("update fragen set antwort=LTrim('%s'), frage=LTrim('%s') where ID='%s'",v.stt.c_str(),x.stt.c_str(),y.stt.c_str());
			mySQL.sendQuery("update fragen set Fragetyp=1 where ID='%s';",y.stt.c_str());
			mySQL.sendQuery("delete from revision where ID='%s';",y.stt.c_str());

			botSay(vs.get("EditResponseSuccess"), autor.c_str(), y.stt.c_str(), x.stt.c_str(), v.stt.c_str());
		} else		
			botSay (vs.get("EditResponseFail"), autor.c_str(), v.stt.c_str());
	}
}

//-----------------------------------------------------------------------------
void Shutdown() 
{
	botSay(vs.get("BotShutdown"));
	quizStruct.quizIsRunning = 0;
	mySQL.Shutdown();
	ChatBot.Exit (); 	

	Write2Log("querylog.txt","---EXIT NO ERROR IN SESSION---");           
	exit(-1);  
}

//-----------------------------------------------------------------------------
void Shutdown(std::string Sender, MString s2) {
    if (checkOP(Sender)) Shutdown();
}

//-----------------------------------------------------------------------------
void reconnect(std::string Sender) 
{
    if (checkOP(Sender)) {
       botSay("/bye");                          
       ChatBot.errorcode=191;
    }
}

//-----------------------------------------------------------------------------
void LernGlueckwunschSQL (std::string autor, MString str)
{
	if (!checkOP(autor)) 
		return;
	
	str.erase(vs.get("CMDLaudatio").c_str());    
	mySQL.sendQuery("insert into glueckwuensche Values ('%s', LTrim('%s'))",str.stt.c_str(),autor.c_str());  
	botSay (vs.get("ThanksForLaudatio"), autor.c_str());
}

//-----------------------------------------------------------------------------	
void WunschlisteAuswerten() 
{   
    mySQL_ROW row;    
    MString p;      
	themenschlagwort = "";
    double s=0;  
    quizStruct.themenRunde=0;
    
	mySQL_RES res = mySQL.sendQuery("select *,c/a as x from (select wunsch,count(*) as c,(select count(*) as g from wunschliste) as a from wunschliste group by wunsch) as tab4 order by x desc;");
	int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);
	if (numrows > 0)
	{
		p = "Ergebnis:";
		for (int i = 0; i < numrows; i++)
		{
			row = res.rows[0];
			if (i == 0)
			{
				s = strtod(row[3].c_str(), NULL);
				themenschlagwort = row[0].c_str();
			}
			p += " ";
			p += row[0].c_str();
			p += ":";
			p += row[3].c_str();
		}
		botSay(p.stt.c_str());

		mySQL.sendQuery("delete from wunschliste;");

		if (s > 0.5)
		{
			//Sicherheitscheck, sind genug treffer vorhanden
		  /*
			free(query);
			query=(char*)malloc(255);
			sprintf(query, "select count(*) from fragen where gestellt=0 and Frage like '%%%s%%';",themenschlagwort);
			res=SQLGetCommand(query);
			row=MySQLAbstractionLayerClass::fetch_row(res);

		  */
			quizStruct.themenRunde = 1;
			botSay("Thema dieser Runde ist %s", themenschlagwort);
		}
		else
		{
			botSay("_Fuer diese Runde kam kein mehrheitlicher Themenwunsch zustande.");
			quizStruct.themenRunde = 0;
		}
	}
}

//-----------------------------------------------------------------------------	
void ShowEwigeListeSQLuser(std::string Sender) 
{
  mySQL_RES res = mySQL.sendQuery("select name,punkte from ewigeliste order by punkte desc");

  if (!res.count())
	  return;

  int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);
  std::string p;
  
  /*
  float scale=1;
  for (int i=0;i<5;i++) {
  float lcounter=0;
  p= "/w ";
  p=p+Sender.stt.c_str();
  p+=" ";  
  for (int l=0;l<4;l++) {
  row=MySQLAbstractionLayerClass::fetch_row(res);
  MString sg;
  MString s;  
  s=row[0];
  s="";
  for (int o=0;o<10;o++) {
        char *sx3;
        int ui=rand()%5;        
        if (ui==0)sx3="A";
        if (ui==1)sx3="R";
        if (ui==2)sx3="T";
        if (ui==3)sx3="E";
        if (ui==4)sx3="F";  
        s+=sx3;
}
  sg=s.gross();
  p+=sg; 
  char *s2;
  s2=sg.stt.c_str();
  char sx;
  int c=0;
  sx=s2[0];
  do {
    for (int o=0;o<37;o++) {        
        char *sx3;
        sx3=ChatBot.schlussel[o].zeichen.stt.c_str();
        char sx2;
        sx2=sx3[0];
//        printf("[%c|%c]",sx,sx2);        
        if (sx==sx2) {lcounter+=ChatBot.schlussel[o].width*scale;}
    }      
    c++;
    sx=s2[c];  
  } while(sx!='\0');
  bool z;
//  z=0;
  while (lcounter<650*(l+1)) {
//        if (z) {p+=":";lcounter+=24.296;}
        {p+=" &rlm;";lcounter+=20,5;}
//        p+=".";lcounter+=20,5;        
//        z=!z;
  }
  
  }
  botSay (p.stt.c_str());  
  }
  
*/
	  for (int k=0;k<2;k++) 
	  {
		  p= "/w ";
		  p=p+Sender;
		  if (k==0) p += " _TOP15:_ ";
		  if (k==1) p += " _TOP30:_ ";
		  for (int i=0;i<15;i++) {
		  if (i+k*15>=numrows) goto fertig;
		  mySQL_ROW row =res.rows[0];
		  p += "[";
		  p += row[0];
		  p += ":";
		  p += row[1];
		  p += "] ";
	  }
	  fertig:
	  botSay (p);
	  }  
}

//-----------------------------------------------------------------------------	
void ShowTagesRanking(MString Sender) 
{
	mySQL_RES res=mySQL.sendQuery("select name,punkte from tagesliste group by punkte desc");
  
	int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);
	MString p;   
	for (int k=0;k<2;k++) 
	{
		p= "/w ";
		p=p+Sender;
		if (k==0) p += " _TTOP-1..15:_ ";
		if (k==1) p += " _TTOP-16..30:_ ";
		for (int i=0;i<15;i++) {
		if (i+k*15>=numrows) goto fertig;
		mySQL_ROW row = res.rows[0]; /* Get a row from the results */
		p += "[";
		p += row[0].c_str();
		p += ":";
		p += row[1].c_str();
		p += "] ";
	}
	fertig:
	botSay (p);
	}
}

//-----------------------------------------------------------------------------	
void MakeTipp(int laenge) 
{
  int tipart=rand()%2;
  if (tipart==0) for (int i=0;i<laenge;i++) zu[i]=i%4; //standart
  if (tipart==1) 
  {
     for (int i=0;i<laenge;i++) zu[i]=i%4; 
     for (int i=0;i<laenge;i++) {
         int a=rand()%laenge;
         int tmp=zu[a];
         zu[a]=zu[i];
         zu[i]=tmp;
     }
  }
}

//-----------------------------------------------------------------------------
void ShowRanking (std::string Sender)
{
	if (!quizStruct.delayneuesquiz) 
	{		
		char questionsLeft[32];		
		sprintf (questionsLeft, "%i", FragenproRunde- quizStruct.frageinrunde);
		std::string p;
		p = "/w ";
		p = p + Sender;
		p = p + " ";

		if (quizStruct.spielerAnzahl>0) p = p+"Punkte"; else p=p+"Noch keine Punkte. ";	  
		for (int i = 0; i < quizStruct.spielerAnzahl; i++)
		{
			char sIntString[32];
			sprintf (sIntString, "%d", Punkte[rangliste[i]].Pkt);
			p = p + "[";
			p = p + Punkte[rangliste[i]].Spieler.stt.c_str();
			p = p + ":";
			p = p + sIntString;
			p = p + "] ";
		}
		p = p + "Noch ";
		if (quizStruct.frageinrunde<FragenproRunde-1) {p = p + questionsLeft;p = p + " Fragen";}
		else p = p + "eine Frage";	

		if (quizStruct.themenRunde) {
			p+=" Thema dieser Runde: ";
			p+=themenschlagwort;    
		}
		botSay (p);
		} 
	else 
		botSay(vs.get("SecondsToNextQuiz"),Sender.c_str(), std::to_string(60 - (int)(quizStruct.delayTimer / 10)).c_str());
}

//-----------------------------------------------------------------------------
void comendquiz(std::string Sender) 
{
	if (checkOP(Sender)) 
	{
		botSay(vs.get("QuizEnded"));
		quizStruct.quizIsRunning = 0;
		quizStruct.delayneuesquiz=0;
		scoreLists.UpdateEwigeListeSQL();
		ShowRanking (Sender.c_str());
		// 		WriteEwigeListeHTML();
		quizStruct.spielerAnzahl = 0;
	}	
}

//-----------------------------------------------------------------------------
void comstartquiz(std::string Sender, MString Message) 
{
   if (checkOP(Sender)) {
		if (quizStruct.quizIsRunning == 0) quizStruct.startthequiz();
		else
		{
			vs.get("QuizAlreadyRunning");
            char ttString[150];
		    sprintf (ttString, vs.get("QuizAlreadyRunning").c_str(), Sender.c_str());
			botSay (ttString);
		}
	}
}

//-----------------------------------------------------------------------------
void comdelete(std::string Sender, std::string  Message) 
{
   if (checkOP(Sender)) 
   {
		MString x, z;
		x = Message.c_str();
		x.concat ("END");
		z = x.getbedfirst ((vs.get("CmdDelete")+" ").c_str(), "END");	
	    deletefrageID(Sender.c_str(),z.stt.c_str());	    	    
    }
}

//-----------------------------------------------------------------------------
void howmanymain(MString Sender, MString Message) 
{
        if (Message.strlength()<30) 
		{
        MString S;S=Sender;
		if (Message.includes ("@"))
		{		  
			MString o;
			MString dieshier;
			o = Message;
			dieshier.set ("");
			o.concat ("<END>");
			dieshier = o.getbedfirst ("@", "<END>");
			HowManySQL(S, dieshier);
		}
		else HowManySQL(S,S);
    }
}

//-----------------------------------------------------------------------------
void GibPunkte (MString Sender,int pkt) 
{
    char sIntString[200];
    int z;
    MString p;    

    int mcu=0;
	int u = 0;
	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		Punkte[i].fragengehoert++;        
		if (Punkte[i].Spieler==Sender.stt.c_str())
		{
            mcu=i;
			u = 1;
		}
	}
	
	if (u == 0) 
	{
		mcu=quizStruct.spielerAnzahl;
		Punkte[quizStruct.spielerAnzahl].Spieler = Sender;
		Punkte[quizStruct.spielerAnzahl].Pkt = 0;
		Punkte[quizStruct.spielerAnzahl].fragenbeantwortet = 1;
		Punkte[quizStruct.spielerAnzahl].fragengehoert = 1;
		Punkte[quizStruct.spielerAnzahl].Siege = 0;
		quizStruct.spielerAnzahl++;
	}

    for (int i = 0; i < quizStruct.spielerAnzahl; i++) rangliste[i] = i;
	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		for (int l = 0; l < quizStruct.spielerAnzahl; l++)
		{
			if (Punkte[rangliste[i]].Pkt >
			    Punkte[rangliste[l]].Pkt)
			{
				z = rangliste[i];
				rangliste[i] = rangliste[l];
				rangliste[l] = z;
			}
		}
	}

	for (int i = 0; i < quizStruct.spielerAnzahl; i++) 
	{	
		rl1platz[i]=i+1;
		rl1name[i]=Punkte[rangliste[i]].Spieler;
	}		

	Punkte[mcu].Pkt += pkt;
	Punkte[mcu].fragenbeantwortet++;

    for (int i = 0; i < quizStruct.spielerAnzahl; i++) rangliste[i] = i;
	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		for (int l = 0; l < quizStruct.spielerAnzahl; l++)
		{
			if (Punkte[rangliste[i]].Pkt >
			    Punkte[rangliste[l]].Pkt)
			{
				z = rangliste[i];
				rangliste[i] = rangliste[l];
				rangliste[l] = z;
			}
		}
	}
	for (int i = 0; i < quizStruct.spielerAnzahl; i++) {
		rl2platz[i]=i+1;
		rl2name[i]=Punkte[rangliste[i]].Spieler;
	}		
	
	
	for (int i = 0; i < quizStruct.spielerAnzahl; i++) 
	{
	for (int l = 0; l < quizStruct.spielerAnzahl; l++) 
	{
	if (rl1name[i]==rl2name[l]) 
	{
	//	printf("er: %s %i %i\n",rl1[i].name.stt.c_str(),rl1[i].platz,rl2[l].platz);		
		if (rl2platz[l]<rl1platz[i]) 
		{
			sprintf (sIntString, "_%s_ _%i_ &uArr; _%i_; ", rl1name[i].stt.c_str(),rl1platz[i],rl2platz[l]);
			p+=sIntString; 
		}
		if (rl2platz[l]>rl1platz[i]) 
		{
			sprintf (sIntString, "_%s_ _%i_ &dArr; _%i_; ", rl1name[i].stt.c_str(),rl1platz[i],rl2platz[l]);
			p+=sIntString; 
		}	
	}
	}
	}	
	if ((echtzeitrangliste)&&(p!="")) botSay(p.stt.c_str());		
}

//-----------------------------------------------------------------------------
void displayhelp(MString Sender) 
{
	//TODO Write usable help, whisper it to user (or refer to webpage)
	botSay(vs.get("HelpInformation"),Sender.stt.c_str());
}

//-----------------------------------------------------------------------------
std::string FolgeFragen (std::string SenderY)
{
	MString p, SenderX;
	SenderX.stt = SenderY;
	if (SenderX == LastAnswer)
	{
		folgefragen++;
		int folgefragen2=folgefragen-1;
		if (folgefragen2>3) folgefragen2=3;

//		p=tippstring();
		p = " + (";
		p += folgefragen;
		p+=" in Folge _";		
		p += folgefragen2;
		if (folgefragen == 2)
			p+="_ Pkt.)";
		else
			p+="_ Pkte.)";
		GibPunkte (SenderX, folgefragen2);
        quizStruct.niemandSpieltMit=0;		
//		sleep (1);
		return(p.stt.c_str());
	}
	else
	{
		folgefragen = 1;
		LastAnswer=SenderX;
		return("");
	}
}

//-----------------------------------------------------------------------------	
void ShowRankingAll ()
{
	std::string p = tippstring();
	(quizStruct.spielerAnzahl>0) ? p+= vs.get("HasPoints") : p=vs.get("HasNoPoints");

	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
		p += "[_" + (Punkte[rangliste[i]].Spieler.stt) + "_:_" + std::to_string(Punkte[rangliste[i]].Pkt) + "_] ";

	botSay (p.c_str());
}

//-----------------------------------------------------------------------------
void ShowToolTip() 
{
  printf("showtooltip anfang\n");    



  mySQL_RES res = mySQL.sendQuery("select * from tooltip order by rand() limit 1;");
  if (res.count()>0) 
  {
	  mySQL_ROW row=res.rows[0];
      MString p;
      p="_Hinweis:_ *";     
      p+=row[0].c_str();
      botSay(p.stt.c_str());
  }
  printf("showtooltip ende\n");      
}

//-----------------------------------------------------------------------------
void weiter(MString chatter) 
{
	if (!quizStruct.delayneuesquiz) 
	{
		int x;
		x=weiterlist.chattersaysweiter(chatter);
		if (x==1) 
		{
		MString p;
		p="/w ";
		p+=chatter;
		p+=" Du hast bereits ";
		p+=weiterlist.maxweitersprorunde;
		p+="mal Weiter in dieser Runde gesagt";
		botSay(p.stt.c_str());
		}
		if (x==2) 
		{
		MString p;
		p="/w ";
		p+=chatter;
		p+=" Du hast bei dieser Frage schon geweitert";
		botSay(p.stt.c_str());
		}
		if (x==3) 
		{
		MString p;
		p="_WEITER_ sagt ";
		p+=chatter;
		if (weiterlist.weiternum<ChatBot.ChatterSepList.ChatterAngemeldet()) {
			p+=" - fehlen noch _";
			p+=ChatBot.ChatterSepList.ChatterAngemeldet()-weiterlist.weiternum;
			p+="_ weiters!";
		}
		botSay(p.stt.c_str());
		}
	}
}