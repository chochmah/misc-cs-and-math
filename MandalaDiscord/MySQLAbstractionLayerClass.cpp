#include "MySQLAbstractionLayerClass.h"
#include "VariableStorageStruct.h"

#include <mysql.h>

//-----------------------------------------------------------------------------
MYSQL *mandalaDataBase;

//-----------------------------------------------------------------------------
MYSQL_RES *SQLGetCommand(std::string query)
{
	if (mysql_real_query(mandalaDataBase, query.c_str(), (unsigned long)strlen(query.c_str()))) 
		return(NULL);
	else 
		return(mysql_store_result(mandalaDataBase));
}

//-----------------------------------------------------------------------------
mySQL_RES MySQLAbstractionLayerClass::sendQuery(std::string query, ...)
{
	char *buffer = (char*)malloc(2000);

	va_list args;
	va_start(args, query);
	vsprintf(buffer, query.c_str(), args);
	va_end(args);

	mySQL_RES result;

	MYSQL_RES *res = SQLGetCommand(buffer);
	free(buffer);

	if (res != NULL && res->row_count != 0)
	{
		auto col_num = mysql_num_fields(res);
		auto row_num = mysql_num_rows(res);
		
		std::vector<std::string> tmp;

		for (int i=0;i<row_num;i++)
		{						
			auto r = mysql_fetch_row(res);			

			for (int l = 0; l < col_num; l++)
				tmp.push_back(r[l]);

			tmp.push_back(r[0]);
		} 
		result.rows.push_back(tmp);
	}
	
	return result;
}

//-----------------------------------------------------------------------------	
my_ulonglong MySQLAbstractionLayerClass::num_rows(mySQL_RES* arg)
{
	return arg->rows.size();
}

void MySQLAbstractionLayerClass::Shutdown()
{
	mysql_close(mandalaDataBase);
}

//-----------------------------------------------------------------------------	
// create database if it doesn't exist

void MySQLAbstractionLayerClass::InitMySQL()
{
	mandalaDataBase = mysql_init(NULL);	
	if (mysql_real_connect(mandalaDataBase, vs.get("SQLServerIP", "SQLSection").c_str(), vs.get("SQLUsername", "SQLSection").c_str(), vs.get("SQLPwd", "SQLSection").c_str(), NULL, stoi(vs.get("SQLPort", "SQLSection")), NULL, 0) == NULL)
	{
		printf("Error %u: %s\n", mysql_errno(mandalaDataBase), mysql_error(mandalaDataBase));
		exit(1);
	}

	if (mysql_select_db(mandalaDataBase, vs.get("SQLDatabaseName","SQLSection").c_str()))
	{
		printf(mysql_error(mandalaDataBase));
		printf("Error. SQL Database does not exist\n");
	}

	mySQL.sendQuery("create table IF NOT EXISTS fragen (Author char(50), Frage char(255), Antwort char(255), Fragetyp int, ID int unsigned NOT NULL AUTO_INCREMENT,count INT, diff FLOAT, gestellt int,PRIMARY KEY (ID));");
	mySQL.sendQuery("create table IF NOT EXISTS comment (name varchar(50), comment text, CID int(50) NOT NULL AUTO_INCREMENT,PRIMARY KEY (CID));");
	mySQL.sendQuery("create table IF NOT EXISTS ewigeliste(name varchar(50) NOT NULL, punkte int, gehoert int, beantwortet int, siege int, PRIMARY KEY(name));");
	mySQL.sendQuery("create table IF NOT EXISTS tagesliste(name varchar(50) NOT NULL, punkte int, gehoert int, beantwortet int, siege int, PRIMARY KEY(name));");
	mySQL.sendQuery("create table IF NOT EXISTS monatsliste(name varchar(50) NOT NULL, punkte int, gehoert int, beantwortet int, siege int, PRIMARY KEY(name));");
	mySQL.sendQuery("create table IF NOT EXISTS glueckwuensche (string text, name varchar(50));");
	mySQL.sendQuery("create table IF NOT EXISTS links (link text);");
	mySQL.sendQuery("create table IF NOT EXISTS tooltip (tip char(255));");
	mySQL.sendQuery("create table IF NOT EXISTS revision (ID int(11), user varchar(50), typ int(11));");
	mySQL.sendQuery("create table IF NOT EXISTS log (name varchar(50), frageID int(11), datum date, zeit int(11), date timestamp);");
	mySQL.sendQuery("create table IF NOT EXISTS wunschliste (wunsch varchar(30), name varchar(50));");
	mySQL.sendQuery("create table IF NOT EXISTS operatorenliste (operator varchar(50), macher varchar(50));");
	mySQL.sendQuery("USE " + vs.get("SQLDatabaseName", "SQLSection"));
}
