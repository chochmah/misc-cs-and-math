#pragma once
class HighScoreListenClass
{
public:
	HighScoreListenClass();
	~HighScoreListenClass();

	void ShowTagesListeSQL();
	void ShowEwigeListeSQL();
	void ShowEwigeListeChange();
	void UpdateEwigeListeSQL();
};

static HighScoreListenClass scoreLists;