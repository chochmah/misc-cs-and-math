/*-----------------------------------------------------------------------------
	Class for variables that reference an item within an ini file, the variable
	is initialized with the value that is found in the .ini. If there is non an
	entry will be generated. If no default value is given the variable will be ini-
	tialzed as empty, false or zero. The entry in the file will then be kept in
	sync with the variable.
	
	types: cint, cbool, cfloat, cstring;

	Behavior: 
	If the file doesn't exist -> Fatal Error
	If the Section or Key doesn't exist it is created with the default value but
		if no default Value is given no key is created	

	Usage:
	type [fileName, sectionName, keyName, defaultValue, synchType]	

	Optional argument synchType:	
	AUTO_SYNC		: Changing the variable will result in a corresponding
	change in the .ini immediately on changes
	DONT_SYNC		: Variable will be Initialized from the .ini but not
	synched back to the .ini if changed
	Default = AUTO_SYNC

	Usage examples:
	cstring	y("test.ini", "section1", "myStringVar", "defaultValue", SYNCTYPE::DONT_SYNC);
	cfloat	y("test.ini", "Section2", "myFloatVar", 100.0f);
	cbool	z("test.ini", "section3", "myBoolVar");
	cInt	z("test.ini", "section3", "myIntVar", SYNCTYPE::AUTO_SYNC);

	TODO::EXTENSION & IMPROVEMENTS
		Maybe switch that for deferred writing of changes to files if needed
			might be handy for options-menus?
		Maybe command to reread all values from files if needed?
		If to slow centralized file holder for all instances to speed up access
			and only read/write  files once ?
		Status flags: changed / init like default
		Crash when key/section/filename/default > 255 zeichen, do something?
-----------------------------------------------------------------------------*/

#pragma once

#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h> 
#include <crtdbg.h>  


#include "ConfigFileIOClass.h"
#include "ConfigVariableClassInterface.h"

#include <windows.h>
#include <iostream>
#include <sstream>


// Namespace of the config file class

namespace cf
{
	//----------------------------------------------------------------------------
	template <class T>
	class ConfigVariableClass : public cf::ConfigVariableClassInterface {
	private:
		std::string			fileName, section, key = "";
		T					defaultVal, val;
		SYNCTYPE			autoSynch = AUTO_SYNC;

		static std::mutex	writeLock;

	protected:
		void		write();		
		T			getDefaultVal() const { return defaultVal; }
		std::string	getFileName()	const { return fileName; };
		std::string	getSection()	const { return section; };
		std::string	getKey()		const { return key; };
		SYNCTYPE	getSyncType()	const { return autoSynch; }
		
		std::ostream&	getKeyPrintString(std::ostream& stream);

	public:
		T			getVal()		const { return val; }

		ConfigVariableClass(
			const std::string	_fileName,
			const std::string	_section,
			const std::string	_key,
			const T				_defaultVal);
		ConfigVariableClass(
			const std::string	_fileName,
			const std::string	_section,
			const std::string	_key);
		ConfigVariableClass(
			const std::string	_fileName,
			const std::string	_section,
			const std::string	_key,
			const T				_defaultVa,
			const SYNCTYPE		_autoSynch);
		ConfigVariableClass(
			const std::string	_fileName,
			const std::string	_section,
			const std::string	_key,
			const SYNCTYPE		_autoSynch);
		virtual ~ConfigVariableClass();

					void			print();
		static		void			printAll();

		// overloaded operators	
		operator T() const { return this->val;}

		ConfigVariableClass<T>&	operator=(const ConfigVariableClass<T>&);
		ConfigVariableClass<T>&	operator=(const T&);
		
		void					operator++(int) { this->val++; write(); }
		void					operator--(int) { this->val++; write(); }

		//-----------------------------------------------------------------------------
		friend std::ostream& operator<< (std::ostream& stream, const ConfigVariableClass& x_)
		{
			stream << x_.val;
			return stream;
		};

		//-----------------------------------------------------------------------------
		// copy constructor

		ConfigVariableClass<T>(const ConfigVariableClass<T> &x_)
		{
			fileName = x_.fileName;
			section = x_.section;
			key = x_.key;
			defaultVal = x_.defaultVal;
			val = x_.val;

			varList.add(this);
		};
	};

	//-----------------------------------------------------------------------------
	typedef ConfigVariableClass<int>			cint;
	typedef ConfigVariableClass<bool>			cbool;
	typedef ConfigVariableClass<float>			cfloat;
	typedef ConfigVariableClass<std::string>	cstring;

	//-----------------------------------------------------------------------------
	// static member declaration

	template <class T>
	std::mutex ConfigVariableClass<T>::writeLock;

	//-----------------------------------------------------------------------------
	// initialize key value from file

	template <class T>
	ConfigVariableClass<T>::ConfigVariableClass(
		const std::string	_fileName,
		const std::string	_section,
		const std::string	_key,
		const T				_defaultVal)
		: fileName(_fileName), section(_section), key(_key), defaultVal(_defaultVal)
	{
		std::stringstream defaultArgument;
		defaultArgument << defaultVal;

		ConfigFileIOClass configFile(fileName);
		val = (T)configFile.getParam(section, key, defaultArgument.str());
		varList.add(this);

		if (val == defaultVal)
			write();
	}

	template <class T>
	ConfigVariableClass<T>::ConfigVariableClass(
		const std::string	_fileName,
		const std::string	_section,
		const std::string	_key)
		: fileName(_fileName), section(_section), key(_key), defaultVal(T())
	{
		std::stringstream defaultArgument;
		defaultArgument << defaultVal;

		ConfigFileIOClass configFile(fileName);
		val = (T)configFile.getParam(section, key, defaultArgument.str());
		varList.add(this);
	}

	template <class T>
	ConfigVariableClass<T>::ConfigVariableClass(
		const std::string	_fileName,
		const std::string	_section,
		const std::string	_key,
		const T				_defaultVal,
		const SYNCTYPE		_autoSynchl)
		: fileName(_fileName), section(_section), key(_key), defaultVal(_defaultVal), autoSynch(autoSynch)
	{
		std::stringstream defaultArgument;
		defaultArgument << defaultVal;

		ConfigFileIOClass configFile(fileName);
		val = (T)configFile.getParam(section, key, defaultArgument.str());
		varList.add(this);

		if (val == defaultVal)
			write();
	}

	template <class T>
	ConfigVariableClass<T>::ConfigVariableClass(
		const std::string	_fileName,
		const std::string	_section,
		const std::string	_key,
		const SYNCTYPE		_autoSynch)
		: fileName(_fileName), section(_section), key(_key), defaultVal(T()), autoSynch(autoSynch)
	{
		std::stringstream defaultArgument;
		defaultArgument << defaultVal;

		ConfigFileIOClass configFile(fileName);
		val = (T)configFile.getParam(section, key, defaultArgument.str());
		varList.add(this);
	}

	template <class T>
	ConfigVariableClass<T>::~ConfigVariableClass()
	{
		varList.remove(this);
	};

	//-----------------------------------------------------------------------------
	// write val back to .ini file if autoSynch is enabled

	template <class T>
	void ConfigVariableClass<T>::write()
	{
		if (autoSynch == SYNCTYPE::AUTO_SYNC)
		{
			PIniRW::PIniReadWrite iniWriter(fileName.c_str());

			while (!writeLock.try_lock())
				Sleep(1);
			{
				ConfigFileIOClass configFile(fileName);
				configFile.setParam(section, key, val);
			}

			writeLock.unlock();
		}
	}

	//-----------------------------------------------------------------------------
	template <class T>
	ConfigVariableClass<T>& ConfigVariableClass<T>::operator=(const T& x_)
	{
		this->val = x_;
		write();

		return *this;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	ConfigVariableClass<T>& ConfigVariableClass<T>::operator=(const ConfigVariableClass<T>& x_)
	{
		this->val = x_.getVal();
		write();

		return *this;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator+
		(ConfigVariableClass<T> lhs, const ConfigVariableClass<T>& rhs)
	{
		lhs = lhs.getVal() + rhs.getVal();
		return lhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator+
		(ConfigVariableClass<T> lhs, const T& rhs)
	{
		lhs = lhs.getVal() + rhs;
		return lhs;
	}

	//-----------------------------------------------------------------------------
	inline ConfigVariableClass<bool> operator+
		(ConfigVariableClass<bool> lhs, const ConfigVariableClass<bool>& rhs)
	{
		lhs = lhs.getVal() ^ rhs.getVal();
		return lhs;
	}

	//-----------------------------------------------------------------------------
	inline ConfigVariableClass<bool> operator+
		(ConfigVariableClass<bool> lhs, const bool& rhs)
	{
		lhs = lhs.getVal() ^ rhs;
		return lhs;
	}

	//-----------------------------------------------------------------------------
	inline ConfigVariableClass<std::string> operator+
		(ConfigVariableClass<std::string> lhs, const char* rhs)
	{
		lhs = lhs.getVal() + (std::string)rhs;
		return lhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator-
		(ConfigVariableClass<T> lhs, const T& rhs)
	{
		lhs = lhs.getVal() - rhs;
		return lhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator-
		(ConfigVariableClass<T> lhs, const ConfigVariableClass<T>& rhs)
	{
		ConfigVariableClass<T> result = lhs.getVal() - rhs.getVal();
		return result;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator*
		(ConfigVariableClass<T> lhs, const T& rhs)
	{
		lhs = lhs.getVal() * rhs;
		return lhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator*
		(ConfigVariableClass<T> lhs, const ConfigVariableClass<T>& rhs)
	{		
		ConfigVariableClass<T> result = lhs.getVal() * rhs.getVal();
		return result;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator/
		(ConfigVariableClass<T> lhs, const T& rhs)
	{
		lhs = lhs.getVal() / rhs;
		return lhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline ConfigVariableClass<T> operator/
		(ConfigVariableClass<T> lhs, const ConfigVariableClass<T>& rhs)
	{		
		ConfigVariableClass<T> result = lhs.getVal() / rhs.getVal();
		return result;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator==
		(const ConfigVariableClass<T>& lhs, const ConfigVariableClass<T>& rhs)
	{
		return lhs.getVal() == rhs.getVal();
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator!=
		(const ConfigVariableClass<T>& lhs, const ConfigVariableClass<T>& rhs)
	{
		return !(lhs == rhs);
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator==
		(const ConfigVariableClass<T>& lhs, const T& rhs)
	{
		return lhs.getVal() == rhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator!=
		(const ConfigVariableClass<T>& lhs, const T& rhs)
	{
		return !(lhs == rhs);
	}

	//-----------------------------------------------------------------------------
	inline bool operator==
		(const ConfigVariableClass<std::string>& lhs, const char* rhs)
	{
		return !(lhs == (std::string)rhs);
	}

	//-----------------------------------------------------------------------------
	inline bool operator!=
		(const ConfigVariableClass<std::string>& lhs, const char* rhs)
	{
		return !(lhs != (std::string)rhs);
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator< (const ConfigVariableClass<T>& lhs, const ConfigVariableClass<T>& rhs)
	{
		return lhs.getVal() < rhs.getVal();
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator< (const ConfigVariableClass<T>& lhs, const T& rhs)
	{
		return lhs.getVal() < rhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator> (const ConfigVariableClass<T>& lhs, const ConfigVariableClass<T>& rhs)
	{
		return lhs.getVal() > rhs.getVal();
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator> (const ConfigVariableClass<T>& lhs, const T& rhs)
	{
		return lhs.getVal() > rhs;
	}


	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator>= (const ConfigVariableClass<T>& lhs, const ConfigVariableClass<T>& rhs)
	{
		return lhs.getVal() >= rhs.getVal();
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator>= (const ConfigVariableClass<T>& lhs, const T& rhs)
	{
		return lhs.getVal() >= rhs;
	}


	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator<= (const ConfigVariableClass<T>& lhs, const ConfigVariableClass<T>& rhs)
	{
		return lhs.getVal() <= rhs.getVal();
	}

	//-----------------------------------------------------------------------------
	template <class T>
	inline bool operator<= (const ConfigVariableClass<T>& lhs, const T& rhs)
	{
		return lhs.getVal() <= rhs;
	}

	//-----------------------------------------------------------------------------
	template <class T>
	void ConfigVariableClass<T>::print()
	{
		std::cout << "|" << fileName << "|" << section << "|" << key << "|" << defaultVal << "|" << val << "|\n";
	}

	//-----------------------------------------------------------------------------
	template <class T>
	std::ostream& ConfigVariableClass<T>::getKeyPrintString(std::ostream& stream)
	{
		stream << "[" << ((autoSynch == SYNCTYPE::AUTO_SYNC) ? "+" : "-") << "]";
		stream << "|" << val << "|";
		stream << "(" << defaultVal << ")";
		return stream;
	}


	//-----------------------------------------------------------------------------
	template <class T>
	void ConfigVariableClass<T>::printAll()
	{
		//////////////////////////////////////////////////////////////////////////////////
		varList.vars.sort([](const ConfigVariableClassInterface* a, const ConfigVariableClassInterface* b)
		{
			if (a->getFileName() < b->getFileName())
				return true;
			if (a->getFileName() == b->getFileName())
			{
				if (a->getSection() < b->getSection())
					return true;
				if (a->getSection() == b->getSection())
				{
					if (a->getKey() < b->getKey())
						return true;
					return false;
				}
				return false;
			}
			return false;
		});
		//////////////////////////////////////////////////////////////////////////////////

		std::cout << "//-----------------------------------------------------------------------------\n";

		ConfigVariableClassInterface*						lastElement = nullptr;
		std::list<ConfigVariableClassInterface*>::iterator	it = varList.vars.begin();

		while (it != varList.vars.end())
		{
			if (lastElement == nullptr || (*it)->getFileName() != lastElement->getFileName())
				std::cout << "file: \"" << (*it)->getFileName() << "\"\n";
			if (lastElement == nullptr || (*it)->getSection() != lastElement->getSection())
				std::cout << "	<" << (*it)->getSection() << ">\n";

			std::cout << "		" << (*it)->getKey() << ": ";

			std::ostream stream(nullptr);
			stream.rdbuf(std::cout.rdbuf());
			(*it)->getKeyPrintString(stream);
			std::stringbuf str;
			stream.rdbuf(&str);

			std::cout << "\n";

			lastElement = *it;
			it++;
		}

		std::cout << "//-----------------------------------------------------------------------------\n";
	};

	
	//-----------------------------------------------------------------------------
	//-----------------------------------------------------------------------------
	static void ConfigFileClassTest()
	{
		{
			std::string cfgfn = "C:\\meinedateien\\MagentaCLOUD\\BitBucket\\Tower\\TowerMain\\test.ini";

			// will not be created if not existant b/c no default is given
			cint xn(cfgfn, "Section0", "IWONTEXIST");
		
			return;

			cstring x(cfgfn, "Section2", "svar1", "svar1_cpp_default");
			cstring y(cfgfn, "Section2", "svar2", "svar2_cpp_default", SYNCTYPE::DONT_SYNC);
			cstring z(cfgfn, "Section3", "svar3", SYNCTYPE::DONT_SYNC);						


			x = x + "Hallo test";
			y = "ABC";
			x = x + " testtesttesttest";
			z = z + "test";
			std::cout << "string: " << x << " " << x + "AB" << " " << x + "CD" << " " << x + y << " " << z << "\n";

			cint xi(cfgfn, "Section1", "ivar1", 100);
			cint yi(cfgfn, "Section1", "ivar2", 200); 
			cint zi(cfgfn, "Section2", "ivar3", 300);				

			yi++;			
			xi = xi + 43;
			zi = yi * 2;
			std::cout << "int: " << xi << " " << xi + 9 << " " << xi - 3 << " " << xi + yi << " " << zi << "\n";
			
			cfloat xf(cfgfn, "Section1", "fvar1", 100.1f);
			cfloat yf(cfgfn, "Section3", "fvar2", 200.1f, SYNCTYPE::DONT_SYNC);
			cfloat zf(cfgfn, "Section3", "fvar3", 300.1f);
			
			xf++;
			yf++;
			zf = xf + yf;
			zf = zf / 1.312f;
			std::cout << "float: " << xf << " " << xf + 9.1f << " " << xf - 3.1f << " " << xf + yf << " " << zf << "\n";
			
			cbool xb(cfgfn, "Section1", "bvar1", true);
			cbool yb(cfgfn, "Section2", "bvar2");
			cbool zb(cfgfn, "Section3", "bvar3", true, SYNCTYPE::DONT_SYNC);
			
			std::cout << "booL: " << xb << " ";
			xb++;
			std::cout << xb << " ";
			xb--;
			std::cout << xb << " ";
			zb = xb + yb;
			zb = zb + (bool)TRUE;
			std::cout << zb << "\n";
			
			ConfigVariableClass<int>::printAll();
		}
	}

}