#pragma once

#define maxspieler          200	/* Maximale Anzahl von Spieler in einem Spiel */

#include "QuizStruct.h"
#include "mstring.h"

static QuizStateStruct quizStruct;


typedef struct
{
	MString Spieler;
	int Pkt;
	unsigned long time;
	unsigned long fragengehoert;
	unsigned long fragenbeantwortet;
	unsigned long Siege;
	int oep, nep;
	int team;
} punktestruct;

static punktestruct EListe[500];
static punktestruct Punkte[maxspieler];	/*Speicher f�r aktuelle Punktest�nde */
