#pragma once

#include "sleepy_discord/websocketpp_websocket.h"
#include "MessageQeueStruct.h"

#include "VariableStorageStruct.h"

//-----------------------------------------------------------------------------
class ClientClass : public SleepyDiscord::DiscordClient
{
public:
	using SleepyDiscord::DiscordClient::DiscordClient;

	void onMessage(SleepyDiscord::Message message)
	{
		messageList.pushMessage(message);
	}
};

static ClientClass *client;

//-----------------------------------------------------------------------------
static void botSay(std::string msg, ...)
{
	char *buffer = (char*)malloc(2000);

	va_list args;
	va_start(args, msg);
	vsprintf(buffer, msg.c_str(), args);
	va_end(args);

	//client->sendMessage("526560273344233485", buffer);

	try
	{
		client->sendMessage(vs.get("DiscordChannelID", "DiscordSection"), buffer);
	}
	catch (const std::exception& e) { // caught by reference to base
		std::cout << " a standard exception was caught, with message '"
			<< e.what() << "'\n";
	}

	free(buffer);
}
