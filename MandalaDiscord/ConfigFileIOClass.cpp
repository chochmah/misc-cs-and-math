#include "ConfigFileIOClass.h"


namespace cf
{
	//-----------------------------------------------------------------------------
	ConfigFileIOClass::ConfigFileIOClass(const std::string fileName_)
		: fileName(fileName_)
	{
		// open File
		this->iniReader = std::make_unique<PIniRW::PIniReadWrite>(fileName.c_str());
		this->iniWriter = std::make_unique<PIniRW::PIniReadWrite>(fileName.c_str());
	}

	//-----------------------------------------------------------------------------
	ConfigFileIOClass::~ConfigFileIOClass()
	{}

	//-----------------------------------------------------------------------------
	ConfigFileClassProxy ConfigFileIOClass::getParam(const std::string paramSection, const std::string paramName, const std::string defaultVal) const
	{
		ConfigFileClassProxy result(this, paramSection, paramName, defaultVal);
		return result;
	}

	//-----------------------------------------------------------------------------
	std::string	ConfigFileIOClass::getParamString(const std::string paramSection, const std::string paramName, const std::string defaultVal) const
	{
		return iniReader->ReadString(paramSection.c_str(), paramName.c_str(), defaultVal.c_str());
	};

	//-----------------------------------------------------------------------------
	int ConfigFileIOClass::getParamInt(const std::string paramSection, const std::string paramName, const std::string defaultVal) const
	{
		return iniReader->ReadInteger(paramSection.c_str(), paramName.c_str(), std::stoi(defaultVal));		
	}

	//-----------------------------------------------------------------------------
	float ConfigFileIOClass::getParamFloat(const std::string paramSection, const std::string paramName, const std::string defaultVal) const
	{
		return iniReader->ReadFloat(paramSection.c_str(), paramName.c_str(), std::stod(defaultVal));
	}

	//-----------------------------------------------------------------------------
	bool ConfigFileIOClass::getParamBool(const std::string paramSection, const std::string paramName, const std::string defaultVal) const
	{
		return iniReader->ReadBoolean(paramSection.c_str(), paramName.c_str(), defaultVal.c_str());
	}

		
	//-----------------------------------------------------------------------------
	void ConfigFileIOClass::setParam(const std::string paramSection, const std::string paramName, const std::string val) const
	{
		iniWriter->WriteString(paramSection.c_str(), paramName.c_str(), val.c_str());
	};
	
	//-----------------------------------------------------------------------------
	void ConfigFileIOClass::setParam(const std::string paramSection, const std::string paramName, const int val) const
	{
		iniWriter->WriteInteger(paramSection.c_str(), paramName.c_str(), val);
	}
	
	//-----------------------------------------------------------------------------
	void ConfigFileIOClass::setParam(const std::string paramSection, const std::string paramName, const float val) const
	{
		iniWriter->WriteFloat(paramSection.c_str(), paramName.c_str(), val);
	}

	//-----------------------------------------------------------------------------
	void ConfigFileIOClass::setParam(const std::string paramSection, const std::string paramName, const bool val) const
	{
		iniWriter->WriteBoolean(paramSection.c_str(), paramName.c_str(), val);
	}
}