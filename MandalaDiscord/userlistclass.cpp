#include "pch.h"
#include "userlistclass.h"
#include <math.h>

UserListClass::UserListClass() {
}

void UserListClass::clearall() {
     userelements.clear();
}

userstruct UserListClass::Zahlenratenaufloesen(float Zielwert) 
{
  userstruct rep;           
  rep = *userelements.begin();
  std::vector<userstruct>::iterator it;    
  for(it = userelements.begin(); it != userelements.end(); it++) {       
      userstruct wle;
      wle=*it;
      if (fabs(wle.Pkt-Zielwert) < fabs(rep.Pkt-Zielwert)) rep = wle;
  }
  return(rep);
}

int UserListClass::CheckDatenOrdnenRichtig(MString loesung, MString *result,void (*gibpunktefkt)(MString Sender,int pkt)) {
  int numofrichtig=0;
  std::vector<userstruct>::iterator it;    
  for(it = userelements.begin(); it != userelements.end(); it++) {       
      userstruct wle;
      wle=*it;
      if (wle.Antwort.klein()==loesung.klein()) {*result+=wle.Spieler;*result+=" ";wle.richtig=1;numofrichtig++;gibpunktefkt(wle.Spieler,4);}else wle.richtig=0;
//    printf(" [%s][%s]\n",wle.Antwort.stt.c_str(),wle.Spieler.stt.c_str());    
  }      
  return(numofrichtig);
}


int UserListClass::AddUserZahlenraten(MString Spieler, float Antwort, float Zielwert) 
{
    if (Antwort == 0) 
		return(0);    
    std::vector<userstruct>::iterator it;    
    for(it = userelements.begin(); it != userelements.end(); it++) 
	{       
		userstruct wle;
		wle=*it;
		if (wle.Spieler==Spieler) return(0);
    }      
    userstruct wle;    
    wle.Spieler = Spieler;
    wle.Pkt = Antwort;  
    userelements.push_back(wle);           
//  printf("%f %f\n",Antwort,Zielwert);    
    if (Antwort==Zielwert) 
		return(1);
    return(0);
}

int UserListClass::AddUserDatenOrdnen(MString Spieler, MString Antwort) {
  std::vector<userstruct>::iterator it;    
  for(it = userelements.begin(); it != userelements.end(); it++) {       
      userstruct wle;
      wle=*it;
      if (wle.Spieler==Spieler) return(0);
  }      
                   Antwort=Antwort.klein(); 	 
                   Antwort.erase(" ");              
//                 printf("gefundenpre: %s\n",Antwort.stt.c_str());                  
                   if (Antwort.strlength()!=3) return(0);
                   if (!Antwort.includes("a")) return(0);
                   if (!Antwort.includes("b")) return(0);
                   if (!Antwort.includes("c")) return(0);  
//                 printf("gefunden und gespeichert: [%s] [%s]\n",Spieler.stt.c_str(),Antwort.stt.c_str());                                    
      userstruct wle;    
      wle.Spieler = Spieler;
      wle.Antwort = Antwort;  
      userelements.push_back(wle);           
      return(1);
}
