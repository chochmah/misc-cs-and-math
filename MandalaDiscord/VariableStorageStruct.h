#pragma once

#include <map>
#include "ConfigFileClass.h"


//-----------------------------------------------------------------------------
static struct VariableStorageStruct
{	
	std::string cfPath = "C:\\meinedateien\\MagentaCLOUD\\BitBucket\\CSandMATHproblems\\Misc CS and Math Problems\\MandalaDiscord\\QuizConfig.ini";

	std::map<std::string, cf::cstring> vMap;	
public:
	VariableStorageStruct() {};
	~VariableStorageStruct() {};

	//-----------------------------------------------------------------------------
	std::string get(const std::string vName, const std::string vSection)
	{
		do {
			auto it = vMap.find(vName);
			if (it == vMap.end())
				// key doesnt exist yet, create new config file mapped variable			
				vMap.insert(std::pair<std::string, cf::cstring>(vName, cf::cstring(cfPath, vSection, vName, "DefaultDefault")));
			else
				// key does exist, return variable
				return it->second;
		} while (true);
	}

	//-----------------------------------------------------------------------------
	std::string get(const std::string vName)
	{
		return get(vName, "DefaultSection");
	}

	//-----------------------------------------------------------------------------
	// Drop all mappings. Reread them from file. Usefull when ini file has been 
	// changed manually

	void flushMappings()
	{
		vMap.clear();
	}

} vs;