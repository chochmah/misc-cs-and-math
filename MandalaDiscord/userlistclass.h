#ifndef UserListClass_H
#define UserListClass_H
#include "mstring.h"
#include <vector>   

  typedef struct {
    MString Antwort;
	MString Spieler;
	float Pkt;
	bool richtig;
  } userstruct;

class UserListClass
{
private:
        
public:
  std::vector<userstruct> userelements;     
  userstruct Zahlenratenaufloesen(float Zielwert);       
  int CheckDatenOrdnenRichtig(MString loesung, MString *result,void (*gibpunktefkt)(MString Sender,int pkt));
  int AddUserZahlenraten(MString Spieler, float Antwort, float Zielwert);
  int AddUserDatenOrdnen(MString Spieler, MString Antwort);     
  UserListClass(); 
  void clearall();
  int size(){return((int)userelements.size());}
};

#endif 
