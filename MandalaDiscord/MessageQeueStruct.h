#pragma once

#include <vector>
#include "sleepy_discord/websocketpp_websocket.h"
#include <mutex>

//-----------------------------------------------------------------------------
struct MessageListStruct
{
private:
	std::vector<SleepyDiscord::Message> qeue;
	std::mutex qeueLock;

public:
	//-----------------------------------------------------------------------------
	void pushMessage(const SleepyDiscord::Message& msg)
	{
		qeueLock.lock();
		qeue.push_back(msg);
		qeueLock.unlock();
	}

	//-----------------------------------------------------------------------------
	bool getMessage(SleepyDiscord::Message& msg)
	{
		qeueLock.lock();
		if (!qeue.size())
		{
			qeueLock.unlock();
			return false;
		}
		else
		{
			msg = qeue.back();
			qeue.pop_back();
			qeueLock.unlock();
			return true;
		}
	}
};

static MessageListStruct messageList;

