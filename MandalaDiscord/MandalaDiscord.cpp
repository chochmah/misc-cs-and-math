#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h> 
#include <crtdbg.h>  

#include "pch.h"
#include <iostream>

#include "mstring.h"

#include "ClientClass.h"



#define DEBUGOUTPUT		1

#include "..\PIniReadWrite\PIniReadWrite.h"


#include <windows.h>
#include <winsock.h>
#include <stdio.h>
#include <stdlib.h>
#include "chatbot.h"
#include <vector>   
#include <math.h>
#include <stdarg.h>
#include <sys/types.h>
//#include <unistd.h>
#include <time.h>
#include <pthread.h>
//#include <ftplib.h>
#include <iostream>
#include "befehle.h"
#include "userlistclass.h"
#include <string.h>

#include <algorithm>    // std::random_shuffle

//-----------------------------------------------------------------------------
bool CheckIfFragenDatabaseEmpty()
{
	auto sqlReply = mySQL.sendQuery("select * from fragen limit 1;");

	if (sqlReply.count() == 0)
	{		
		botSay(vs.get("NoQuestionsInDataBase"));
		quizStruct.HaltQuiz();
		return 1;
	}

	return 0;
}

//-----------------------------------------------------------------------------
//	Alle Fragen gestellt, "gestellt"-flag  wieder auf 0 bei allen fragen setzen

void FragenKatalogReset() 
{	
	botSay(vs.get("AllQuestionsAskedResetLib"));
	mySQL.sendQuery("update fragen set gestellt=0;");
	//NOTE: Cant remember what this does
	mySQL.sendQuery(vs.get("SQLUpdateQuestionType"));
	//NOTE: Old Note: Schwierigkeitsgrad neu betimmen muß hier
	//Sicherheitshalber falls aus irgendeinem Grund keine Treffer zum Schlagwort gefunden werden
	quizStruct.themenRunde = 0; 	
}

//-----------------------------------------------------------------------------
void NeueFrageStellenSQL() 
{
nochmal:
	weiterlist.neuefrage();

	if (!CheckIfFragenDatabaseEmpty()) 
	{
		quizStruct.quizType = QuizStateStruct::QuizTypes::standart;
		if (abcfragenhaeufigkeit != 0) 
			if ((rand() % abcfragenhaeufigkeit) == 0) 
				quizStruct.quizType = QuizStateStruct::QuizTypes::datenordnen;

#ifdef DEBUGOUTPUT
	printf("quiztyp: %i\n", quizStruct.quizType);
#endif

		if (quizStruct.quizType == QuizStateStruct::QuizTypes::datenordnen)
		{
			int richtjahr = rand() % 2000;
			
			//TODO: fix query so 3 or less results are returned
			mySQL_RES	res		= mySQL.sendQuery(vs.get("SQLSelect3DataSortQuestionsOptions"), richtjahr );
			
			if (res.count() > 2)
			{				
				// randomize answer options
				std::vector<int> c { 0,1,2 };
				std::random_shuffle(c.begin(), c.end());

				for (int i = 0; i < 3; i++) 
				{
					mySQL_ROW row = res.rows[i];
					datenspeicher[c[i]].set((int)strtod(row[1].c_str(), nullptr), row[0].c_str(), row[2].c_str());
				}

				botSay(vs.get("SortYearDatesAnnouncement"));

				for (int i = 0; i < 3; i++) 
				{
					std::string p = textformat.leadstring;
					switch (i)
					{
					case 0:	p += " ; (_A_) ;"; break;
					case 1: p += " ; (_A_) ;"; break;
					case 2: p += " ; (_C_) ;"; break;
					}
					botSay(p + datenspeicher[i].frage.stt.c_str() + " [" + datenspeicher[i].ID.stt.c_str() + "]");
				}

				quizStruct.aktuelleFrageIstBeantwortet = 0;
			}	
		}

		if (quizStruct.quizType == QuizStateStruct::QuizTypes::standart)
		{					
			mySQL_RES	res;

			if (quizStruct.themenRunde == 0) 
			{
				if ((letzteAntwort == "") || (!zusammenhaengendefragen)) 					
					res = mySQL.sendQuery("select * from fragen where gestellt=0 order by rand() limit 1;");
				else 
					res = mySQL.sendQuery("select * from fragen where gestellt=0 and Frage like '%%%s%%' order by rand() limit 1;", letzteAntwort.stt.c_str());				
			}
			else 
				res = mySQL.sendQuery("select * from fragen where gestellt=0 and Frage like '%%%s%%' order by rand() limit 1;", themenschlagwort);
			
			if (res.count() == 0)
			{
				if (zusammenhaengendefragen) 
				{ 
					letzteAntwort = ""; 
					goto nochmal; 
				}
				if (quizStruct.themenRunde) 
					quizStruct.themenRunde = 0;
				else 
					FragenKatalogReset();
				goto nochmal;
			}
			
			mySQL_ROW row = res.rows[0];

			int diff;
			char * pEnd;

			AFrage.Autor	= row[0].c_str();
			AFrage.Frage	= row[1].c_str();
			AFrage.Antwort	= row[2].c_str();
			AFrage.typ		= int(strtod(row[3].c_str(), &pEnd));
			AFrage.ID		= int(strtod(row[4].c_str(), &pEnd));
			diff			= int(strtod(row[6].c_str(), &pEnd));
			letzteAntwort = AFrage.Antwort;
			

			res = mySQL.sendQuery("update fragen set count=count+1,gestellt=1 where ID=%s;", row[4].c_str());

			MakeTipp(AFrage.Antwort.strlength());
			if (AFrage.typ == 1) 
			{
				botSay(vs.get("ProximityQuestionAnnouncement"));
				Zahlenraten		= 1;
				zeitfuerrunde	+= 15;
			}

			std::string difficulty = vs.get("DifficultyMissing");
			if (diff > 0)
			{				
				if ((diff > 0)		&& (diff < 7))		difficulty = vs.get("DifficultyVeryEasy");
				if ((diff >= 7)		&& (diff < 15))		difficulty = vs.get("DifficultyEasy"); 
				if ((diff >= 15)	&& (diff < 50))		difficulty = vs.get("DifficultyNormal"); 
				if ((diff >= 50)	&& (diff < 90))		difficulty = vs.get("DifficultyHard"); 
				if ((diff >= 90)	&& (diff <= 100))	difficulty = vs.get("DifficultyVeryHard"); 
			}

			char buffer[2000];
			std::sprintf(buffer, vs.get("SQLAskNormalQuestion").c_str(), 
				textformat.leadstring.c_str(), std::to_string(quizStruct.frageinrunde + 1).c_str(), std::to_string(FragenproRunde).c_str(),
				(AFrage.Autor != "") ? AFrage.Autor.stt.c_str() : "-",
				std::to_string(AFrage.ID).c_str(), difficulty.c_str(),
				AFrage.Frage.stt.c_str()
			);

			botSay(buffer);

			if (diff > 0) if ((diff >= 90) && (diff <= 100)) 
			{				
				//TODO: Add Tipp permutation for really hard questions!
			}

			quizStruct.aktuelleFrageIstBeantwortet = 0;
		}
	}
}

//-----------------------------------------------------------------------------
bool stringBeginsWith(std::string str, std::string substr)
{
	return !std::strncmp(str.c_str(), substr.c_str(), strlen(substr.c_str()));
}

//-----------------------------------------------------------------------------
void workquerrystack()
{
	for (int i=0;i<stack.getStackElements().size();i++)
	{
		StackClass::stackstruct st = stack.getStackElements()[i];
		
		// possibly depreciated
		if (st.S1 == "!neuerimsep")		comneuerimsep(st.S2.c_str());
		if (st.S1 == "!verlaesstsep")	comverlaesstsep(st.S2.c_str());
		if (stringBeginsWith(st.S2, "!remfromsperrliste ")) remfromsperrliste(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, "!addtosperrliste ")) addtosperrliste(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, "!reconnect")) reconnect(st.S1.c_str());

		//-----------------------------------------------------------------------------		
		// internal commands
		if (stringBeginsWith(st.S2, "WUNSCHLISTEAUSWERTEN1")) WunschlisteAuswerten();
		if (stringBeginsWith(st.S2, "NEUEFRAGE1")) NeueFrageStellenSQL();
		if (stringBeginsWith(st.S2, "WRITE2LOG")) write2log(st.S1.c_str(), st.i1, st.i2);

		//-----------------------------------------------------------------------------		
		if (stringBeginsWith(st.S2, vs.get("CmdDelete") + " "))				comdelete(st.S1, st.S2);
		if (stringBeginsWith(st.S2, vs.get("CmdStartQuiz")))				comstartquiz(st.S1.c_str(), st.S2.c_str());		
		if (stringBeginsWith(st.S2, vs.get("CmdLastQuestion")))				frage(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSetSize")))					setsize(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSetSpeed")))					setspeed(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSetABCFragenFreq")))			setabcfragen(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdHowMany")))					howmanymain(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdFlush")))					flush(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdLern")))						LernFrageSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdThema")))					SetFrageThema(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSetInterQuestionDelay")))	invite(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSetStragglerDelay")))		setzwischenfragendelay(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSetZusammenhaengendefragen"))) setzusammenhaengendefragen(st.S1.c_str(), st.S2.c_str());

		if (stringBeginsWith(st.S2, vs.get("CmdRevolt")))					revolt(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdAddop")))					addOP(st.S1.c_str(), st.S2.c_str());		
		if (stringBeginsWith(st.S2, vs.get("CmdEdit")))						EditFrageSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdLaudatio ")))				LernGlueckwunschSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdStats")))					showstats(st.S1.c_str());		
		if (stringBeginsWith(st.S2, vs.get("CmdWeiter")))					weiter(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdEstatus")))					ShowEwigeListeSQLuser(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdEndquiz")))					comendquiz(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdStatus")))					ShowRanking(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdTstatus")))					ShowTagesRanking(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSuche")))					sucheSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdSay")))						say(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdHelp")))						displayhelp(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdDeletelast")))				deletelast(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdZeigemeinefragen")))			zeigemeinefragenSQL(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdShutdown")))					Shutdown(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdVote")))						voteSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdVeto")))						setveto(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdWait")))						setwait(st.S1.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdWunsch")))					wunschSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdTransfer")))					TransferSQL(st.S1.c_str(), st.S2.c_str());
		if (stringBeginsWith(st.S2, vs.get("CmdComment")))					comment(st.S1.c_str(), st.S2.c_str());		
		
	}
	stack.clearall();
}

//-----------------------------------------------------------------------------
void Rundebeenden() 
{
	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	int hour = timeinfo->tm_hour;

	quizStruct.frageinrunde = 0;
	Punkte[rangliste[0]].Siege++;

	bool keinglueckwunsch = 0;

	mySQL_ROW row;	

	mySQL_RES res = mySQL.sendQuery(vs.get("SQLGetCongratulation"));

	if (MySQLAbstractionLayerClass::num_rows(&res) == 0)
		keinglueckwunsch = 1;
	else 
		row = res.rows[0];

	char buffer[2000];
	sprintf(buffer, vs.get("PriceGivenBy").c_str(), row[0].c_str(), row[1].c_str());

	botSay(vs.get("QuizEndedWinnerIs"), tippstring().c_str(), Punkte[rangliste[0]].Spieler.stt.c_str(),
		(!keinglueckwunsch) ? buffer : "" );

	ShowRankingAll();
	Sleep(1000);
	scoreLists.UpdateEwigeListeSQL();
	scoreLists.ShowEwigeListeSQL();
	quizStruct.delayneuesquiz = 1;
	scoreLists.ShowEwigeListeChange();
	
	std::string						ranking = vs.get("BadResult");
	if (zeitfuerrunde <=	3500)	ranking = vs.get("AverageResult");
	if (zeitfuerrunde <		3500)	ranking = vs.get("GoodResult");

	botSay(vs.get("RoundDurationAnnouncement"), std::to_string((int)(zeitfuerrunde / 10)).c_str(), ranking.c_str());

	scoreLists.ShowTagesListeSQL();
	quizStruct.spielerAnzahl = 0;

	if (!zeitautomatik || hour < inibotautoquittime)
		botSay(vs.get("NextQuizSoonAnnouncement"), iniftpwebpage.c_str());
	else 
	{
		botSay("BotGoesOnSceduledSleep", std::to_string(hour).c_str());		
		Shutdown();
	}

	WriteEwigeListeHTML();
}

//-----------------------------------------------------------------------------
void TestGesilenct(messagestruct ms)
{
	//TODO: probably deprec
	if (ms.name == inibotName) { gesilenct = 0; printf("Antwort bekommen\n"); }
}

//-----------------------------------------------------------------------------
void ParseChatLine(messagestruct ms)
//ParseChatLine (int flag, MString col,MString Sender, MString Message)
{
	int flag		= ms.flag;
	MString col		= ms.col;
	MString Sender	= ms.name;
	MString Message = ms.message;

	//-----------------------------------------------------------------------------		
	
	if									(Message.beginswith(vs.get("CMDDebug")))						debug = !debug;
	if									(Message.beginswith(vs.get("CMDEchtzeitrangliste")))			echtzeitrangliste = !echtzeitrangliste;
	if									(Message.beginswith(vs.get("CMDZeitautomatik")))				zeitautomatik = !zeitautomatik;

	//-----------------------------------------------------------------------------
	if ((userinteraktion) &&			(Message.beginswith(vs.get("CmdDelete") + " ")))				stack.InsertStack(Sender, Message);
	if ((userinteraktion) &&			(Message.beginswith(vs.get("CmdStartQuiz"))))					stack.InsertStack(Sender, Message);	
	if ((userinteraktion) &&			(Message.beginswith(vs.get("CmdLastQuestion"))))				stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSetSize")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSetSpeed")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSetABCFragenFreq")))				stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdHowMany")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdFlush")))						stack.InsertStack(Sender, Message);
	if ((inicomlern) &&					(Message.beginswith(vs.get("CmdLern"))))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdThema")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSetInterQuestionDelay")))		stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSetStragglerDelay")))			stack.InsertStack(Sender, Message);

	if ((userinteraktion) &&			(Message.beginswith(vs.get("CmdInvite"))))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSetZusammenhaengendefragen")))	stack.InsertStack(Sender, Message);
	if ((userinteraktion) &&			(Message.beginswith(vs.get("CmdEndquiz"))))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdSay")))							stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdEdit")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdStats")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdWeiter")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdEstatus")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdStatus")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdTstatus")))						stack.InsertStack(Sender, Message);
	if ((inicomsuche) &&				(Message.beginswith(vs.get("CmdSuche"))))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdHelp")))							stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdDeletelast")))					stack.InsertStack(Sender, Message);
	if ((inicomzeigemeinefragen) &&		(Message.beginswith(vs.get("CmdZeigemeinefragen"))))			stack.InsertStack(Sender, Message);
	if ((inicomvote) &&					(Message.beginswith(vs.get("CmdVote"))))						stack.InsertStack(Sender, Message);
	if ((quizStruct.delayneuesquiz) &&	(Message.beginswith(vs.get("CmdVeto"))))						stack.InsertStack(Sender, Message);
	if ((quizStruct.delayneuesquiz) &&	(Message.beginswith(vs.get("CmdWait"))))						stack.InsertStack(Sender, Message);
	if ((inicomwunsch) &&				(Message.beginswith(vs.get("CmdWunsch"))))						stack.InsertStack(Sender, Message);
	if ((inicomcomment) &&				(Message.beginswith(vs.get("CmdComment"))))						stack.InsertStack(Sender, Message);	
	if									(Message.beginswith(vs.get("CmdTransfer")))						stack.InsertStack(Sender, Message);
	if ((inicomlaudatio) &&				(Message.beginswith(vs.get("CmdLaudatio"))))					stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdShutdown")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdAddop")))						stack.InsertStack(Sender, Message);
	if									(Message.beginswith(vs.get("CmdRevolt")))						stack.InsertStack(Sender, Message);

	//-----------------------------------------------------------------------------
	// possibly depreciated
	if (Message.beginswith("!reconnect")) stack.InsertStack(Sender.stt.c_str(), Message.stt.c_str());
	if (Message.beginswith("!remfromsperrliste")) stack.InsertStack(Sender.stt.c_str(), Message.stt.c_str());
	if (Message.beginswith("!addtosperrliste")) stack.InsertStack(Sender.stt.c_str(), Message.stt.c_str());
	if ((flag == 1) && (Message.includes("betritt das Separee"))) stack.InsertStack("!neuerimsep", Message.stt.c_str());
	if ((flag == 1) && (Message.includes("verlässt das Separee"))) stack.InsertStack("!verlaesstsep", Message.stt.c_str());


	if ((flag == 3 || flag == 0) && quizStruct.quizIsRunning && !quizStruct.delayneuesquiz)
	{
		if ((flag != 3) && (quizStruct.frageVonBotGeloest == 1) && (quizStruct.niemandSpieltMit == inifragenbisquizstopbeikeinerantwort))
		{
			quizStruct.niemandSpieltMit		= 0;
			quizStruct.frageVonBotGeloest	= 0;
			quizStruct.neueFrage			= 1;
			quizStruct.af++;
			quizStruct.frageinrunde++;

			botSay(vs.get("BotSolvesQuestion"), tippstring().c_str());

			stack.InsertStack("", "NEUEFRAGE1");
		}
		
		MString Xantwort;
		Xantwort = (AFrage.Antwort.klein());  //holger1
		Xantwort.erase("'");
		Xantwort.erase(" ");
		Xantwort.erase("-");
		Xantwort.erase(".");
		Xantwort.erase(",");
		Xantwort.erase("/");
		Xantwort.erase("%");
		Xantwort.keineumlaute();

		MString Xuser;		
		Xuser = Message.klein();  
		Xuser.erase("'");
		Xuser.erase(" ");
		Xuser.erase("/");
		Xuser.erase("-");
		Xuser.erase(".");
		Xuser.erase(",");
		Xuser.erase("%");
		Xuser.keineumlaute();		

		switch (quizStruct.quizType)
		{
		case QuizStateStruct::QuizTypes::datenordnen:
			userbuffer.AddUserDatenOrdnen(Sender, Message);
			//TODO: This halts the round when everybody in the chat has answered
			//if (userbuffer.size() == ChatBot.ChatterSepList.ChatterAngemeldet()) 
			//	quizStruct.rateTimer = 300;
			break;
		case QuizStateStruct::QuizTypes::standart:
			if (flag == 0)
			{
				if ((Zahlenraten)) 
				{
					if (userbuffer.AddUserZahlenraten(Sender, (float)atof(Xuser.stt.c_str()), (float)atof(Xantwort.stt.c_str())))
						quizStruct.rateTimer = 300;
					//TODO: When as many people in sep as have guessed end question
					//if (userbuffer.size() == ChatBot.ChatterSepList.ChatterAngemeldet()) quizStruct.rateTimer = 300;
				}
				if ((!Zahlenraten) && (flag != 1) && (quizStruct.aktuelleFrageIstBeantwortet == 0))
				{
					if (!(Xuser.includes(Xantwort)))
					{
						//frage falsch beantwortet, antwort als mc speichern
						fragestruct::SaveMCAntwort(Message, AFrage.ID);
					}
					else
					{		//Frage richtig beantwortet
						if ((warteaufnachzuegler))
						{
							//nachzuegler eintragen
							if (Sender != warteaufnachzueglerersterspieler)userbuffer.AddUserZahlenraten(Sender, (float)warteaufnachzueglertimer, 0.0f);
						}

						if (!warteaufnachzuegler)
						{
							userbuffer.clearall();
							stack.InsertStack(Sender.stt.c_str(), "WRITE2LOG", AFrage.ID, quizStruct.zeitmesser / 10);
							warteaufnachzuegler = 1;
							warteaufnachzueglertimer = 0;
							warteaufnachzueglerersterspieler = Sender;
							//Warte auf nachzügler
						}
					}
				}
			}
			break;
		}
	}
}

//-----------------------------------------------------------------------------
void writeinifile() 
{
	PIniRW::PIniReadWrite iniWriter(".\\quiz.ini");		
	iniWriter.WriteString("GeneralSetting", "BotName", "mandala");
	iniWriter.WriteString("GeneralSetting", "BotPass", "---");
	iniWriter.WriteString("GeneralSetting", "InitialChannel", "Muenchen");
	iniWriter.WriteString("GeneralSetting", "SepName", "---");
	iniWriter.WriteString("SQLSetting", "Databasename", "testuserfragen");
	iniWriter.WriteString("FTPSetting", "ftpServer", "web7.webmailer.de");
	iniWriter.WriteString("FTPSetting", "ftpName", "www.ph2.net");
	iniWriter.WriteString("FTPSetting", "ftpPass", "---");
	iniWriter.WriteString("FTPSetting", "ftpDir", "Fragen");
	iniWriter.WriteBoolean("GeneralSetting", "StartQuizOnExec", true);
	iniWriter.WriteString("SQLSetting", "sqlIP", "127.0.0.1");
	iniWriter.WriteInteger("SQLSetting", "sqlPort", 3306);
}

//-----------------------------------------------------------------------------
void readinifile() 
{
	PIniRW::PIniReadWrite iniReader(".\\quiz.ini");
	iniautokicknicksonsperrlist = iniReader.ReadInteger("GeneralSetting", "autokicknicksonsperrlist", 0);
	iniautoaddtosperrlistonspaming = iniReader.ReadInteger("GeneralSetting", "autoaddtosperrlistonspaming", 0);

	inibotName = iniReader.ReadString("GeneralSetting", "BotName", "");
	inibotPass = iniReader.ReadString("GeneralSetting", "BotPass", "");
	inibotInitialChannel = iniReader.ReadString("GeneralSetting", "InitialChannel", "");
	iniSepName = iniReader.ReadString("GeneralSetting", "SepName", "");
	iniftpwebpage = iniReader.ReadString("FTPSetting", "ftpWebpage", "FEHLER");
	iniftpServer = iniReader.ReadString("FTPSetting", "ftpServer", "");
	iniftpName = iniReader.ReadString("FTPSetting", "ftpName", "");
	iniftpPass = iniReader.ReadString("FTPSetting", "ftpPass", "");
	iniftpDir = iniReader.ReadString("FTPSetting", "ftpDir", "");
	iniStartQuizOnExec = iniReader.ReadInteger("GeneralSetting", "StartQuizOnExec", 1);
	inisqlIP = iniReader.ReadString("SQLSetting", "sqlIP", "127.0.0.1");

	inisqlPort = iniReader.ReadInteger("SQLSetting", "sqlPort", 3306);
	inibotauto_quit = iniReader.ReadInteger("BotSetting", "botauto_quit", 0);
	inicomlaudatio = iniReader.ReadInteger("BotSetting", "comlaudatio", 1);
	inicomlern = iniReader.ReadInteger("BotSetting", "comlern", 1);
	inicomsuche = iniReader.ReadInteger("BotSetting", "comsuche", 1);
	warteaufnachzueglerdelay = iniReader.ReadInteger("BotSetting", "warteaufnachzueglerdelay", 750);
	abcfragenhaeufigkeit = iniReader.ReadInteger("BotSetting", "abcfragenhaeufigkeit", 40);
	zusammenhaengendefragen = iniReader.ReadInteger("BotSetting", "zusammenhaengendefragen", 1);
	inicomzeigemeinefragen = iniReader.ReadInteger("BotSetting", "comzeigemeinefragen", 1);
	inicomvote = iniReader.ReadInteger("BotSetting", "comvote", 1);
	inifragenbisquizstopbeikeinerantwort = iniReader.ReadInteger("BotSetting", "inifragenbisquizstopbeikeinerantwort", 2);
	inicomwunsch = iniReader.ReadInteger("BotSetting", "comwunsch", 1);
	zwischenfragendelay = iniReader.ReadInteger("BotSetting", "zwischenfragendelay", 1000);
	inicomcomment = iniReader.ReadInteger("BotSetting", "comcomment", 1);
	inibotautoquittime = iniReader.ReadInteger("BotSetting", "botauto_quittime", 22);
	inibotquizsize = iniReader.ReadInteger("BotSetting", "botquizsize", 20);
	iniAddSepNumbers = iniReader.ReadInteger("GeneralSetting", "AddSepNumbers", 0);
	zeitautomatik = inibotauto_quit;
	FragenproRunde = inibotquizsize;
	//iniWriter.WriteFloat("generalsetting", "Height", 1.82f); 
}

//-----------------------------------------------------------------------------
void *MandalaMainLoop(void *threadid)
{

//neuverbinden:
	/*

	do {
		printf("neuverbinden\n");
		keepalive = 0;
		ChatBot.errorcode = 0;



		printf("%s %s %s\n", inibotName, inibotPass, inibotInitialChannel);
		ChatBot.login(inibotName, inibotPass, inibotInitialChannel);
		Sleep(1000);
		if (!ChatBot.errorcode) {
			if (!quizon) {
				frageinrunde = 0;
				quizStruct.neueFrage = 0;
				af = 0;
			}
			neuessepauf();
			botSay("/messageon");
			if (!quizon) if (iniStartQuizOnExec) { startthequiz(); quizon = 1; }
			goto keinproblem;
		}
		printf("%s\n", ChatBot.ErrorMsg.stt.c_str());
		if (ChatBot.errorcode) printf("Errorcode: %s\n", ChatBot.ErrorMsg.stt.c_str());
		if (ChatBot.ErrorMsg.includes("Chat verwiesen worden\n")) {
			printf("Chatverweis, neuer Verbindungsversuch in 60 Sekunden\n");
			Sleep(60000);
		}
		if (ChatBot.ErrorMsg.includes("Zerolengthreturn")) {
			printf("Nulllängenantwort vom Sever, verbinde neu\n");
			Sleep(1000);
		}
		if ((ChatBot.ErrorMsg.includes("Call to connect()")) && (ChatBot.errorcode != 10061)) {
			printf("Neuer Verbindungsversuch in 10 Sekunden\n");
			Sleep(10000);
		}
		if ((1) && (ChatBot.errorcode == 10061)) {
			printf("Host ist Down, Neuer Verbindungsversuch in 1 Minute\n");
			Sleep(60000);
		}
	} while (ChatBot.errorcode);
	*/


//keinproblem:


	keepalive = 1;
	printf("Connected\n");
	keepalive = 1;
	int intcounter = 0;
	int checkconcounter = 0;
	int checkint = 0;



	while (1)
	{
		//checkconcounter++;
		////  checkint++;

		////  if (checkint>600) {checkaufopsilenced();checkint=0;}

		//if (checkconcounter > 10) {
		//	if ((keepalive) && (!ChatBot.errorcode)) {
		//		time_t seconds2; seconds2 = time(NULL);
		//		if ((seconds2 - ChatBot.timeoflastrec()) >= 60) { botSay("/away"); botSay("/alive");  printf("conscheck idletime:%i\n", seconds2 - ChatBot.timeoflastrec()); }
		//		if ((seconds2 - ChatBot.timeoflastrec()) > 80) {
		//			printf("Connection lost by concheck... (%i)\n", seconds2 - ChatBot.timeoflastrec());
		//			ChatBot.errorcode = 1;
		//		}
		//	}
		//	checkconcounter = 0;
		//}

		timer++;
		if (warteaufnachzuegler) 
			warteaufnachzueglertimer += 100;
		//        for (int i=0;i<10;i++) {/*printf("in");*/ChatBot.readchatline (pfi);Sleep(100);/*printf("out");*/}

		SleepyDiscord::Message msg;		
		if (messageList.getMessage(msg))
		{
			messagestruct ms;
			ms.col = "000000";

			/*
				flags are probably:
				0 normal
				1 wenlock personal information
				2
				3 flüstern
				4 im /me geschrieben
				5 possibly italic
				10 chatverweis
			*/

			ms.flag = 0;
			ms.gender = 0;
			ms.message = msg.content.c_str();
			
			auto user = msg.author;					

			ms.name = (user.username + ((std::string)("#")) + user.discriminator).c_str();

			// TODO: change to Name from inifile or receive own name per discord API (better)
			if (ms.name == "Mandala Quizbot#4324")
				ms.flag = 2;

			std::cout << msg.channelID.number() << " " <<  ms.flag << " " << ms.name.stt.c_str() << " " << ms.message.stt.c_str() << "\n";

			ParseChatLine(ms);
					
		}
		//ChatBot.readchatline(pfi); 
		Sleep(100);
		
		/*
		if (ChatBot.errorcode) 
		{ 
			printf("CBERRORCODE cought\n"); 
			goto neuverbinden; 
		}
		
		if (intcounter > 10 && !ChatBot.isinsep)
		{ 
			ChatBot.errorcode = 10; 
			goto neuverbinden; 
		}*/

		
		switch(quizStruct.quizType)
		{
		case QuizStateStruct::QuizTypes::datenordnen:
			quizStruct.rateTimer++;

			if (quizStruct.rateTimer == 250) 
				botSay(vs.get("FiveSecondsInRoundWarning"), tippstring().c_str());
			if (quizStruct.rateTimer >= 300)
			{ 
				//Datumsspiel Auflösen
				//TODO: sort algorithm
				std::vector<int> c{0,1,2};

				for (int i = 0; i < 3; i++)
					for (int l = i + 1; l < 3; l++)
						if (datenspeicher[c[i]].datum > datenspeicher[c[l]].datum) 
						{
							int tmp = c[i];
							c[i] = c[l];
							c[l] = tmp;
						}

				MString loesungsstring;
				for (int i = 0; i < 3; i++)
					loesungsstring += (c[i] == 0) ? "A" : (c[i] == 1) ? "B" : "C";
				
				MString p = tippstring().c_str();
				p += " Loesung: _";
				for (int i = 0; i < 3; i++)
					loesungsstring += (c[i] == 0) ? "__ A:__" : (c[i] == 1) ? "__ B:__" : "__ C:__" + datenspeicher[c[i]].datum;

				p += "_ (Antwort war: _";
				p += loesungsstring;
				p += "_). ";

				MString result = "";
				int numofrichtig = userbuffer.CheckDatenOrdnenRichtig(loesungsstring, &result, GibPunkte);
				//Gibpunkte liegt in der DatenOrdnenFunktion
				if (numofrichtig > 0) 
				{
					if (numofrichtig > 1) p += "Richtig lagen: "; else p += "Richtig lag nur ";
					p += result;
					if (numofrichtig > 1) p += " jeweils _4_ Punkte"; else p += " und bekommt _4_ Punkte";
					quizStruct.niemandSpieltMit = 0;
					//    					Sleep (100);
				}
				else {
					p += "_Niemand_ lag richtig.";
				}
				//                    printf("richtige antwort: [%s] anzahl rater: %i\n",loesungsstring.stt.c_str(), rater);
				userbuffer.clearall();
				botSay(p.stt.c_str());
				
				quizStruct.QuestionHasBeenAnswered();
				quizStruct.AskAnotherQuestion();

				Sleep(zwischenfragendelay);
			}
			break;
		case QuizStateStruct::QuizTypes::standart:
			if ((Zahlenraten)) 
			{
				quizStruct.rateTimer++;
				std::string Sieger;
				float SiegerPkt=0;
				if (quizStruct.rateTimer == 100) 
					botSay(vs.get("FiveSecondsInRoundWarning"), tippstring().c_str());
				if (quizStruct.rateTimer >= 150)
				{
					quizStruct.rateTimer = 0;

					if (userbuffer.size() > 0)
					{
						float ant = AFrage.Antwort; 
						userstruct rep = userbuffer.Zahlenratenaufloesen(AFrage.Antwort);

						if (fabs(ant - rep.Pkt) != 0) 
						{							
							botSay(vs.get("NextToNumberWas"), tippstring().c_str(), std::to_string(ant).c_str(), rep.Spieler.stt.c_str(), std::to_string(fabs(ant - rep.Pkt)).c_str(), FolgeFragen(rep.Spieler).c_str());
							GibPunkte(rep.Spieler, 3);
						}
						else 
						{
							botSay(vs.get("NumberWasExactRight"), tippstring().c_str(), rep.Spieler.stt.c_str(), FolgeFragen(rep.Spieler).c_str());
							GibPunkte(rep.Spieler, 5);
						}
						quizStruct.af++;
						quizStruct.niemandSpieltMit = 0;
						stack.InsertStack(rep.Spieler.stt.c_str(), "WRITE2LOG", AFrage.ID, (int)(fabs(ant - SiegerPkt)));
						//    					Sleep (100);
						Sleep(zwischenfragendelay);
					}
					else 
						botSay(vs.get("NobodyAnsweredOrRightEnough"), tippstring().c_str());

					userbuffer.clearall();
					stack.InsertStack("timeout", "WRITE2LOG", AFrage.ID, 100);
					
					quizStruct.QuestionHasBeenAnswered();					
					quizStruct.AskAnotherQuestion();

					Zahlenraten = 0;					

					Sleep(zwischenfragendelay);
				}
			}
			if ((!Zahlenraten))
			{
				if (quizStruct.delayneuesquiz)
				{
					quizStruct.delayTimer++;
					timer--;
				}

				if (quizStruct.delayTimer == 300)
				{
					// if(rand()%4==0) ShowToolTip();
				}

				if (quizStruct.delayTimer >= 600)
				{
					if (inicomwunsch)
					{
						stack.InsertStack("", "WUNSCHLISTEAUSWERTEN1");
					}

					Sleep(100);
					quizStruct.delayTimer = 0;
					quizStruct.delayneuesquiz = 0;
					quizStruct.quizIsRunning = 1;
					quizStruct.aktuelleFrageIstBeantwortet = 0;

					botSay(vs.get("AnnouncementQuizStarts"),tippstring().c_str());

					vetonum = 0;
					waitnum = 0;
					weiterlist.clearall();
					LastAnswer = "";
					zeitfuerrunde = 0;
					quizStruct.neueFrage = 1;
					stack.InsertStack("", "NEUEFRAGE1");
				}

				if ((quizStruct.quizIsRunning) && (quizStruct.niemandSpieltMit < inifragenbisquizstopbeikeinerantwort))
					quizStruct.myTimer++;
				if (quizStruct.quizIsRunning)
					quizStruct.helpTimer++;

				quizStruct.zeitmesser++;

				if (!quizStruct.delayneuesquiz)
				{
					if (quizStruct.neueFrage == 1)
					{
						quizStruct.hif = 0;
						quizStruct.neueFrage = 0;
						quizStruct.zeitmesser = 0;
						quizStruct.myTimer = 0;
						quizStruct.helpTimer = 0;
					}

					//weitersteuerung    				
					if ((ChatBot.ChatterSepList.ChatterAngemeldet() > 0) && (weiterlist.weiternum >= ChatBot.ChatterSepList.ChatterAngemeldet()))
					{
						printf("erfoderliche anzahl von weiters zum frageskip erreicht (%i)\n", weiterlist.weiternum);

						botSay((tippstring().c_str() + std::string("*Frage durch weitern aufgelöst:* _") + AFrage.Antwort.stt.c_str()).c_str());

						quizStruct.hif = 0;
						quizStruct.frageVonBotGeloest = 1;						
						stack.InsertStack("wt", "WRITE2LOG", AFrage.ID, 100);
						zeitfuerrunde += 600;
						quizStruct.myTimer = 0;						
						
						quizStruct.QuestionHasBeenAnswered();
						quizStruct.AskAnotherQuestion();

						Sleep(zwischenfragendelay);
					}

					if (quizStruct.helpTimer == 150)
					{
						quizStruct.hif++;

						if (AFrage.typ == 0)
						{
							if (quizStruct.hif < 4)
							{
								std::string p = tippstring().c_str() + std::string(" _Tipp:_ _");

								for (int i = 0; i < AFrage.Antwort.strlength(); i++)
								{
									(zu[i] < quizStruct.hif) ?
										p = p + AFrage.Antwort.stringval(i) //ACHTUNG Fehlerquelle
										:
										p = p + "-";
								}
								botSay(p.c_str());
								quizStruct.helpTimer = 0;
							}
						}
					}

					if ((warteaufnachzuegler) && (warteaufnachzueglertimer > warteaufnachzueglerdelay))
					{
						//warten auf nachzügler abgeschlossen
						warteaufnachzuegler = 0;
						zeitfuerrunde += quizStruct.zeitmesser - warteaufnachzueglerdelay / 100;

						botSay(vs.get("QuestionHasBeenAnswered"), tippstring().c_str(), warteaufnachzueglerersterspieler.stt.c_str(), std::to_string((int)(quizStruct.zeitmesser / 10) - warteaufnachzueglerdelay / 1000).c_str(), std::to_string(4 - quizStruct.hif).c_str(), FolgeFragen(warteaufnachzueglerersterspieler).c_str());

						MString p = tippstring().c_str();
						p += "Nachzuegler ";

						int counter = 0;
						std::vector<userstruct>::iterator it;
						for (it = userbuffer.userelements.begin(); it != userbuffer.userelements.end(); it++)
						{
							if (counter > 0)
								p += " , ";
							userstruct wle;
							wle = *it;
							p += wle.Spieler.stt.c_str();
							GibPunkte(wle.Spieler, std::max(3 - quizStruct.hif, 1));
							counter++;
						}

						p += " (";
						p += std::max(3 - quizStruct.hif, 1);
						p += " Pkt.)";

						if (counter > 0) botSay(p);

						GibPunkte(warteaufnachzueglerersterspieler, 4 - quizStruct.hif);

						quizStruct.QuestionHasBeenAnswered();

						Sleep(zwischenfragendelay);

						if ((quizStruct.frageinrunde >= FragenproRunde))
						{
							if ((quizStruct.spielerAnzahl > 1) && (Punkte[rangliste[0]].Pkt == Punkte[rangliste[1]].Pkt))
							{								
								botSay(vs.get("Tiebreak"), tippstring().c_str(), Punkte[rangliste[0]].Spieler.stt.c_str(), Punkte[rangliste[1]].Spieler.stt.c_str());
								Sleep(3000);
								NeueFrageStellenSQL();
							}
							else {
								Rundebeenden();
							}
						}
						else
						{
							//				sleep (1);
							stack.InsertStack("", "NEUEFRAGE1");
							//                Sleep(200);			
						}
					}
					if (quizStruct.myTimer == 600)
					{
						quizStruct.hif = 0;

						botSay(vs.get("QuestionSolvedByBot"), tippstring().c_str(), AFrage.Antwort.stt.c_str());
						quizStruct.frageVonBotGeloest = 1;
						
						quizStruct.aktuelleFrageIstBeantwortet = 1;
						stack.InsertStack("60Sek", "WRITE2LOG", AFrage.ID, 100);
						zeitfuerrunde += 600;
						quizStruct.myTimer = 0;
						quizStruct.frageinrunde++;
						//quizStruct.af++;
						quizStruct.niemandSpieltMit = quizStruct.niemandSpieltMit + 1;
						
						if (ChatBot.ChatterSepList.ChatterAngemeldet() == 0) 
							quizStruct.niemandSpieltMit = inifragenbisquizstopbeikeinerantwort;
						
						if ((quizStruct.niemandSpieltMit == inifragenbisquizstopbeikeinerantwort))
						{
							botSay(vs.get("NobodyPlayingWaitMessage"));							
						}
						else 
						{
							quizStruct.neueFrage = 1;
							stack.InsertStack("", "NEUEFRAGE1");
						}

						Sleep(zwischenfragendelay);
					}
				}
			}
			break;
		}
		workquerrystack();		
	}

	pthread_exit(NULL);
	return 0;
}

//-----------------------------------------------------------------------------
int main() 
{
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	_CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
	//_CrtSetBreakAlloc(25496);
	//_CrtSetBreakAlloc(25495);
	

	srand((unsigned int)time((time_t *)NULL));

	Startuptime = time(0);

	for (int i = 0; i < 100; i++) zu[i] = i % 4;
	float ll = 4;
	
	textformat.leadstring = vs.get("LeadString");

	readinifile();

	Write2Log("querylog.txt", "-----------\n");

	mySQL.InitMySQL();

	ids = 1;
	pfi = &ParseChatLine;
	pfiTEST = &TestGesilenct;
	ChatBot.leseeigenenschrieb = 0;


	bool quizon = 0;
	keepalive = 0;

	//-----------------------------------------------------------------------------
	pthread_t botMainThread;

	if (pthread_create(&botMainThread, NULL, MandalaMainLoop, (void *)0))
	{
		std::cout << "Error:unable to create bot mainthread" << std::endl;
		exit(-1);
	}
	
	client = new ClientClass(vs.get("DiscordAuthToken","DiscordSection"), 2);
	client->run();

	//-----------------------------------------------------------------------------	
	mySQL.Shutdown();
	ChatBot.Exit();

	pthread_exit(NULL);

	delete client;

	_CrtDumpMemoryLeaks();
}