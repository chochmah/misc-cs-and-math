#include "pch.h"
#include "ConfigFileClass.h"

namespace cf
{
	//-----------------------------------------------------------------------------
	uint32_t					ConfigVariableClassInterface::instanceCounter = 0;
	std::mutex					ConfigVariableClassInterface::lock;
	ConfigVariableListClass		*ConfigVariableClassInterface::ConfigVariableList = nullptr;
}