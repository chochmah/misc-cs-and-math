#include "ConfigVariableClassInterface.h"

#include <windows.h>
#include <mutex>
#include <iostream>

namespace cf
{
	//-----------------------------------------------------------------------------
	ConfigVariableListClass ConfigVariableClassInterface::varList;


	void ConfigVariableClassInterface::construct()
	{
		while (lock.try_lock())
			Sleep(1);
		{
			if (instanceCounter == 0)
			{
				ConfigVariableList = new ConfigVariableListClass();
				std::cout << "create ConfigVariableList\n";
			}

			instanceCounter++;
		}
		lock.unlock();
	}

	void ConfigVariableClassInterface::destruct()
	{
		while (lock.try_lock())
			Sleep(1);
		{
			if (instanceCounter == 0)
			{
				std::cout << "ConfigVariableClassInterface Faral Error, instancecount<0 \n";
				//exit(1);
			}
			instanceCounter--;
			if (instanceCounter == 0)
			{
				delete ConfigVariableList;
				std::cout << "destroy ConfigVariableList\n";
			}
		}
		lock.unlock();
	}

	ConfigVariableClassInterface::ConfigVariableClassInterface()
	{
		construct();
	}

	ConfigVariableClassInterface::~ConfigVariableClassInterface()
	{
		destruct();
	}
}

