/*
	Abstracts MySQL away from the main program... duh.
*/

#pragma once

#include <string>
#include <vector>

#if defined(_WIN32)
typedef unsigned __int64 my_ulonglong;
#else
typedef unsigned long long my_ulonglong;
#endif /* _WIN32 */

//-----------------------------------------------------------------------------
typedef std::vector<std::string> mySQL_ROW;

typedef struct mySQL_RES 
{
	std::vector <mySQL_ROW> rows;

	size_t count()
	{
		return rows.size();
	}

} mySQL_RES;

//-----------------------------------------------------------------------------
class MySQLAbstractionLayerClass
{
public:
	MySQLAbstractionLayerClass() {};
	~MySQLAbstractionLayerClass() {};	

	static mySQL_RES sendQuery(std::string query, ...);
	static my_ulonglong num_rows(mySQL_RES* arg);
	static void InitMySQL();
	static void Shutdown();
};

static MySQLAbstractionLayerClass mySQL;