#include "pch.h"
#include "stackclass.h"


StackClass::~StackClass()
{
	clearall();
}

//-----------------------------------------------------------------------------
StackClass::StackClass(ChatBotClass* chatbot_) : chatbot(chatbot_)
{
}

//-----------------------------------------------------------------------------
void StackClass::clearall() 
{
     stackelements.clear();
}

//-----------------------------------------------------------------------------
void StackClass::InsertStack(std::string R1, std::string R2) 
{
      stackstruct wle;    
      wle.S1=R1;
      wle.S2=R2;
      stackelements.push_back(wle);           
}

//-----------------------------------------------------------------------------
void StackClass::InsertStack(std::string R1, std::string R2, int x1, int x2) 
{
	stackstruct wle;
	wle.S1 = R1;
	wle.S2 = R2;
	wle.i1 = x1;
	wle.i2 = x2;
	stackelements.push_back(wle);
}

//-----------------------------------------------------------------------------
int StackClass::size() 
{
      return((int)stackelements.size());
}

//-----------------------------------------------------------------------------
std::vector<StackClass::stackstruct>& StackClass::getStackElements()
{
	return stackelements;
}

//-----------------------------------------------------------------------------
int StackClass::say(std::string textin)
{
	ChatBotClass::MStruct msg;
	msg.nick = "";
	msg.message = textin.c_str();
	msg.timestamp = time(NULL);
	chatbot->saystack.push_back(msg);

	return 0;
}