#include "HighScoreListenClass.h"

#include "structs.h"

#include "MySQLAbstractionLayerClass.h"
#include "VariableStorageStruct.h"

#include "mstring.h"
#include "QuizStruct.h"


HighScoreListenClass::HighScoreListenClass()
{
}


HighScoreListenClass::~HighScoreListenClass()
{
}


//-----------------------------------------------------------------------------	
void HighScoreListenClass::UpdateEwigeListeSQL()
{
	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		mySQL_ROW row = mySQL.sendQuery("select 1+count(*) from ewigeliste where punkte>(select punkte from ewigeliste where name='%s');", Punkte[i].Spieler.stt.c_str()).rows[0];
		Punkte[i].oep = (int)strtod(row[0].c_str(), NULL);
	}

	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		mySQL.sendQuery("insert into tagesliste (name, punkte, gehoert, beantwortet,siege) Values ('%s',%i,%i,%i,%i) ON DUPLICATE KEY UPDATE punkte=punkte+%i, gehoert=gehoert+%i, beantwortet=beantwortet+%i, siege=siege+%i", Punkte[i].Spieler.stt.c_str(), Punkte[i].Pkt, Punkte[i].fragengehoert, Punkte[i].fragenbeantwortet, Punkte[i].Siege, Punkte[i].Pkt, Punkte[i].fragengehoert, Punkte[i].fragenbeantwortet, Punkte[i].Siege);
		mySQL.sendQuery("insert into ewigeliste (name, punkte, gehoert, beantwortet,siege) Values ('%s',%i,%i,%i,%i) ON DUPLICATE KEY UPDATE punkte=punkte+%i, gehoert=gehoert+%i, beantwortet=beantwortet+%i, siege=siege+%i", Punkte[i].Spieler.stt.c_str(), Punkte[i].Pkt, Punkte[i].fragengehoert, Punkte[i].fragenbeantwortet, Punkte[i].Siege, Punkte[i].Pkt, Punkte[i].fragengehoert, Punkte[i].fragenbeantwortet, Punkte[i].Siege);
	}

	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		auto row = mySQL.sendQuery("select 1+count(*) from ewigeliste where punkte>(select punkte from ewigeliste where name='%s');", Punkte[i].Spieler.stt.c_str()).rows[0];
		Punkte[i].nep = (int)strtod(row[0].c_str(), NULL);
	}
}

//-----------------------------------------------------------------------------	
void HighScoreListenClass::ShowEwigeListeChange()
{
	char oIntString[32];
	char nIntString[32];
	char buffer[1000];

	MString p = "";
	for (int i = 0; i < quizStruct.spielerAnzahl; i++)
	{
		sprintf(oIntString, "%i", Punkte[i].oep);
		sprintf(nIntString, "%i", Punkte[i].nep);
		//printf("%s %i %i\n",Punkte[i].Spieler.stt.c_str(),Punkte[i].nep,Punkte[i].nep);

		if ((Punkte[i].oep == 1) && (Punkte[i].nep > 1))
		{
			sprintf(buffer, vs.get("IsNewToScoreList").c_str(), Punkte[i].Spieler.stt.c_str(), nIntString);
			p += buffer;
		}

		if (Punkte[i].oep != 1)
			if (Punkte[i].oep > Punkte[i].nep)
			{
				sprintf(buffer, vs.get("RisesInScoreList").c_str(), Punkte[i].Spieler.stt.c_str(), oIntString, nIntString);
				p += buffer;
			}
		if (Punkte[i].oep != 1)
			if (Punkte[i].oep < Punkte[i].nep)
			{
				sprintf(buffer, vs.get("DropsInScoreList").c_str(), Punkte[i].Spieler.stt.c_str(), oIntString, nIntString);
				p += buffer;
			}
	}
	if (p != "") botSay(p.stt.c_str());
}

//-----------------------------------------------------------------------------	
void HighScoreListenClass::ShowEwigeListeSQL()
{
	mySQL_RES res = mySQL.sendQuery("select name,punkte from ewigeliste group by punkte desc;");

	if (!res.count())
		return;

	int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);

	botSay("_&bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; TOP 28 &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull;");
	Sleep(60);

	MString p;
	for (int k = 0; k < 7; k++)
	{
		p = "_&bull;_ ";
		for (int i = 0; i < 4; i++)
		{
			if (i + k * 4 >= numrows) goto fertig;
			mySQL_ROW row = res.rows[0];

			int numb = atoi(row[1].c_str());

			p += "_[";
			p += i + k * 4 + 1;
			p += "]_*";
			p += row[0].c_str();
			p += "*:";
			p += numb;
			//  if (kvar) p+="k";
			p += " ";
		}
	fertig:
		botSay(p.stt.c_str());
		Sleep(60);
	}

	botSay("_&bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull;");
}

//-----------------------------------------------------------------------------	
void HighScoreListenClass::ShowTagesListeSQL()
{
	mySQL_RES res = mySQL.sendQuery("select name,punkte from tagesliste group by punkte desc;");
	if (!res.count())
		return;

	int numrows = (int)MySQLAbstractionLayerClass::num_rows(&res);

	botSay("_&bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; TTP 12 &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull;");
	Sleep(60);

	MString p;

	for (int k = 0; k < 3; k++)
	{
		p = "_&bull;_ ";
		for (int i = 0; i < 4; i++)
		{
			if (i + k * 4 >= numrows)
				goto fertig;

			mySQL_ROW row = res.rows[0]; /* Get a row from the results */

			p += "_[";
			p += i + k * 4 + 1;
			p += "]_*";
			p += row[0].c_str();
			p += "*:";
			p += row[1].c_str();
			p += " ";
		}
	fertig:
		botSay(p.stt.c_str());
		Sleep(60);
	}

	botSay("_&bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull; &bull;");
}
