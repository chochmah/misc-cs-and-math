#pragma once

#include "ClientClass.h"
#include "stackclass.h"

extern StackClass stack;

//-----------------------------------------------------------------------------	
class QuizStateStruct
{
public:

	enum class QuizTypes { standart, datenordnen};

	QuizStateStruct() {}
	~QuizStateStruct() {}

	QuizTypes	quizType{ QuizTypes::standart };
	int			delayTimer	{ 0 };
	int			myTimer		{ 0 };
	int			helpTimer	{ 0 };
	int			rateTimer	{ 0 };
	bool		neueFrage	{ 1 };
	int			niemandSpieltMit{ 0 };
	bool		delayneuesquiz{ 0 };
	int			zeitmesser	{ 0 };
	int			hif;									/* verbleibende Punkte */

	bool		aktuelleFrageIstBeantwortet{ 0 };
	bool		frageVonBotGeloest{ 0 };
	int			frageinrunde{ 0 };

	bool		quizIsRunning{ 0 };
	int			spielerAnzahl{ 0 };		/* Zahl der Spieler im augenblicklichen Spiel */

	bool		themenRunde{ 0 };		/* Flag if the current round is Themed or not */
	int			af{ 0 };
	
	//-----------------------------------------------------------------------------	
	void QuestionHasBeenAnswered()
	{
		neueFrage = 1;
		aktuelleFrageIstBeantwortet = 1;
		af++;
		frageinrunde++;		
		rateTimer = 0;		
	}

	//-----------------------------------------------------------------------------	
	void AskAnotherQuestion()
	{
		stack.InsertStack("", "NEUEFRAGE1");
	}

	//-----------------------------------------------------------------------------	
	void startthequiz()
	{
		frageinrunde = 0;
		quizIsRunning = 1;
		af = 0;
		stack.InsertStack("", "NEUEFRAGE1");
	}

	//-----------------------------------------------------------------------------	
	void HaltQuiz()
	{
		quizIsRunning = 0;
		spielerAnzahl = 0;
		botSay("Mandala angehalten");
	}
};