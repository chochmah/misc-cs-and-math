#include "pch.h"
#include "weiterlistclass.h"


WeiterListClass::WeiterListClass() {
   maxweitersprorunde=5;
   weiternum=0;
}

void WeiterListClass::clearall() {
     weiterelements.clear();
}


void WeiterListClass::neuefrage() {
     printf("inweiter\n");
    weiternum=0;
    std::vector<wlestruct>::iterator it;    
    for(it = weiterelements.begin(); it != weiterelements.end(); it++) {       
      printf("|");
      wlestruct wle;
      wle=*it;
      it->saidweiter=0;      
      printf("%s in weiterliste resetet\n",wle.nick.stt.c_str());
    }        
     printf("outweiter\n");    
}

int WeiterListClass::chattersaysweiter(MString chatter) {
/*
Returnvalues:
1:chatter hat schon �fter !weiter in dieser runde gesagt als er darf
2:chatter hat bei dieser frage schon weiter gesagt
3:das weiter des chatters wurde registriert
*/
  int gibtsschon=0;
  int listechanged=0;
  std::vector<wlestruct>::iterator it;    
    for(it = weiterelements.begin(); it != weiterelements.end(); it++) {       
      wlestruct wle;
      wle=*it;
      if (wle.nick==chatter) {
         printf("%s in liste gefunden (unter %s)\n",chatter.stt.c_str(),wle.nick.stt.c_str());
         if (wle.weitercounter>=maxweitersprorunde) return(1);
         if (wle.saidweiter) return(2);
         else {
              weiternum++;
              it->saidweiter=1;
              printf("chatter %s hat schon %i weiters und sein weiter wurde registriert",chatter.stt.c_str(),wle.weitercounter);
              it->weitercounter++;
              return(3);
         }
         gibtsschon=1;
         
      }
    }        
    if (!gibtsschon) {
      weiternum++;
      wlestruct wle;    
      wle.nick=chatter;
      wle.saidweiter=1;
      wle.weitercounter=1;
      weiterelements.push_back(wle);           
      listechanged=1;
      printf("chatter %s hat noch keine weiters in der runde und das weiter wurde registriert\n",chatter.stt.c_str());         
      return(3);
    }
}
