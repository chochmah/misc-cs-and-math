#ifndef WeiterListClass_H
#define WeiterListClass_H
#include "mstring.h"
#include <vector>   


class WeiterListClass
{
private:

  typedef struct {
        MString nick;
        int weitercounter;
        bool saidweiter;
  } wlestruct;

  std::vector<wlestruct> weiterelements;     
        
public:
  int weiternum;
  int maxweitersprorunde;
  WeiterListClass(); 
  int chattersaysweiter(MString chatter);
  void neuefrage();
  void clearall();
};

#endif 
