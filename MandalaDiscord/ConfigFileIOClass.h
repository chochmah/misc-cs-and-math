#pragma once

#include <memory>
#include <string>
#include "..\PIniReadWrite\PIniReadWrite.h"


namespace cf
{
	//-----------------------------------------------------------------------------
	// Prototype
	class ConfigFileClassProxy;

	//-----------------------------------------------------------------------------
	class ConfigFileIOClass {
	public:
		ConfigFileIOClass(const std::string fileName_);
		~ConfigFileIOClass();

		ConfigFileClassProxy
			getParam				(const std::string paramSection, const std::string paramName, const std::string defaultVal) const;
		int			getParamInt		(const std::string paramSection, const std::string paramName, const std::string defaultVal) const;
		float		getParamFloat	(const std::string paramSection, const std::string paramName, const std::string defaultVal) const;
		bool		getParamBool	(const std::string paramSection, const std::string paramName, const std::string defaultVal) const;
		std::string	getParamString	(const std::string paramSection, const std::string paramName, const std::string defaultVal) const;

		void		setParam		(const std::string paramSection, const std::string paramName, const std::string val) const;
		void		setParam		(const std::string paramSection, const std::string paramName, const int			val) const;
		void		setParam		(const std::string paramSection, const std::string paramName, const float		val) const;
		void		setParam		(const std::string paramSection, const std::string paramName, const bool		val) const;


	private:
		std::string					fileName = NULL;
		std::unique_ptr<PIniRW::PIniReadWrite> iniReader;
		std::unique_ptr<PIniRW::PIniReadWrite> iniWriter;
	};

	//----------------------------------------------------------------------------
	// proxy to allow to runtime template argument specification for read
	class ConfigFileClassProxy {
		ConfigFileIOClass const*	configFileClassOwner;
		std::string					paramName;
		std::string					paramSection;
		std::string					defaultVal;
	public:
		ConfigFileClassProxy(ConfigFileIOClass const* owner, const std::string paramSection_, const std::string paramName_, const std::string defaultVal_)
			: configFileClassOwner(owner), paramSection(paramSection_), paramName(paramName_), defaultVal(defaultVal_)
		{}

		operator int()			const { return configFileClassOwner->getParamInt	(paramSection, paramName, defaultVal); }
		operator bool()			const { return configFileClassOwner->getParamBool	(paramSection, paramName, defaultVal); }
		operator float()		const { return configFileClassOwner->getParamFloat	(paramSection, paramName, defaultVal); }
		operator std::string()	const { return configFileClassOwner->getParamString	(paramSection, paramName, defaultVal); }
	};
}