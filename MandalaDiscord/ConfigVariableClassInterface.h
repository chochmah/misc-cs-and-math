#pragma once

#include <list>
#include <mutex>

namespace cf
{
	//----------------------------------------------------------------------------
	enum SYNCTYPE { DONT_SYNC, AUTO_SYNC };

	//----------------------------------------------------------------------------
	class ConfigVariableClassInterface;

	//-----------------------------------------------------------------------------
	// class to hold list of ConfigVariableListClass objects 

	class ConfigVariableListClass {
	public:
		std::list<ConfigVariableClassInterface*> vars;

	public:
		ConfigVariableListClass() {};
		~ConfigVariableListClass() {};

		void add(ConfigVariableClassInterface* _x)
		{
			vars.push_back(_x);
		}

		void remove(ConfigVariableClassInterface* _x)
		{
			vars.remove(_x);
		}
	};


	//-----------------------------------------------------------------------------
	// class interface for ConfigVariableListClass to inherit from to allow for a list 
	// of ConfigVariableList objects.

	class ConfigVariableClassInterface {
	protected:
		static ConfigVariableListClass		varList;

		void construct();
		void destruct();

		static uint32_t						instanceCounter;
		static ConfigVariableListClass*		ConfigVariableList;
		static std::mutex					lock;	// to ensure threadsavety
				
		virtual void						print() = 0;

	public:		
		ConfigVariableClassInterface();
		virtual ~ConfigVariableClassInterface();

		virtual std::string	getFileName()	const { return ""; }
		virtual std::string	getSection()	const { return ""; }		
		virtual std::string	getKey()		const { return ""; }
		virtual	SYNCTYPE	getSyncType()	const { return SYNCTYPE::AUTO_SYNC; }

		virtual std::ostream&	getKeyPrintString(std::ostream& stream) = 0;
	};
}