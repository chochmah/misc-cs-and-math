#ifndef StackClass_H
#define StackClass_H

#include "mstring.h"
#include <vector>   
#include "chatbot.h"

//-----------------------------------------------------------------------------
class StackClass
{
public:
	typedef struct
	{
		std::string S1, S2;
		int			i1, i2;
	} stackstruct;

private:
	std::vector<stackstruct> stackelements;
	ChatBotClass* chatbot;

public:
	~StackClass();
	StackClass(ChatBotClass *chatbot);      
	void InsertStack(std::string R1, std::string R2);
	void InsertStack(std::string R1, std::string R2, int x1, int x2);

	int size();
	void clearall();

	std::vector<stackstruct>& getStackElements();
	int say(std::string textin);
};

#endif 




