#include "pch.h"
#include <iostream>
#include <vector>
#include <string>


std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

int len = 8;

void recursive(std::vector<int> places, int p, std::string ps, int iteration) {
	if (iteration > 1) {
		int count = 0;
		std::string pc = ps;

		for (int i = 0; i < 10; i++) {
			if (i == 0 && places[0] == 1)
				continue;
			for (int k = 0; k < places.size(); k++)
				if (places[k])
					pc[k] = 48 + i;
			if (natNumbers[std::stoi(pc)])
				count++;			
		}
		
		if (count >= len) {
			std::cout << "found one (solution might be with substitution) " << p << "   ";
			for (int k = 0; k < ps.size(); k++)
				std::cout << places[k];
			std::cout << "\n";
			exit(0);
		}
	}

	if (iteration < ps.size()+1) {
		for (int i = 0; i < ps.size(); i++) {
			if (places[i] == 0) {
				places[i] = 1;
				recursive(places, p, ps, iteration + 1);
				places[i] = 0;
			}
		}
	}
}

// solution is 121313

int main() {
	CreatePrimeList(primes, 1000000);
	for (auto p : primes) 		
		recursive(std::vector<int>(std::to_string(p).size(),0),p, std::to_string(p),1);
}
	

//found one 2090021   0101100