// Euler Project 17 Letter Number Counts.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>

int tcount = 0;

void cc(std::string nr)
{	
	std::cout << nr << " ";
	tcount += nr.length();
}

int main()
{
	std::vector<std::string> nrs{
		"one","two","three", "four", "five", "six", "seven", "eight", "nine", "ten", 
		/*10*/ "eleven", "twelfe", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", 
		/*19*/ "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" ,
		/*27*/ "hundredand" , "onethousand", "hundred"};

	for (int i = 0; i < 19; i++)
		cc(nrs[i]);
	for (int k = 0; k < 8; k++)
	{
		cc(nrs[19 + k]);
		for (int i = 0; i < 9; i++)
			cc(nrs[19 + k] + nrs[i]);
	}			

	for (int k=0;k<9;k++)
	{ 
		cc(nrs[k] + nrs[29]);
		for (int i = 0; i < 19; i++)
			cc(nrs[k] + nrs[27] + nrs[i]);
		for (int v = 0; v < 8; v++)
		{
			cc(nrs[k] + nrs[27] + nrs[19 + v]);
			for (int s = 0; s < 9; s++)
				cc(nrs[k] + nrs[27] + nrs[19 + v] + nrs[s]);
		}
	}
	cc(nrs[28]);


	std::cout << tcount << "\n";
}
