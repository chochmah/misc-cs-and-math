#include "pch.h"
#include <iostream>
#include <vector>

typedef unsigned long long myInt;
constexpr myInt maxNr = 1E9;
std::vector<myInt> primes{ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
myInt count(0);

void searchFkt(std::vector<myInt> v, unsigned int p) {
	myInt total(1);
	for (auto e : v)
		total *= e;
	if (total <= maxNr)
	{		
		count++;

		for (int i = p; i < primes.size(); i++) {
			std::vector<myInt> vTemp(v);
			vTemp.push_back(primes[i]);
			searchFkt(vTemp, i);
		}
	}
}

int main() {
	searchFkt(std::vector < myInt>(), 0);	
	std::cout << count << "\n";
	std::getchar();
}