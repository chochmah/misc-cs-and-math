#include <stdio.h>
#include <iostream>

#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeIntegerClass.h"
#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeFloatClass.h"


/*
	works but is way, way too slow
	Pell equation, use https://mathworld.wolfram.com/PellEquation.html and large integers to solve
*/


int main() {		
	typedef unsigned long long int IntL;

	IntL solution = 0;
	IntL solutionD = 0;
	for (IntL D = 2; D < 1000; D=D+1) {
		if (std::sqrt(D)*std::sqrt(D) != D) {
			for (IntL x = 2; x < 1E20; x=x+1) {
				IntL m = (x * x - 1) / D;
				if (m*D == x * x - 1) {
					IntL y = std::sqrt(m);
					if (y != 0 && x*x - D * y*y == 1) {
						if (x > solution) {
							solution = x;
							solutionD = D;
						}
						std::cout << x << "^2 - " << D << "*" << y << "^2 = 1\n";
						goto fertig;
					}
				}
			}
			std::cout << "Failure!\n";
			getchar();
			exit(1);
		fertig: 
			int q = 1;
		}
	}

	std::cout << "Solution: " << solution << " for D=" << solutionD << "\n";

	getchar();

	return 0;
}
