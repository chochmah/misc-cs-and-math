//    Huffman und LZ77 File Encoder and Decoder library
//    komprimiert und dekomprimiert Dateien und Streams
//
//

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <bitset>
#include <vector>
#include <stdlib.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


using namespace std;

const char LZ77Steuerzeichen = char(22);

int codesfound;
std::vector<std::string> OutputBuffer;

class BitString {
	//erlaubt das schreiben von einzelnen bits in den String
	std::vector<std::string> Buffer;
	unsigned int currentpos;
	unsigned int currentbytepos;
	char c;
	std::vector<char> buffer;
	long filesize;
public:
	bool EOFbit;
	unsigned int bitpos() {
		return(currentbytepos + currentpos * 8);
	}
	BitString() {
		c = 0;
		currentbytepos = 0;
		currentpos = 0;
		EOFbit = 0;
	}
	void writebit(bool b) {
		if (currentbytepos>7) {
			currentbytepos = 0;
			currentpos++;
			string ms; ms = c;
			Buffer.push_back(ms);
			c = 0;
		}
		if (b) {
			//cout << "1";
			c = c | (0x1 << (7 - currentbytepos));
		}
		else {
			//cout << "0";
			c = c | (0x0 << (7 - currentbytepos));
		}
		currentbytepos++;
	}
	void writebyte(char b) {
		for (int i = 0; i<8; i++) {
			writebit((b >> i) & 0x01);
		}
	}
	void writechar(char b) {
		for (int i = 0; i<8; i++) {
			writebit((b >> (7 - i)) & 0x01);
		}
	}

	void Write2Stream(std::vector<char> *OutStream) {
		OutStream->resize(Buffer.size());
		for (unsigned int i = 0; i<Buffer.size(); i++)
			(*OutStream)[i] = Buffer[i][0];
	}

	void Write2File(const char *name) {
		ofstream myfile2;
		myfile2.open(name, ios::out | ios::binary);
		for (unsigned int i = 0; i<Buffer.size(); i++)
			myfile2 << Buffer[i];
		myfile2.close();
	}
	bool readbit() {
		if (currentbytepos>7) {
			currentbytepos = 0;
			currentpos++;
			if (currentpos <= (unsigned int)filesize) {
				c = buffer[currentpos];
			}
			else {
				cout << "EOF ERROR" << currentpos << endl;
				EOFbit = 1;
				exit(0);
			}
		}

		if ((currentpos == filesize - 1) && (currentbytepos == 7)) {
			EOFbit = 1;
			cout << "[EOF]";
		}

		if (c & (0x1 << (7 - currentbytepos))) {
			//cout << "1";		  		
			currentbytepos++;
			return(1);
		}
		else {
			//cout << "0";
			currentbytepos++;
			return(0);
		}
	}
	char readbyte() {
		char ret;
		for (int i = 0; i<8; i++) {
			if (readbit()) ret = ret | (0x1 << (7 - i));
		}
		return((unsigned char)ret);
	}
	char readchar() {
		char ret;
		for (int i = 0; i<8; i++) {
			if (readbit()) ret = ret | (0x1 << i);
		}
		return((unsigned char)ret);
	}
	void ReadFromFile(const char *name) {

		std::ifstream myfile;
		myfile.open(name, ios::out | ios::binary);
		myfile.seekg(0, myfile.end);
		filesize = (long)myfile.tellg();
		myfile.seekg(0, myfile.beg);
		cout << "filesize: " << filesize << endl;
		buffer.resize(filesize);
		myfile.read(&buffer[0], filesize);
		myfile.close();
		cout << "Filesize: " << filesize * 8 << " bits" << endl;
		c = buffer[0];
	}
};


class BinaryTreeNode {
public:
	BinaryTreeNode *Child0;
	BinaryTreeNode *Child1;
	bool leaf;
	char codebook;
	unsigned int frequency;
	bool *codebook_huffman;
	unsigned int codelength;
	BinaryTreeNode() {
		leaf = 0;
		frequency = 0;
		codelength = 0;
	}
};



bool recursive_tree_traverse(BinaryTreeNode *ParentNode, bool *code,
	unsigned int depth) {
	if (ParentNode->leaf) {
		ParentNode->codelength = depth;
		ParentNode->codebook_huffman = new bool[depth];
		for (unsigned int i = 0; i<depth; i++)
			ParentNode->codebook_huffman[i] = code[i];
	}
	else {
		int clength = sizeof(code);
		bool *new_code_0 = new bool[depth + 1];
		for (unsigned int i = 0; i<depth; i++) new_code_0[i] = code[i];
		new_code_0[depth] = 0;
		bool *new_code_1 = new bool[depth + 1];
		for (unsigned int i = 0; i<depth; i++) new_code_1[i] = code[i];
		new_code_1[depth] = 1;
		recursive_tree_traverse(ParentNode->Child0, new_code_0, depth + 1);
		recursive_tree_traverse(ParentNode->Child1, new_code_1, depth + 1);
	}
	return true;
}

bool write_huffmann_tree(BinaryTreeNode *ParentNode,
	unsigned int depth, BitString *bString) {
	if (ParentNode->leaf) {
		bString->writebit(1);
		bString->writebyte(ParentNode->codebook);
	}
	else {
		bString->writebit(0);
		write_huffmann_tree(ParentNode->Child0, depth + 1, bString);
		write_huffmann_tree(ParentNode->Child1, depth + 1, bString);
	}
	return true;
}

bool recursive_tree_read(BitString *bString, BinaryTreeNode *ParentNode,
	unsigned int codebooksize) {
	if (codebooksize>(unsigned int)codesfound) {
		if (bString->readbit()) {
			//1 gefunden als n�chstes kommt ein 8bit-code						
			ParentNode->codebook = bString->readchar();
			ParentNode->leaf = 1;
			//cout << "found code '" << ParentNode->codebook<< "'" << endl;
			codesfound++;
		}
		else {
			//nix gefunden, baum weiter rekursiv aufbauen			
			BinaryTreeNode *NewNode0 = new BinaryTreeNode;
			BinaryTreeNode *NewNode1 = new BinaryTreeNode;
			ParentNode->Child0 = NewNode0;
			ParentNode->Child1 = NewNode1;
			recursive_tree_read(bString, ParentNode->Child0, codebooksize);
			recursive_tree_read(bString, ParentNode->Child1, codebooksize);
		}
	}
	return true;
}

bool recursive_code_read(BitString *bString, BinaryTreeNode *ParentNode) {
	if (ParentNode->leaf) {

		//cout <<"["<<ParentNode->codebook<<"]"<<endl;
		//cout << ParentNode->codebook;
		string ms; ms = ParentNode->codebook;
		OutputBuffer.push_back(ms);
	}
	else
		if (bString->readbit()) {
			//cout << "1";
			recursive_code_read(bString, ParentNode->Child1);
		}
		else {
			//cout << "0";
			recursive_code_read(bString, ParentNode->Child0);
		}
	return true;
}

void HuffmanDeCompressFile2Stream(const char *inname, std::vector<char> *outputbuffer) {
	BinaryTreeNode RootNode;
	unsigned int codebooksize = 0;
	BitString bString;
	bString.ReadFromFile(inname);
	codebooksize = 1 + (unsigned char)bString.readbyte();
	cout << "Codebooksize: " << (int)codebooksize << endl;
	codesfound = 0;
	OutputBuffer.clear();
	recursive_tree_read(&bString, &RootNode, codebooksize);

	//Read Filler
	unsigned int fillbits = 0;
	do {
		fillbits++;
	} while (!bString.readbit());
	cout << "fillerbits:" << fillbits << endl;

	do {
		recursive_code_read(&bString, &RootNode);
	} while (!bString.EOFbit);
	outputbuffer->resize(OutputBuffer.size());
	for (unsigned int i = 0; i<OutputBuffer.size(); i++)
		(*outputbuffer)[i] = OutputBuffer[i][0];
	cout << "decompression done" << endl;
}


void HuffmanDeCompressFile2File(const char *inname, const char *outname) {
	std::vector<char> buffer;
	HuffmanDeCompressFile2Stream(inname, &buffer);
	ofstream myfile2;
	myfile2.open(outname, ios::out | ios::binary);
	for (unsigned int i = 0; i<OutputBuffer.size(); i++)
		myfile2 << OutputBuffer[i];
	myfile2.close();
}

void HuffmanCompressStream(std::vector<char> *buffer, std::vector<char> *outputbuffer) {
	unsigned char codebookpointer[256];
	bool codebookassign[256];
	char codebook[256];
	int frequency[256];
	unsigned int codebooksize = 0;
	long filesize = (long)buffer->size();

	for (int i = 0; i<256; i++)
		codebookassign[i] = 0;

	for (int i = 0; i<filesize; i++) {
		if (codebookassign[(unsigned char)(*buffer)[i]] == 1) {
			frequency[codebookpointer[(unsigned char)(*buffer)[i]]]++;
		}
		else
		{
			codebookpointer[(unsigned char)(*buffer)[i]] = codebooksize;
			codebookassign[(unsigned char)(*buffer)[i]] = 1;
			codebook[codebooksize] = (*buffer)[i];
			frequency[codebooksize] = 1;
			codebooksize++;
		}
	}

	std::vector<BinaryTreeNode> LeafNodes(codebooksize);
	std::vector<BinaryTreeNode*> ActiveNodes(codebooksize);
	int activenodenumber = codebooksize;

	for (unsigned int i = 0; i<codebooksize; i++) {
		LeafNodes[i].codebook = codebook[i];
		LeafNodes[i].frequency = frequency[i];
		LeafNodes[i].leaf = 1;
		LeafNodes[i].codelength = 1;
		ActiveNodes[i] = &LeafNodes[i];
	}

	while (activenodenumber>1) {
		//nodeSort gro� nach klein
		for (int i = 0; i<activenodenumber; i++)
			for (int l = i + 1; l<activenodenumber; l++) {
				if (ActiveNodes[l]->frequency>ActiveNodes[i]->frequency) {
					BinaryTreeNode *BufferNode;
					BufferNode = ActiveNodes[l];
					ActiveNodes[l] = ActiveNodes[i];
					ActiveNodes[i] = BufferNode;
				}
			}
		BinaryTreeNode *NewNode = new BinaryTreeNode;
		NewNode->leaf = 0;
		NewNode->Child0 = ActiveNodes[activenodenumber - 2];
		NewNode->Child1 = ActiveNodes[activenodenumber - 1];
		NewNode->frequency =
			ActiveNodes[activenodenumber - 1]->frequency
			+ ActiveNodes[activenodenumber - 2]->frequency;
		ActiveNodes[activenodenumber - 2] = NewNode;
		activenodenumber--;
	}

	recursive_tree_traverse(ActiveNodes[0], 0, 0);

	unsigned int size_after_compression = 0;
	unsigned int size_of_codebook = 0;

	for (unsigned int i = 0; i<codebooksize; i++)
		size_after_compression += LeafNodes[i].frequency*LeafNodes[i].codelength;

	cout << endl;
	cout << "Size before compression: " << filesize * 8 << " bits" << endl;
	cout << "Size after compression: " << size_after_compression
		<< " bits" << endl;
	cout << "Words in Codebook: " << (int)codebooksize << endl;

	// Write Compressed Stream
	BitString bString;
	bString.writechar(codebooksize - 1);
	write_huffmann_tree(ActiveNodes[0], 0, &bString);
	size_of_codebook = bString.bitpos();
	cout << endl << "Size of Codebook: " << size_of_codebook << " bits" << endl;

	//Filler to reach EOF 
	unsigned int fillbits = 8 - (size_after_compression + size_of_codebook) % 8;
	cout << "Total Size w/o Filler: "
		<< size_after_compression + size_of_codebook << endl;
	if (fillbits == 0) fillbits = 8;
	cout << "bits to fill:" << fillbits << "   ";
	for (unsigned int i = 0; i<fillbits - 1; i++) bString.writebit(0);
	bString.writebit(1);
	cout << endl;
	//cout << "text_codet: ";
	int pos = 0;
	unsigned int compressedlength = 0;
	for (int k = 0; k<filesize; k++) {
		for (unsigned int i = 0; i<codebooksize; i++)
			if ((*buffer)[pos] == LeafNodes[i].codebook) {
				//cout << buffer[pos];	  		
				compressedlength += LeafNodes[i].codelength;
				for (unsigned int l = 0; l<LeafNodes[i].codelength; l++) {
					if (LeafNodes[i].codebook_huffman[l]) bString.writebit(1);
					else bString.writebit(0);
				}
				goto ok;
			}
	ok:
		pos++;
	}

	cout << endl;
	cout << "Size after compression: " << compressedlength << " bits" << endl;
	bString.writebit(0); //To Flush
	bString.Write2Stream(outputbuffer);
	cout << "compression done" << endl;
}

void LZ77DeCompressStream2Stream(std::vector<char> *inputbuffer, std::vector<char> *outputbuffer) {
	int posindecodedstream = 0;
	cout << "DeCompress LZ77" << endl;
	for (int i = 0; i<inputbuffer->size(); i++) {
		if ((unsigned char)(*inputbuffer)[i] == LZ77Steuerzeichen)
		{
			if ((unsigned char)(*inputbuffer)[i + 1] == char(0)) {
				outputbuffer->push_back(LZ77Steuerzeichen);
				i++;
				posindecodedstream++;
			}
			else {
				int position = posindecodedstream - 1 - ((unsigned char)(*inputbuffer)[i + 2] * 256 + (unsigned char)(*inputbuffer)[i + 3]);
				for (int k = 1; k<(int)(*inputbuffer)[i + 1]; k++) {
					posindecodedstream++;
					outputbuffer->push_back((unsigned char)(*outputbuffer)[position + k]);
				}
				i += 3;
			}
		}
		else {
			outputbuffer->push_back((unsigned char)(*inputbuffer)[i]);
			posindecodedstream++;
		}
	}
}


void LZ77DeCompressFile2Stream(const char *inname, std::vector<char> *outputbuffer) {
	std::vector<char> buffer;
	std::ifstream myfile;
	myfile.open(inname, ios::out | ios::binary);
	myfile.seekg(0, myfile.end);
	int filesize = (int)myfile.tellg();
	myfile.seekg(0, myfile.beg);
	cout << "filesize: " << filesize << endl;
	buffer.resize(filesize);
	myfile.read(&buffer[0], filesize);
	myfile.close();
	LZ77DeCompressStream2Stream(&buffer, outputbuffer);
}


void LZ77DeCompressFile2File(const char *inname, const char *outname) {
	std::vector<char> buffer;
	LZ77DeCompressFile2Stream(inname, &buffer);
	ofstream myfile2;
	myfile2.open(outname, ios::out | ios::binary);
	for (unsigned int i = 0; i<buffer.size(); i++)
		myfile2 << buffer[i];
	myfile2.close();
}



void LZ77CompressStream(std::vector<char> *buffer, std::vector<char> *outputbuffer) {
	int librarywindowsize = 1024;
	char currentsymbol;
	bool codieresteuerzeichen;
	long filesize = (long)buffer->size();
	unsigned short enc_length, enc_position;

	cout << "Compress LZ77" << endl;

	for (int i = 0; i<filesize; i++) {
		currentsymbol = (unsigned char)(*buffer)[i];
		if (currentsymbol == LZ77Steuerzeichen) codieresteuerzeichen = 1;
		else codieresteuerzeichen = 0;
		enc_length = 0;
		enc_position = 0;
		if (!codieresteuerzeichen)
			for (int l = MAX(i - librarywindowsize, 0); l<i - 3; l++) {
				int cl = 0;
				for (int k = l; k<i; k++) {
					if (((unsigned char)(*buffer)[k] != (unsigned char)(*buffer)[i + cl]) &&
						(i + cl<filesize)
						&& (cl<254)) {
						if (cl>3) {
							if (cl>enc_length) {
								enc_length = cl;
								enc_position = i - l;
							}
						}
						break;
					}
					cl++;
				}
			}
		if (enc_length == 0) outputbuffer->push_back(currentsymbol);
		else {
			outputbuffer->push_back(LZ77Steuerzeichen);
			outputbuffer->push_back(char(1 + enc_length));
			outputbuffer->push_back(char((enc_position - (enc_position % 256)) / 256));
			outputbuffer->push_back(char(enc_position % 256));
			i += enc_length - 1;
		}
		if (codieresteuerzeichen) {
			char x = char(0);
			outputbuffer->push_back(x);
		}
	}

}

void HuffmanCompressStream2File(std::vector<char> *buffer, const char *outputname) {
	std::vector<char> outbuffer;
	HuffmanCompressStream(buffer, &outbuffer);
	ofstream myfile2;
	myfile2.open(outputname, ios::out | ios::binary);
	for (unsigned int i = 0; i<outbuffer.size(); i++)
		myfile2 << outbuffer[i];
	myfile2.close();
}

void HuffmanCompressFile2File(const char *inputname, const char *outputname) {
	long filesize;
	std::ifstream myfile;
	myfile.open(inputname, ios::in | ios::binary);
	myfile.seekg(0, myfile.end);
	filesize = (long)myfile.tellg();
	cout << "filesize: " << filesize << endl;
	myfile.seekg(0, myfile.beg);
	std::vector<char> buffer;
	std::vector<char> outbuffer;
	buffer.resize(filesize);
	myfile.read(&buffer[0], filesize);
	HuffmanCompressStream2File(&buffer, outputname);
}

void LZ77CompressStream2File(std::vector<char> *buffer, const char *outputname) {
	std::vector<char> outbuffer;
	LZ77CompressStream(buffer, &outbuffer);
	ofstream myfile2;
	myfile2.open(outputname, ios::out | ios::binary);
	for (unsigned int i = 0; i<outbuffer.size(); i++)
		myfile2 << outbuffer[i];
	myfile2.close();
}

void LZ77CompressFile2File(const char *inputname, const char *outputname) {
	long filesize;
	std::ifstream myfile;
	myfile.open(inputname, ios::in | ios::binary);
	myfile.seekg(0, myfile.end);
	filesize = (long)myfile.tellg();
	cout << "filesize: " << filesize << endl;
	myfile.seekg(0, myfile.beg);
	std::vector<char> buffer;
	std::vector<char> outbuffer;
	buffer.resize(filesize);
	myfile.read(&buffer[0], filesize);
	LZ77CompressStream2File(&buffer, outputname);
}

int main(int argc, char *argv[]) {

	//TEST        
	LZ77CompressFile2File("testfile.txt", "testfile.lz77");
	HuffmanCompressFile2File("testfile.lz77", "testfile.cop");
	HuffmanDeCompressFile2File("testfile.cop", "testfile-decomp.lz77");
	LZ77DeCompressFile2File("testfile-decomp.lz77", "testfile-out.txt");
	
	std::vector<char> buffer;
	HuffmanCompressFile2File("testbild.jpg","testfile.cop");
	HuffmanDeCompressFile2Stream("testfile.cop", &buffer);
	HuffmanCompressStream2File(&buffer,"testfile-reencoded.cop");
	HuffmanDeCompressFile2File("testfile-reencoded.cop", "testbild-decompress.jpg");
	
	system("Pause");
	return 0;
}

