#include <iostream>

int main()
{
	std::cout <<
		"https://projecteuler.net/problem=3\n" <<
		"The prime factors of 13195 are 5, 7, 13 and 29.\n" <<
		"What is the largest prime factor of the number 600851475143 ?\n\n";

	typedef long long INT;
	const INT number = 600851475143;

	auto checkPrime = [](INT x) {
		for (INT i = 2; i < x; i++) {
			if (x%i == 0) return false;
		}
		return true;
	};

	INT n = number;
	INT check = 1;
	int factor = 0;

	std::cout << "prime factors: ";

	for (int i = 2; i <= n; i++)
	{
		if (checkPrime(i))
		{
			if (n % i == 0)
			{
				n = n / i;
				check *= i;
				if (i > factor) factor = i;
				std::cout << i << " ";
			}
		}
	}
	if (number != check) std::cout << "\nerror!\n";

	std::cout << "\nThe largest prime factor of the number " << number << " is " << factor << "\n\n";

	getchar();
}