#include <iostream>
#include <vector>
#include <sstream>

std::vector<int> primes2;
std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

bool coprime(int a, int b) {
	int tmp;
	while (b != 0) {
		if (a < b) 
			std::swap(a, b);
		tmp = b;
		b = a % b;
		a = tmp;
	}
	if (a==1) return 1;	
	return 0;
}

int totient(int x) {
	int count = 0;
	for (int i = 2; i < x; i++)
		if (coprime(i,x))
			count++;
	return count+1;
}

// assumes number is product of two primes
int fasttotient(int x, int y) {	
	return (x-1)*(y-1);
}


bool permutation(int x, int y) {
	int nc[10] = { 0 };
	std::string sx = std::to_string(x);
	std::string sy = std::to_string(y);
	if (sx.size() != sy.size())
		return false;
	for (int i = 0; i < sx.size(); i++) {
		nc[sx[i] - 48]++;
		nc[sy[i] - 48]--;
	}
	for (int i = 0; i < 10; i++)
		if (nc[i] != 0)
			return false;
	return true;
}

int main() {	
	CreatePrimeList(primes, 10000);
	for (auto p : primes)
		if (p > 1000)
			primes2.push_back(p);

	std::cout << "Primes between 1000 and 10000: " << primes2.size() << "\n";

	double min = 1E8;
	
	for (int i=0;i<primes2.size();i++)
		for (int l = i+1; l < primes2.size(); l++)
		{			
			int nr = primes2[i] * primes2[l];
			if (nr >= 1E7) break;

			int tot = fasttotient(primes2[i], primes2[l]);
			if (permutation(nr, tot))
			{								
				if ((double)nr / (double)tot < min) {										
					std::cout << nr << "	" << tot << "	" << (double)nr / (double)tot << "\n";	
					min = (double)nr / (double)tot;
				}
			}
		}
	
	getchar();
}