// Euler Project Problem 24 Lexicographic Permut.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
	int i, s;
	int x[10] = { 0,1,2,3,4,5,6,7,8,9};
	int len = 10;

	for (int u = 0; u < 1000000-1; u++)
	{		
		for (s = len-2; s >= 0; s--)
			if (x[s] < x[s + 1])
				break;

		int m = s + 1;
		for (i = s + 1; i < len; i++)
			if (x[i] > x[s] && x[i] < x[m])
				m = i;		
		std::swap(x[s], x[m]);

		//sort
		for (int k=s+1;k< len-1;k++)
			for (int q = s+1; q < len-1; q++)
				if (x[q] > x[q + 1])
					std::swap(x[q], x[q + 1]);
	}
	
	for (int k = 0; k < len; k++)
		std::cout << x[k];
	std::cout << "\n";

}
