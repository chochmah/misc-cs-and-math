#include <iostream>
#include <vector>
#include <algorithm>

const int maximum = 1E6;
int ts[maximum] = { 0 };
int visited[maximum] = { 0 };
int maxlen = 0;

int main() {
	for (int q = 1; q < maximum; q++) {
		ts[q] = 0;
		visited[q] = 0;
	}

	for (int i = 2; i < maximum; i++) {
			int k = i;
			int mul = 1;
			do {				
				ts[k] += mul;
				k += i;
				mul++;
			} while (k < maximum);			
	}

	for (int i = 12496; i < maximum; i++) {
		//std::cout << i << "\n";
		std::vector<int> trace;
		int current = i;
		do {
			trace.push_back(current);

			current = ts[current];
			//---------------------------------------------
			if (current >= maximum) {
				for (auto t : trace)
					visited[t] = 8;
				break;
			}
			if (visited[current] == 8) {
				for (auto t : trace)
					visited[t] = 8;
				break;
			}

			//---------------------------------------------
			if (std::find(trace.begin(), trace.end(),current) != trace.end()) {
				for (auto t : trace)
					visited[t] = 8;
				if (trace.size() > maxlen) {
					trace.push_back(current);
					std::cout << trace.size() << "<\n";
					for (int u = 0; u < trace.size(); u++)
						std::cout << trace[u] << " ";
					std::cout << "\n";
					maxlen = trace.size();
				}
				break;
			}
		} while (true);
	}

	getchar();
}