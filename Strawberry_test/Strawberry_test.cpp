
#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>


struct teststruct {
	int x = 123123;
	std::string m1 = "This is a test";
	std::string m2 = "Strawberry";
};


int main()
{	
	std::string filename = "test.bin";
	std::fstream s(filename,std::ios::binary | std::ios::trunc | std::ios::in | std::ios::out);
	if (!s.is_open()) {
		std::cout << "could not open " << filename << '\n';
	}
	else {
		
		// write 2 structs
		teststruct d;
		s.write((char*)(&d), sizeof d);
		
		teststruct *d2 = new teststruct;
		d2->m2 = "Even more Strawberries!";
		s.write((char*)(&d), sizeof d);

	
		s.seekp(0); // only necessary because we haven't closed the file between w and r

		// read the structs from the file
		while(s.read((char*)(&d), sizeof d)) // binary input		
			std::cout << "read back: " << d.x << ' ' << d.m1 << ' ' << d.m2 << '\n';
	}

	return(1);
}

