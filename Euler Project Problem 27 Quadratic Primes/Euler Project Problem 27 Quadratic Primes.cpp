// Euler Project Problem 27 Quadratic Primes.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>

std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

bool prime(int x)
{	
	if (abs(x) > 130000)
	{
		std::cout << "Error\n";		
	}
	return (natNumbers[abs(x)]) ? true : false;
}

int main()
{	
	CreatePrimeList(primes,130000);	
	std::cout << "start\n";

	int maxN = 0;

	for (int i = -1000; i < 1000; i++)
		for (int l = -1000; l <= 1000; l++)
		{			
			for (int n=0;n<80;n++)
			{ 
				if (prime(n*n+i*n + l))
				{
					if (n > maxN)
					{
						maxN = n;
						std::cout << maxN << "  i:" << i << " l:" << l << " val:" << (long)n * (long)n + (long)i * (long)n + (long)l << "    " << i * l << "\n";
					}					
				}
				else
					break;
			}

		}	
}