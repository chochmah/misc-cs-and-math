#include "stdafx.h"
#include <vector>
#include <iostream>
#include <chrono>
#include <ctime>
#include <math.h>
#include <functional>
#include <sstream> 
#include <fstream>
#include <set>
#include <cstdint>

#define _CRT_SECURE_NO_WARNINGS


//-----------------------------------------------------------------------------
namespace EULER050EULER051
{
	typedef long long INT;
	const INT Limit		= 100000;

	std::vector<INT>	primes;
	std::vector<bool>	net(Limit + 1);

	void createPrimeNumberTable()
	{
		// create table of prime numbers using the dragnet of Erasthothenes		

		for (INT i = 0; i < Limit; i++)
		{
			net[(size_t)i] = true;
		}
		net[0] = 0;
		net[1] = 0;
		net[2] = 1;

		INT pivot = 1;
		INT pivotElement;
		do
		{
			for (INT l = pivot + 1; l < Limit; l++)
			{
				if (net[(size_t)l] == true)
				{
					pivot = l;
					goto ok;
				}
			}
			goto fertig;
		ok:

			pivotElement = pivot;
			INT testElement = pivotElement * 2;

			while (testElement < Limit)
			{
				net[(size_t)testElement] = false;
				testElement += pivotElement;
			}
		} while (pivot <= Limit);

	fertig:
		for (INT i = 0; i < Limit; i++)
			if (net[(size_t)i] == true) primes.push_back(i);

		std::cout << "Primes below " << Limit << " : " << primes.size() << "\n";
	}

	void Problem050()
	{
		/*https://projecteuler.net/problem=50

		The prime 41, can be written as the sum of six consecutive primes :
		41 = 2 + 3 + 5 + 7 + 11 + 13
		This is the longest sum of consecutive primes that adds to a prime below one - hundred.
		The longest sum of consecutive primes below one - thousand that adds to a prime, contains 21 terms, and is equal to 953.
		Which prime, below one - million, can be written as the sum of the most consecutive primes ?

		Solution:
		1. create list of all primes below 1.000.000
		2. reverse exhaustion*/

		// 2)
		INT bestPrime = 0;
		INT bestFactors = 0;

		for (size_t p = primes.size() - 1; p > 0; p--)
		{
			std::vector<INT> factors;
			INT prime = primes[p];
			for (int i = 0; i < p; i++)
			{
				//factors.clear();
				INT sum = 0;
				for (int l = i; l < p; l++)
				{
					if (sum > primes[p]) break;

					sum += primes[l];
					//factors.push_back(primes[l]);
					if (sum == primes[p])
					{
						if (l - i + 1 > bestFactors)
						{
							bestPrime = primes[p];
							bestFactors = l - i + 1;

							std::cout << bestPrime << " " << bestFactors << "\n";
						}
					}
				}
			}

		}

	}

	void linearsolver51()
	{
		// 1)
		createPrimeNumberTable();

		// https://projecteuler.net/problem=50
		// Prime triples and geometric sequences
		// Problem 518

		INT totalSum = 0;
		INT checks = 0;
		INT hits = 0;

		for (size_t i = 0; i < primes.size(); i++)
		{
			if (i % 1000 == 0)
			{
				std::cout << i << "	sum: " << totalSum << "	checks: " << checks << "	hits: " << hits << "\n";
				checks = 0;
				hits = 0;
			}

			INT a = primes[i];
			INT a_s = primes[i] + 1;
			INT factor_1, factor_2;

			for (size_t l = i + 1; l < primes.size(); l++)
			{
				INT b = primes[l];
				INT b_s = b + 1;

				factor_1 = b_s;
				factor_2 = a_s;

				INT c_s = b_s * factor_1;
				if (c_s % factor_2 != 0)
				{
					continue;
				}

				c_s = c_s / factor_2;

				INT c = c_s - 1;
				if (c > Limit) break;

				checks++;

				if (net[(size_t)c] == true)
				{
					hits++;
					//std::cout << "found one: " << a << " " << b << " " << c << "\n";
					totalSum += a + b + c;
				}
			}

		}
		std::cout << "sum: " << totalSum << "\n";
	}
}



//-----------------------------------------------------------------------------
namespace EULER005
{
	void Problem()
	{
		// https://projecteuler.net/problem=5
		// can be done with a calculator
	}
}

	

//-----------------------------------------------------------------------------
namespace EULER110
{
	void Problem()
	{
		typedef long long INT;

		auto num_factors = [](INT x) {
			int count = 0;
			int i = 2;
			do {
				if (x%i == 0)
				{
					x = x / i;
					count++;
					i = 2;
				}
				else i++;
			} while (x != 1);
			return count;
		};

		int count = num_factors(1260);
		std::cout << count << "\n\n";
		return;


		int i = 2;
		do
		{
			int n = num_factors(i*i) - 1;
			count += n;
			//std::cout << i << "	"  << n << "\n";
			i++;
		} while ((i - 1) < 400000);
	}
}

//-----------------------------------------------------------------------------
namespace EULER025
{
	void Problem()
	{
		// https://projecteuler.net/problem=25

		int digits[3][1000];
		for (int k = 0; k < 3; k++) for (int m = 0; m < 1000; m++) digits[k][m] = 0;

		digits[0][0] = 1;
		digits[1][0] = 1;

		int index0 = 0;
		int index1 = 1;
		int index2 = 2;

		int counter = 2;

		do {
			counter++;

			int overflow = 0;
			for (int i = 0; i < 1000; i++)
			{
				int ad = digits[index0][i] + digits[index1][i] + overflow;
				if (ad == 0) break;
				overflow = (ad - ad % 10) / 10;
				ad = ad % 10;
				digits[index2][i] = ad;
				std::cout << digits[index2][i];
			}
			std::cout << "	" << counter << "\n";

			if (digits[index2][10] != 0) break;

			int buf0 = index0;
			int buf1 = index1;
			int buf2 = index2;

			index0 = buf2;
			index1 = buf0;
			index2 = buf1;

		} while (true);

		std::cout << counter << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER010
{
	void Problem()
	{
		// https://projecteuler.net/problem=10

		EULER050EULER051::createPrimeNumberTable();

		long long sum = 0;
		for (auto p : EULER050EULER051::primes) sum += p;

		std::cout << "The sum of all primes below 2.000.000 is " << sum << "\n\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER011
{
	void Problem()
	{
		// https://projecteuler.net/problem=11

		int const data[20][20] = {
			{ 8, 2, 22, 97, 38, 15, 00, 40, 00, 75, 4, 5, 7, 78, 52, 12, 50, 77, 91, 8 },
			{ 49, 49, 99, 40, 17, 81, 18, 57, 60, 87, 17, 40, 98, 43, 69, 48, 4, 56, 62, 00 },
			{ 81, 49, 31, 73, 55, 79, 14, 29, 93, 71, 40, 67, 53, 88, 30, 3, 49, 13, 36, 65 },
			{ 52, 70, 95, 23, 4, 60, 11, 42, 69, 24, 68, 56, 1, 32, 56, 71, 37, 2, 36, 91 },
			{ 22, 31, 16, 71, 51, 67, 63, 89, 41, 92, 36, 54, 22, 40, 40, 28, 66, 33, 13, 80 },
			{ 24, 47, 32, 60, 99, 3, 45, 2, 44, 75, 33, 53, 78, 36, 84, 20, 35, 17, 12, 50 },
			{ 32, 98, 81, 28, 64, 23, 67, 10, 26, 38, 40, 67, 59, 54, 70, 66, 18, 38, 64, 70 },
			{ 67, 26, 20, 68, 2, 62, 12, 20, 95, 63, 94, 39, 63, 8, 40, 91, 66, 49, 94, 21 },
			{ 24, 55, 58, 5, 66, 73, 99, 26, 97, 17, 78, 78, 96, 83, 14, 88, 34, 89, 63, 72 },
			{ 21, 36, 23, 9, 75, 00, 76, 44, 20, 45, 35, 14, 00, 61, 33, 97, 34, 31, 33, 95 },
			{ 78, 17, 53, 28, 22, 75, 31, 67, 15, 94, 3, 80, 4, 62, 16, 14, 9, 53, 56, 92 },
			{ 16, 39, 5, 42, 96, 35, 31, 47, 55, 58, 88, 24, 00, 17, 54, 24, 36, 29, 85, 57 },
			{ 86, 56, 00, 48, 35, 71, 89, 7, 5, 44, 44, 37, 44, 60, 21, 58, 51, 54, 17, 58 },
			{ 19, 80, 81, 68, 5, 94, 47, 69, 28, 73, 92, 13, 86, 52, 17, 77, 4, 89, 55, 40 },
			{ 4, 52, 8, 83, 97, 35, 99, 16, 7, 97, 57, 32, 16, 26, 26, 79, 33, 27, 98, 66 },
			{ 88, 36, 68, 87, 57, 62, 20, 72, 3, 46, 33, 67, 46, 55, 12, 32, 63, 93, 53, 69 },
			{ 4, 42, 16, 73, 38, 25, 39, 11, 24, 94, 72, 18, 8, 46, 29, 32, 40, 62, 76, 36 },
			{ 20, 69, 36, 41, 72, 30, 23, 88, 34, 62, 99, 69, 82, 67, 59, 85, 74, 4, 36, 16 },
			{ 20, 73, 35, 29, 78, 31, 90, 1, 74, 31, 49, 71, 48, 86, 81, 16, 23, 57, 5, 54 },
			{ 1, 70, 54, 71, 83, 51, 54, 69, 16, 92, 33, 48, 61, 43, 52, 1, 89, 19, 67, 48 }
		};

		int max_sum = 0;

		for (int i = 0; i < 20 - 3; i++)
			for (int l = 0; l < 20; l++)
			{
				int t_sum = data[i][l] * data[i + 1][l] * data[i + 2][l] * data[i + 3][l];
				if (max_sum < t_sum) max_sum = t_sum;
			}

		for (int i = 0; i < 20 - 3; i++)
			for (int l = 0; l < 20; l++)
			{
				int t_sum = data[l][i] * data[l][i + 1] * data[l][i + 2] * data[l][i + 3];
				if (max_sum < t_sum) max_sum = t_sum;
			}
		for (int i = 0; i < 20 - 3; i++)
			for (int l = 0; l < 20 - 3; l++)
			{
				int t_sum = data[l][i] * data[l + 1][i + 1] * data[l + 2][i + 2] * data[l + 3][i + 3];
				if (max_sum < t_sum) max_sum = t_sum;
			}

		for (int i = 0; i < 20 - 3; i++)
			for (int l = 3; l < 20; l++)
			{
				int t_sum = data[l][i] * data[l - 1][i + 1] * data[l - 2][i + 2] * data[l - 3][i + 3];
				if (max_sum < t_sum) max_sum = t_sum;
			}


		std::cout << "Largest sum is " << max_sum << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER012
{
	void Problem()
	{
		// https://projecteuler.net/problem=12
		// Highly divisible triangular number	
		// What is the value of the first triangle number to have over five hundred divisors?	

#define triagNr(n)  (n*(n+1) / 2)

		auto factors = [](int n) { // returns number of factors of n		
			int facs = 0;
			for (int k = 2; k < sqrt(n) + 1; k++)
				if (n%k == 0) facs += 2;
			return facs;
		};

		int i = 0;
		for (i = 0; factors(triagNr(i)) < 500; i++);
		std::cout << "smallest triag number with >=500 factors: " << triagNr(i) << "\n\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER015
{
	void Problem()
	{
		// https://projecteuler.net/problem=15	
		// = (40 over 20)

		const int n = 20 + 1;
		long long grid[n][n];

		for (int i = 0; i < n; i++) {
			grid[i][n - 1] = 1;
			grid[n - 1][i] = 1;
		}

		for (int i = n - 2; i >= 0; i--)
			for (int l = n - 2; l >= 0; l--)
				grid[i][l] = grid[i][l + 1] + grid[i + 1][l];

		std::cout << "There are " << grid[0][0] << " paths\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER0016
{
	void Problem()
	{
		// https://projecteuler.net/problem=16

		std::vector<short> n(1, 1);

		for (size_t i = 0; i < 1000; i++) {
			short overflow = 0;
			for (size_t k = 0; k < n.size(); k++) {
				short b = n[k] * 2 + overflow;
				overflow = (b - b % 10) / 10;
				n[k] = b % 10;
			}
			if (overflow > 0) n.push_back(overflow);
		}

		int ret = 0;
		for (auto z : n) ret += z;

		std::cout << "the sum of the digits of the number 2^1000 is: " << ret << "\n\n";
	}
}



//-----------------------------------------------------------------------------
namespace EULER007
{
	void Problem()
	{
		// https://projecteuler.net/problem=7
		// What is the 10001st prime number?

		struct mType { int p; int c; };
		std::vector<mType> e(1, { 3, 2 });

		int n = 1;
		do {
		ok:
			n += 2;
			for (size_t i = 0; i < e.size(); i++)
				while (e[i].p*e[i].c <= n) {
					e[i].c++;
					if (e[i].p*(e[i].c - 1) == n) goto ok;
				}
			e.push_back({ n, 2 });
		} while (e.size() < 100001);

		std::cout << e.back().p << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER008
{
	void Problem()
	{
		// https://projecteuler.net/problem=8
		// Find the thirteen adjacent digits in the 1000-digit number that 
		// have the greatest product. What is the value of this product?

		std::stringstream s("7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450");

		int ar[1000];

		char c;
		int i = 0;
		while (s.get(c)) {
			ar[i] = (char)c - 48;
			i++;
		}

		long long max = 0;
		for (int i = 0; i < 1000 - 13; i++)
		{
			long long sum = 1;
			for (int l = i; l < i + 13; l++)
				sum *= ar[l];
			if (sum > max) {
				for (int l = i; l < i + 13; l++)
					std::cout << ar[l];
				max = sum;
			}
		}
		std::cout << "largest product: " << max << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER014
{
	void Problem()
	{
		// https://projecteuler.net/problem=14

		struct maxStrucxt { uint32_t max_cain; uint32_t max_index; } maxe{ 0, 0 };

		std::vector<uint32_t> table(1000000, 0);
		table[1] = 1;

		for (uint32_t i = 1; i < 1000000; i++) {
			if (table[i] != 0) continue;
			uint32_t n = i;
			uint32_t count = 0;
			while (n != 1) {
				n = (n % 2) ? 3 * n + 1 : n / 2;
				count++;
				if (n < 1000000 && table[n] != 0) {
					count += table[n];
					break;
				}
			}

			table[i] = count;
			if (count > maxe.max_cain) maxe = { count, i };
		}
		std::cout << "Max starting number: " << maxe.max_index << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER020
{
	void Problem()
	{
		// https://projecteuler.net/problem=20
		// solve with cauchy product

		const int n_max = 100;
		std::vector<int> n(1, 1);

		for (int i = 2; i <= n_max; i++) {

			std::vector<int> m(n.size(), 0);
			//copy n+1 number into polynom
			uint32_t k = 0;
			do {
				int b = i % (int)pow(10, k);
				int a = i - b * (int)pow(10, k);
				m[k] = a;
				k++;
			} while ((size_t)k < m.size() && (int)pow(10, k + 1) < i);

			//multiply with n
			std::vector<int> res(n.size(), 0);
			for (size_t o = 0; o < n.size(); o++)
				for (size_t r = 0; r <= o; r++)
					res[o] += n[r] * m[o - r];
			n = res;

			//normalize the coefficents
			k = 0;
			int b = 0;
			do {
				b = (n[k] - n[k] % 10) / 10;
				if (b != 0) {
					n[k] -= b * 10;
					if (n.size() <= (size_t)(k + 1)) n.push_back(0);
					n[k + 1] += b;
				}
				k++;
			} while (b != 0 || size_t(k) < n.size());
		}

		long long sum = 0;
		std::cout << n_max << "! = ";
		for (size_t s = 0; s < n.size(); s++)
		{
			std::cout << n[n.size() - s - 1] << "";
			sum += n[s];
		}

		std::cout << "\n\nsum of the digits in the number 100! : " << sum << "\n\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER013
{
	void Problem()
	{
		// https://projecteuler.net/problem=13
		// Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.

		std::string		data = "37107287533902102798797998220837590246510135740250463769376774900097126481248969700780504170182605387432498619952474105947423330951305812372661730962991942213363574161572522430563301811072406154908250230675882075393461711719803104210475137780632466768926167069662363382013637841838368417873436172675728112879812849979408065481931592621691275889832738442742289174325203219235894228767964876702721893184745144573600130643909116721685684458871160315327670386486105843025439939619828917593665686757934951621764571418565606295021572231965867550793241933316490635246274190492910143244581382266334794475817892575867718337217661963751590579239728245598838407582035653253593990084026335689488301894586282278288018119938482628201427819413994056758715117009439035398664372827112653829987240784473053190104293586865155060062958648615320752733719591914205172558297169388870771546649911559348760353292171497005693854370070576826684624621495650076471787294438377604532826541087568284431911906346940378552177792951453612327252500029607107508256381565671088525835072145876576172410976447339110607218265236877223636045174237069058518606604482076212098132878607339694128114266041808683061932846081119106155694051268969251934325451728388641918047049293215058642563049483624672216484350762017279180399446930047329563406911573244438690812579451408905770622942919710792820955037687525678773091862540744969844508330393682126183363848253301546861961243487676812975343759465158038628759287849020152168555482871720121925776695478182833757993103614740356856449095527097864797581167263201004368978425535399209318374414978068609844840309812907779179908821879532736447567559084803087086987551392711854517078544161852424320693150332599594068957565367821070749269665376763262354472106979395067965269474259770973916669376304263398708541052684708299085211399427365734116182760315001271653786073615010808570091499395125570281987460043753582903531743471732693212357815498262974255273730794953759765105305946966067683156574377167401875275889028025717332296191766687138199318110487701902712526768027607800301367868099252546340106163286652636270218540497705585629946580636237993140746255962240744869082311749777923654662572469233228109171419143028819710328859780666976089293863828502533340334413065578016127815921815005561868836468420090470230530811728164304876237919698424872550366387845831148769693215490281042402013833512446218144177347063783299490636259666498587618221225225512486764533677201869716985443124195724099139590089523100588229554825530026352078153229679624948164195386821877476085327132285723110424803456124867697064507995236377742425354112916842768655389262050249103265729672370191327572567528565324825826546309220705859652229798860272258331913126375147341994889534765745501184957014548792889848568277260777137214037988797153829820378303147352772158034814451349137322665138134829543829199918180278916522431027392251122869539409579530664052326325380441000596549391598795936352974615218550237130764225512118369380358038858490341698116222072977186158236678424689157993532961922624679571944012690438771072750481023908955235974572318970677254791506150550495392297953090112996751986188088225875314529584099251203829009407770775672113067397083047244838165338735023408456470580773088295917476714036319800818712901187549131054712658197623331044818386269515456334926366572897563400500428462801835170705278318394258821455212272512503275512160354698120058176216521282765275169129689778932238195734329339946437501907836945765883352399886755061649651847751807381688378610915273579297013376217784275219262340194239963916804498399317331273132924185707147349566916674687634660915035914677504995186714302352196288948901024233251169136196266227326746080059154747183079839286853520694694454072476841822524674417161514036427982273348055556214818971426179103425986472045168939894221798260880768528778364618279934631376775430780936333301898264209010848802521674670883215120185883543223812876952786713296124747824645386369930090493103636197638780396218407357239979422340623539380833965132740801111666627891981488087797941876876144230030984490851411606618262936828367647447792391803351109890697907148578694408955299065364044742557608365997664579509666024396409905389607120198219976047599490197230297649139826800329731560371200413779037855660850892521673093931987275027546890690370753941304265231501194809377245048795150954100921645863754710598436791786391670211874924319957006419179697775990283006991536871371193661495281130587638027841075444973307840789923115535562561142322423255033685442488917353448899115014406480203690680639606723221932041495354150312888033953605329934036800697771065056663195481234880673210146739058568557934581403627822703280826165707739483275922328459417065250945123252306082291880205877731971983945018088807242966198081119777158542502016545090413245809786882778948721859617721078384350691861554356628840622574736922845095162084960398013400172393067166682355524525280460972253503534226472524250874054075591789781264330331690";
		size_t			sum = 0;

		for (size_t i = 0; i < 50; i++) {
			for (size_t k = 0; k < 100; k++) sum += data[k * 50 + (49 - i)] - 48;
			data[49 - i] = (char)(sum % 10 + 48);
			sum = (sum - sum % 10) / 10;
		}

		std::cout << std::to_string(sum) + data.substr(0, 50) << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER021
{
	void Problem()
	{
		// https://projecteuler.net/problem=21	

		std::vector<int> t(10001, 1);

		for (int i = 3; i < 10001; i++) {
			for (int l = 2; l*l < i; l++)
				t[i] += (!(i%l)) ? (l*l == i) ? l : l + i / l : 0;
			t[0] += (i > t[i] && i == t[t[i]]) ? i + t[i] : 0;
		}

		std::cout << "sum of all amicable numbers under 10.000 : " << t[0] - 1 << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER025B
{
	void Problem()
	{
		// https://projecteuler.net/problem=25

		std::vector<int>	t[2];
		t[0].push_back(1);
		t[1].push_back(1);

		int counter = 2;
		bool active = 0;

		do {
			int overflow = 0;
			for (size_t i = 0; i < t[active].size(); i++) {
				int n = t[!active][i] + t[active][i] + overflow;
				t[!active][i] = n % 10;
				overflow = (n - (n % 10)) / 10;
			}
			if (overflow != 0) {
				t[!active].push_back(overflow);
				t[active].push_back(0);
			}
			counter++;
			active = !active;
		} while (t[active].size() < 1000);

		std::cout << "index: " << counter << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER051
{
	void Problem()
	{
		using namespace EULER050EULER051;

		// using dragnet Erasthothenes			

		for (INT i = 0; i < Limit; i++)
		{
			net[(size_t)i] = true;
		}
		net[0] = 0;
		net[1] = 0;
		net[2] = 1;

		INT pivot = 1;
		INT pivotElement;
		do
		{
			for (INT l = pivot + 1; l < Limit; l++)
			{
				if (net[(size_t)l] == true)
				{
					pivot = l;
					goto ok;
				}
			}
			goto fertig;
		ok:

			pivotElement = pivot;
			INT testElement = pivotElement * 2;

			while (testElement < Limit)
			{
				net[(size_t)testElement] = false;
				testElement += pivotElement;
			}
		} while (pivot <= Limit);

	fertig:

		for (int i = 10000; i < 99999; i++)
		{

		}

		std::cout << "fertig\n" << "\n";
	}
}

//-----------------------------------------------------------------------------
namespace DAYLIPROGRAMMER261
{
	/*

	15 14  1  4
	12  6  9  7
	2  11  8 13
	5   3 16 10

	1 0 1 0
	0 0 1 1
	0 1 0 1
	1 1 0 0

	*/
	//-------------------------------------------------------
	/*
	uint16_t t[s*s] = {
	20, 19, 38, 30, 31, 36, 64, 22,
	8,16, 61, 53, 1, 55, 32, 34,
	33, 60, 25, 9, 26, 50, 13, 44,
	37, 59, 51, 4, 41, 6, 23, 39,
	58, 35, 2, 48, 10, 40, 46, 21,
	62, 11, 54, 47, 45, 7, 5, 29,
	18, 57, 17, 27, 63, 14, 49, 15,
	24, 3, 12, 42, 43, 52, 28, 56 };
	*/


	constexpr uint16_t s = 24;
#define sum  0.5 * (s*(s*s + 1))
#define e(x,y) t[x+y*s]

	struct SieveType {
		uint16_t a[s];
		SieveType() {
			for (int i = 0; i < s; i++)
				a[i] = 0;
		}
		SieveType put(int p) {
			SieveType s_new = *this;
			s_new.a[p] = 1;
			return s_new;
		}
	};


	/*
	uint16_t t[s*s] = {
	161,43,265,55,169,201,241,284,234,192,20,200,221,337,311,176,92,137,286,385,
	363,281,290,242,344,141,121,199,215,283,143,144,261,73,125,373,116,130,145,21,
	71,372,191,90,134,153,148,333,177,41,278,139,110,151,358,275,198,366,263,162,
	335,356,203,64,247,76,258,94,129,306,316,27,60,188,288,204,376,276,11,196,
	338,277,323,78,83,95,49,61,307,245,54,183,246,365,113,396,327,168,273,29,
	158,69,187,364,86,287,155,236,259,186,182,79,37,238,132,217,103,315,328,392,
	354,52,2,142,70,254,383,285,53,296,314,264,233,271,362,212,124,106,81,152,
	58,322,208,75,31,260,190,87,357,295,57,400,269,150,303,7,341,42,351,207,
	149,343,331,223,85,24,165,329,32,39,397,300,257,274,140,220,99,225,377,1,
	391,19,136,120,301,15,164,249,65,318,166,346,230,138,339,272,175,167,299,100,
	213,317,184,367,387,210,375,115,35,193,72,102,91,348,38,374,80,321,18,170,
	180,111,325,156,218,361,89,378,104,36,393,47,235,44,63,289,28,388,394,171,
	50,93,74,389,381,46,268,14,308,16,309,231,282,126,304,371,297,17,194,240,
	146,135,370,380,26,119,248,294,267,243,147,206,117,173,324,122,270,45,23,355,
	280,88,237,345,108,347,251,298,214,34,154,205,330,163,4,66,227,279,252,128,
	127,13,224,266,232,157,313,12,253,350,319,114,302,5,384,68,256,369,67,179,
	22,352,96,293,195,291,209,59,211,109,292,399,98,160,62,202,305,360,244,51,
	222,320,25,9,189,395,228,185,310,368,133,262,97,398,8,33,239,77,172,340,
	336,250,255,178,390,219,48,112,379,342,159,30,353,82,40,10,226,216,3,382,
	56,197,84,174,334,359,107,386,101,118,105,332,181,326,312,123,131,6,229,349};
	*/
	//Grid 4

	/*
	uint16_t t[s*s] = {
	38,55,128,137,24,60,62,25,54,27,119,141,
	81,111,51,18,73,82,64,94,19,133,29,115,
	72,11,59,61,124,136,95,65,76,66,101,4,
	44,12,126,112,30,74,88,58,79,127,49,71,
	102,97,125,28,67,23,48,68,142,32,122,16,
	21,14,103,87,139,45,107,77,36,131,109,1,
	52,118,34,96,63,6,33,120,104,13,121,110,
	113,143,10,35,53,46,5,89,123,138,37,78,
	99,15,86,42,41,92,100,69,90,3,93,140,
	132,57,40,50,7,117,83,39,84,75,56,130,
	85,129,106,134,114,98,80,22,20,9,26,47,
	31,108,2,70,135,91,105,144,43,116,8,17 };
	*/
	//Grid 5: 16x16
	/*
	uint16_t t[s*s] = {
	183,209,124,52,167,165,57,56,47,180,198,232,60,76,121,129,
	62,116,82,10,225,114,70,213,184,246,249,112,201,41,37,94,
	205,208,39,83,138,151,132,38,26,87,69,211,186,251,109,123,
	139,245,181,119,68,182,115,122,238,1,173,8,16,137,73,239,
	13,12,91,156,226,196,144,192,51,223,145,45,193,117,172,80,
	110,17,244,237,134,21,159,96,86,242,74,206,84,162,43,141,
	195,113,5,127,200,29,250,107,185,157,255,176,18,108,104,27,
	229,42,148,101,30,120,61,158,152,252,219,35,203,63,189,54,
	136,217,85,243,34,214,199,111,147,3,58,36,174,53,253,93,
	236,48,216,234,194,103,32,81,97,99,75,20,100,190,98,233,
	19,40,125,2,128,230,241,163,256,67,7,235,140,177,14,212,
	50,254,248,143,142,28,88,131,6,64,133,95,118,231,220,105,
	65,11,126,150,166,228,207,171,247,78,23,191,59,102,161,71,
	66,168,164,187,92,77,224,130,90,9,135,106,227,175,4,202,
	188,210,153,15,33,154,31,215,155,178,22,179,55,24,240,204,
	160,146,25,197,79,44,46,72,89,170,221,169,222,149,218,49 };


	*/

	uint16_t t[s*s] = {
		91,414,77,295,118,373,398,395,132,466,188,110,251,499,363,115,176,406,326,557,270,171,380,353,
		88,220,514,378,325,65,316,354,144,219,533,408,177,25,352,189,526,347,488,366,55,138,215,482,
		258,242,324,19,570,332,204,247,389,239,259,263,441,365,73,440,530,225,560,274,212,328,129,1,
		234,443,419,32,344,337,545,277,191,136,261,376,431,175,294,276,320,134,85,89,418,517,520,70,
		454,202,410,116,47,107,99,306,233,207,235,355,167,252,480,23,463,433,172,510,464,284,458,447,
		257,546,287,462,178,273,349,121,442,211,221,265,87,68,457,194,256,12,495,468,559,260,296,160,
		346,504,218,384,67,84,321,27,444,226,313,139,538,164,360,341,130,451,549,51,438,285,386,158,
		512,141,554,227,183,417,319,114,146,487,399,377,192,450,187,424,102,231,519,140,314,244,142,103,
		198,452,279,280,308,494,124,151,249,513,243,186,119,396,445,33,299,509,80,407,193,563,41,362,
		401,283,214,298,403,550,165,413,383,404,534,409,56,331,35,184,291,304,61,540,28,39,271,327,
		16,364,181,152,461,575,345,571,536,174,397,127,382,392,9,155,490,477,369,4,15,481,173,78,
		179,456,460,368,335,423,437,553,264,49,213,476,434,245,113,81,106,250,2,96,303,361,229,491,
		569,18,196,539,547,69,293,137,162,573,120,272,5,255,515,48,312,262,237,531,356,90,267,551,
		148,95,44,50,387,305,300,342,412,562,472,429,500,528,159,180,166,374,493,307,100,21,521,29,
		209,561,254,302,340,133,311,22,11,370,333,505,310,93,485,535,402,71,58,157,400,503,556,3,
		529,75,323,286,205,14,566,228,98,489,197,576,83,236,37,411,241,428,222,465,322,367,8,518,
		470,97,301,348,54,156,111,420,496,201,224,216,62,359,568,527,439,199,315,10,484,246,200,421,
		17,388,240,92,210,6,42,288,506,230,508,53,564,269,185,502,79,548,498,232,238,375,473,381,
		195,446,426,525,339,574,486,435,289,170,30,182,544,329,453,60,309,13,128,161,290,469,64,7,
		537,163,330,282,131,416,34,393,122,43,206,45,415,552,297,479,425,357,532,126,150,430,350,109,
		497,190,478,20,422,169,567,203,471,278,501,223,66,104,334,123,317,248,76,483,86,436,74,558,
		149,358,268,459,168,541,145,492,318,371,38,385,275,105,153,555,391,46,31,394,432,52,343,455,
		59,154,101,543,217,475,57,63,351,266,94,24,572,524,427,507,82,474,292,108,516,117,379,522,
		511,112,26,467,565,36,390,372,135,40,405,523,253,208,143,542,72,125,336,448,281,147,449,338 };


	uint16_t solution_count = 0;

	bool iter(uint16_t step, uint16_t sum1, uint16_t sum2, SieveType sieve) {
		uint16_t o = (step < s / 2) ? 0 : 1;
		for (uint16_t i = o; i < s; i++) {
			if (step == 2) std::cout << "step: " << i << " solutions: " << solution_count << "\n";
			if (sieve.a[i] == 0) {
				uint16_t s1 = sum1 + e(step, i);;
				uint16_t s2 = sum2 + e(s - 1 - step, i);

				if (s1 > sum || s2 > sum) continue;

				if (step == s - 1) {
					if (s1 == sum && s2 == sum) {
						solution_count++;
						std::cout << "solution found:\n";
						return true;
					}
					return false;
				}
				//iter(step + 1, s1, s2, sieve.put(i));

				if (iter(step + 1, s1, s2, sieve.put(i)) == true) {
					std::cout << "line: " << i << "\n";
					return true;
				}

			}
		}
		return false;
	}

	void dailyprogrammer261() {
		SieveType sieve;
		iter(0, 0, 0, sieve);
		std::cout << "solutions: " << 2 * solution_count << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER035
{
	void Problem()
	{
		// https://projecteuler.net/problem=35
		/* Circular primes
		The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.
		There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.
		How many circular primes are there below one million?
		*/		

		struct eStruct {
			unsigned char	z[6];
			bool			isPrime{ true };
		} x[1000000];

		#define loop(n) for (size_t n=0;n<10;n++)
		loop(i0) loop(i1) loop(i2) loop(i3) loop(i4) loop(i5) {
			size_t k = i0 + 10 * i1 + 100 * i2 + 1000 * i3 + 10000 * i4 + 100000 * i5;
			x[k].z[0] = (char)i0;
			x[k].z[1] = (char)i1;
			x[k].z[2] = (char)i2;
			x[k].z[3] = (char)i3;
			x[k].z[4] = (char)i4;
			x[k].z[5] = (char)i5;
		}

		for (int i = 2; i < sqrt(1000000); i++)
			if (x[i].isPrime == true)
				for (int z = 2 * i; z < 1000000; z += i)
					x[z].isPrime = false;

		int counter = 0;

		for (int i = 2; i < 1000000; i++) {
			if (x[i].isPrime) {
				int c = 6;
				while (x[i].z[c - 1] == 0 && c > 0)
					c--;
				for (int v = 0; v < c; v++)
					if (x[i].z[v] == 0) goto fertig;

				for (int v = 1; v < c; v++) {
					int n = 0;
					for (int m = 0; m < c; m++)
						n += x[i].z[(m + v) % c] * (int)pow(10, m);
					if (!x[n].isPrime) goto fertig;
				}
				std::cout << i << "\n";
				counter++;
			}
		fertig: int noop = 0;
		}

		std::cout << "number of circular primes < one million: " << counter << "\n";
	}
}


//-----------------------------------------------------------------------------
namespace EULER043
{
	static const uint16_t cmap[] = { 13,11,7,5,3,2 };

	void iterationSolver(uint16_t w, uint16_t s[10][2], long long int& sigma) {
		for (int i = 0; i < 10; i++)
			if (s[i][1] == 0 && (i * 100 + s[w + 2][0] * 10 + s[w + 1][0]) % cmap[w] == 0) {
				uint16_t s2[10][2];
				memcpy(s2, s, sizeof(uint16_t) * 20);
				s2[i][1] = 1;
				s2[w + 3][0] = i;

				if (w != 5)
					iterationSolver(w + 1, s2, sigma);
				else
					for (int m = 0; m < 10; m++)
						sigma += (s2[m][1] == 0) ? m * (long long int)pow(10, 9) : s2[m][0] * (long long int)pow(10, m);
			}
	}

	void Problem() {
		long long int sigma = 0;
		for (uint16_t val = 17; val < 1000; val += 17) {
			uint16_t s[10][2]{ 0 };

			s[0][0] = val % 10;
			s[1][0] = (val % 100 - s[0][0]) / 10;
			s[2][0] = (val - s[1][0]) / 100;

#define asval(x) {if (s[s[x][0]][1] == 1) continue; s[s[x][0]][1] = 1;}
			asval(0); asval(1); asval(2);

			iterationSolver(0, s, sigma);
		}

		std::cout << "solution: " << sigma << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace DAILYPROGRAMMER263
{	
	/*
	#include <map>
	#include <iomanip>
	
	void Problem()
	{
	// https://www.reddit.com/r/dailyprogrammer/comments/4fc896/20160418_challenge_263_easy_calculating_shannon/

	std::string x{"https://www.reddit.com/r/dailyprogrammer" };
	std::map<char, uint16_t> m;
	double score {0.0};

	for (auto l : x) m[l] += 1.0;
	for (auto c : m) score += (c.second / (double)x.length()) * log2((c.second / (double)x.length()));
	std::cout << "Shannon entropy: " << -score << "\n";
	}
	*/
}


//-----------------------------------------------------------------------------------------
namespace EULER030
{
	const int	mp = 5;

	const int buf[]
	{ 0, 1,(int)pow(2,mp),(int)pow(3,mp) ,(int)pow(4,mp) ,(int)pow(5,mp),
	(int)pow(6,mp) ,(int)pow(7,mp) ,(int)pow(8,mp) ,(int)pow(9,mp) };

	int rec(int n1, int n2, int level, int mysum) {
		if (level == 0)
			return (n1 == n2 && n1 != 1) ? mysum + n1 : mysum;

		int count = 0;
		for (int i = 0; i < 10; i++)
			count += rec(n1 + (int)pow(10, level - 1)*i, n2 + buf[i], level - 1, mysum);
		return count;
	}

	void Problem() {
		// https://projecteuler.net/problem=30
		std::cout << "sum: " << rec(0, 0, mp + 1, 0) << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER048
{
	void Problem() {
		// https://projecteuler.net/problem=48
		uint64_t s = 0;
		for (int i = 1; i <= 1E3; i++) {
			uint64_t a = 1;
			for (int k = 1; k <= i; k++)
				a = (a*i) % (uint64_t)1E10;
			s = (a + s) % (uint64_t)1E10;
		}
		std::cout << "sum: " << s << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER052
{
	void Problem() {
		// https://projecteuler.net/problem=52
		std::function<int(int)> h =
			[&](int x) {return (x >= 10) ? h(x / 10) + (int)pow(10, (x % 10)) : (int)pow(10, (x % 10)); };
		std::cout << [=]() { for (int i = (uint64_t)1E5; i < INT_MAX; i++)
			if ([=]() {for (int k : {2, 3, 4, 5, 6}) if (h(i) != h(i*k)) return 0; return 1; }())
				return i; return 0; }() << std::endl;
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER087
{
	void Problem() {
		// https://projecteuler.net/problem=87

		std::vector<uint32_t> p(7070);
		for (int i = 2; i < 7072; i++)
			p[i - 2] = i;
		for (size_t i = 0; i < p.size(); i++)
			for (size_t l = i + 1; l < p.size(); l++)
				if (p[l] % p[i] == 0) p.erase(p.begin() + l);


		std::vector<bool> m((unsigned int)5E7, false);
		std::function<void(int, uint64_t)> h = [&](int x, uint64_t s) {
			for (size_t i = 0; i < p.size(); i++) {
				uint64_t s1 = s + (uint64_t)pow(p[i], x);
				if (s1 >= 5E7) return;
				if (x == 4) {
					m[(unsigned int)s1] = true;
					continue;
				}
				h(x + 1, s1);
			}
			return;
		};

		h(2, 0);
		std::cout << [m]() {int c = 0; for (bool e : m) if (e) c++; return c; }() << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER0135
{
	void Problem() {
		// https://projecteuler.net/problem=135

		std::vector<int> count((uint64_t)1E6, 0);
		for (uint32_t q = 1; q < 1E6; q++)
			for (uint32_t c = q / 4 + 1; c < q && q*(4 * c - q) < 1E6; c++)
				count[q*(4 * c - q)]++;

		std::cout << [count]() {int t = 0; for (int e : count)if (e == 10) t++; return t; }() << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER136
{
	void Problem() {
		// https://projecteuler.net/problem=135

		std::vector<int> count((uint64_t)5E7, 0);
		for (uint32_t q = 1; q < 5E7; q++)
			for (uint32_t c = q / 4 + 1; c < q && q*(4 * c - q) < 5E7; c++)
				count[q*(4 * c - q)]++;

		std::cout << [count]() {int t = 0; for (int e : count)if (e == 1) t++; return t; }() << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER018
{
	#define MAX(x,y) ((x>y) ? x : y)

	void Problem() {
		// https://projecteuler.net/problem=18

		std::vector<std::vector<int>> t{
			{ 75 },
			{ 95,64 },
			{ 17,47,82 },
			{ 18,35,87,10 },
			{ 20,04,82,47,65 },
			{ 19,01,23,75,03,34 },
			{ 88,02,77,73,07,63,67 },
			{ 99,65,04,28,06,16,70,92 },
			{ 41,41,26,56,83,40,80,70,33 },
			{ 41,48,72,33,47,32,37,16,94,29 },
			{ 53,71,44,65,25,43,91,52,97,51,14 },
			{ 70,11,33,28,77,73,17,78,39,68,17,57 },
			{ 91,71,52,38,17,14,91,43,58,50,27,29,48 },
			{ 63,66,04,68,89,53,67,30,73,16,69,87,40,31 },
			{ 04,62,98,27,23, 9,70,98,73,93,38,53,60,04,23 }
		};

		for (size_t i = t.size() - 2; i >= 0; i--)
			for (size_t k = 0; k < t[i].size(); k++)
				t[i][k] += MAX(t[i + 1][k], t[i + 1][k + 1]);

		std::cout << t[0][0] << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER067
{
	void Problem() {
		// https://projecteuler.net/problem=67
		std::vector<std::vector<int>> t(1);

		FILE *fp;
		fopen_s(&fp, "c:\\triangle.txt", "r");

		int l = 0;
		int i;
		int count = 0;
		while (fscanf_s(fp, "%d", &i) != EOF) {
			if (count > l) {
				l++;
				t.resize(t.size() + 1);
				count = 0;
			}
			t[l].push_back(i);
			count++;
		}

		for (size_t i = t.size() - 2; i >= 0; i--)
			for (size_t k = 0; k < t[i].size(); k++)
				t[i][k] += MAX(t[i + 1][k], t[i + 1][k + 1]);

		std::cout << t[0][0] << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER099
{
	void Problem() {
		// https://projecteuler.net/problem=99	

		FILE *fp;
		fopen_s(&fp, "c:\\p099.txt", "r");

		int b, e, sol, count = 1;
		std::pair<double, double> s(1, 1);
		while (fscanf_s(fp, "%i,%i\n", &b, &e) != EOF) {
			if (pow(s.first, s.second / e) < b) {
				s = std::pair<double, double>(b, e);
				sol = count;
			}
			count++;
		}

		std::cout << sol << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER041
{
	void Problem() {
		// https://projecteuler.net/problem=41

		const int len = 7;
		int m[len] = { 7,6,5,4,3,2,1 };

		auto nmbr = [&m, &len]() {int64_t nbr = 0; for (int g = 0; g < len; g++)
			nbr += m[g] * (int)pow(10, len - 1 - g); return nbr; };

		auto biggestnodebeyondksmallerthak = [&m, &len](int e) {
			int pos = 10; int val = 0; for (int i = e + 1; i < len; i++)
				if (m[i] > val && m[i] < m[e]) { pos = i; val = m[i]; }
			return pos; };

		auto sortdownfrome = [&m, &len](int e) {
			for (int i = e; i < len; i++)
				for (int l = i + 1; l < len; l++)
					if (m[i] < m[l]) std::swap(m[i], m[l]); };

		auto checkprime = [&m, &len, &nmbr]() {
			if (m[len - 1] % 2 != 0)
				for (int j = 3; j < sqrt(nmbr()); j += 2)
					if (nmbr() % j == 0)
						return 0;
			return 1;
		};

		int k = len - 1;
		int u = len - 1;

		do {
			if (biggestnodebeyondksmallerthak(k) == 10) {
				if (k == 0)
					return;
				k--;
				if (k < u)
					u = k;
			}
			else {
				std::swap(m[k], m[biggestnodebeyondksmallerthak(k)]);
				sortdownfrome(k + 1);
				k = len - 1;

				if (checkprime()) {
					std::cout << "_____" << nmbr() << "\n";
					return;
				}
			}
		} while (true);
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER026
{
	void Problem() {
		// https://projecteuler.net/problem=26

		std::pair<int, int> max(0, 0);

		for (int n = 2; n < 1000; n++) {
			std::vector < std::pair<int, int>> divs;
			int s = 10;
			for (int c = 1; c < 1E10; c++) {
				int r = s % n;
				int k = [&divs](int& e) {for (auto l : divs) if (l.first == e) return l.second; return 0; }(r);
				if (k) {
					if (c - k > max.second)
						max = std::pair<int, int>(n, c - k);
					break;
				}
				divs.push_back(std::pair<int, int>(r, c));
				s = r * 10;
			}
		}
		std::cout << "maxnr: " << max.first << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER046
{
	void Problem() {
		// https://projecteuler.net/problem=46

		std::vector<uint32_t> p(10000, 1);
		for (size_t i = 2; i < p.size(); i++) {
			if (p[i] == 0 && i % 2)
				[&]() {for (int k = 1; k < sqrt(i / 2); k++) if (p[i - 2 * k*k] == 1) return; std::cout << i << "\n"; return; }();
			for (size_t l = i + 1; l < p.size(); l++)
				if (l % i == 0)
					p[l] = 0;
		}
	}
}


//-----------------------------------------------------------------------------------------
namespace STRUKSTAR
{
	const int n = 10;
	int q[n][n];

#define StrukkStarSquareIterate

	void StrukStarSquareIterate(int x, int y, int num, int dir) {
		switch (dir)
		{
		case 0:
			if (x + 1 < n && q[y][x + 1] == -1)
				x++;
			else {
				y++;
				dir++;
			}
			break;
		case 1:
			if (y + 1 < n && q[y + 1][x] == -1)
				y++;
			else {
				x--;
				dir++;
			}
			break;
		case 2:
			if (x > 0 && q[y][x - 1] == -1)
				x--;
			else {
				y--;
				dir++;
			}
			break;
		case 3:
			if (y > 0 && q[y - 1][x] == -1)
				y--;
			else {
				x++;
				dir = 0;
			}
			break;
		}

		if (q[y][x] != -1) return;

		q[y][x] = num;
		num++;
		StrukkStarSquareIterate(x, y, num, dir);
	}

	void StrukkStarSquare() {
		for (int i = 0; i < n; i++)
			for (int l = 0; l < n; l++)
				q[i][l] = -1;
		q[0][0] = 0;
		StrukkStarSquareIterate(0, 0, 1, 0);

		for (int i = 0; i < n; i++) {
			for (int l = 0; l < n; l++) {
				std::cout << q[i][l] << "	";
			}
			std::cout << "\n";
		}
	}

	/*
	1	2	3	4
	12	13	14	5
	11	16	15	6
	10	9	8	7
	*/
}


//-----------------------------------------------------------------------------------------
namespace EULER019
{
	void Problem() {
		// https://projecteuler.net/problem=19

		uint16_t months[] = { 31,28,31,30,31,30,31,31,30,31,30,31 };

		uint32_t year = 1900;
		uint32_t month = 1;
		uint32_t day = 1;
		uint32_t weekday = 1;
		uint16_t count = 0;

		do {
			if (day == 1 && weekday == 7 && year > 1900) count++;

			day++;
			weekday++;

			if (weekday > 7) weekday = 1;

			bool leapyear = (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0);
			months[1] = (leapyear) ? 29 : 28;

			if (day > months[month - 1]) {
				day = 1;
				month++;
				if (month > 12) {
					year++;
					month = 1;
				}
			}
		} while (year != 2001);

		std::cout << count << " Sundays\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER034
{
	void Problem() {
		// https://projecteuler.net/problem=34
		// max number of digits is 6 (anylitically determined)

		#define loop034(x) for (x = 0; x<10; x++)

		uint64_t fac[] = { 0, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880 };
		int x[6];

		loop034(x[0]) loop034(x[1]) loop034(x[2]) loop034(x[3]) loop034(x[4]) loop034(x[5]) {
			if (![&x, &fac]() {int64_t r = 0; for (int a = 0; a < 6; a++) r += fac[x[a]] - x[a] * (int64_t)pow(10, 5 - a) + (x[a] > 0 && x[a + 1] == 0); return r; }())
				std::cout << x[0] << x[1] << x[2] << x[3] << x[4] << x[5] << "\n";
		}
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER036
{
	bool checkBinaryPalindrom32_t(const int x) {
		int k = 32;
		do {
			k--;
			if ((x & (1 << k)) >> k == 1) {
				int c = -1;
				k++;
				do {
					c++;
					k--;
					if ((x & (1 << k)) >> k != (x & (1 << c)) >> c) return false;
				} while (k > c);
				return true;
			}
		} while (k != 0);
		return true;
	}

	int digit(uint32_t x, const int p) {
		return ((x - x % (int)pow(10, p)) / (int)pow(10, p)) % 10;
	}

	bool checkDecimalPalindrom32_t(const uint32_t x) {
		for (int k = 10; k > 0; k--) {
			uint32_t n = x - x % (uint32_t)pow(10, k);
			if (n != 0)
			{
				int c = -1;
				k++;
				do {
					c++;
					k--;
					if (digit(x, k) != digit(x, c))
						return false;
				} while (k > c);
				return true;
			}
		}
		return true;
	}

	void Problem() {
		// https://projecteuler.net/problem=36
		// Double-base palindromes

		uint64_t count = 0;
		for (uint32_t x = 0; x < 10E6; x++) {
			if (checkBinaryPalindrom32_t(x) && checkDecimalPalindrom32_t(x)) {
				for (int i = 0; i < 32; i++)
					std::cout << ((x & (1 << i)) >> i) << "";
				std::cout << "	" << x << "\n";
				count += x;
			}

		}
		std::cout << "\nsum: " << count << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER029
{
	/*
	#include <set>

	void Problem() {
		// https://projecteuler.net/problem=29
		// Distinct Powers


		const uint32_t size = 8;


		std::set<uint64_t> s;
		uint64_t c = 0;
		for (int i = 2; i <= size; i++)
			for (int l = 2; l <= size; l++) {
				auto k = s.insert(pow(i, l));
				if (k.second == 0) {
					c++;
					std::cout << i << " " << l << " " << pow(i, l) << "\n";
				}
			}

		std::cout << pow(size - 1, 2) - c << "\n-----------------------------------------\n\n";


		std::vector<std::tuple<uint32_t, uint32_t>> list;

		// 1. Find all ^2^3^4... below size
		for (int i = 2; i <= sqrt(size); i++)
			for (int l = 2; l <= size; l++)
				if (pow(i, l) <= size) {
					list.push_back(std::tuple<uint32_t, uint32_t>(i, l));
					std::cout << i << " " << l << " " << pow(i, l) << "\n";
				}

		// 2. Search for doubles
		std::cout << "----------------------\n";
		uint32_t dbl_count = 0;
		for (int l = 0; l < list.size(); l++) {
			std::cout << "------    " << std::get<0>(list[l]) << "\n";
			for (int i = 2; i <= size; i++) {

				int power = i * std::get<1>(list[l]);
				int base = std::get<0>(list[l]);

				if (power > size) {
					for (int k = 0; k < list.size(); k++) {
						if (std::get<0>(list[k]) == base) {

						}

					}
				}

				//auto k = (fak % 2 == 0) ? pow(std::get<0>(list[l]), fak / 2) <= size : false;

				if (power <= size && base <= size)
				{
					std::cout << base << "^" << power << " ; ";
					std::cout << std::get<1>(list[l]) << " " << i << "\n";
					dbl_count++;
				}
			}
		}


		std::cout << pow(size - 1, 2) - dbl_count << "\n";
	}
	*/
}


//-----------------------------------------------------------------------------------------
namespace EULER031
{
	uint32_t f(std::vector<uint8_t> &c, uint16_t t, uint16_t p, uint32_t r) {
		if (t == 200) return 1;
		return r + (p < 7) ? [&](auto t) {int r = 0; while (t <= 200) r += f(c, t += c[p], p + 1, 0); return r; }(t - c[p]) : 0;
	}

	void Problem() {
		// https://projecteuler.net/problem=41
		// Coin sums

		std::vector<uint8_t> c{ 100, 50, 20, 10, 5, 2, 1 };
		std::cout << f(c, 0, 0, 0) + 1 << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER102
{
	void Problem() {
		struct tri_type {
			double x[3], y[3];
		};

		std::vector<tri_type> tri;
		std::ifstream myFile;
		myFile.open("c:\\p102_triangles.txt", std::ifstream::in);
		if (!myFile.is_open())
			std::cout << "can't open file\n";
		std::string line;
		while (std::getline(myFile, line, '\n')) {
			std::stringstream iss(line);
			tri_type t;
			std::string buf;
			std::getline(iss, buf, ',');	t.x[0] = (double)stoi(buf);
			std::getline(iss, buf, ',');	t.y[0] = (double)stoi(buf);
			std::getline(iss, buf, ',');	t.x[1] = (double)stoi(buf);
			std::getline(iss, buf, ',');	t.y[1] = (double)stoi(buf);
			std::getline(iss, buf, ',');	t.x[2] = (double)stoi(buf);
			std::getline(iss, buf);			t.y[2] = (double)stoi(buf);

			tri.push_back(t);
		}
		int count = 0;
		for (int i = 0; i < 1000; i++) {
			int r[3];
			for (int t = 0; t < 3; t++) {
				double z1 = (tri[i].y[(t + 1) % 3] - tri[i].y[t]);
				double z2 = (tri[i].x[(t + 1) % 3] - tri[i].x[t]);
				double qx = z1*(-z2*tri[i].y[t] + z1*tri[i].x[t]) / (z2*z2 + z1*z1);
				double qy = -qx * z2 / z1;
				r[t] = int(qx*z1 - qy*z2);
			}
			if ((r[0] > 0 && r[1] > 0 && r[2] > 0) || (r[0] < 0 && r[1] < 0 && r[2] < 0))
				count++;
		}
		std::cout << count << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER103
{
	std::vector<std::vector<int>> indexset;
	int min_val;

	void recursion(std::vector<int> set, const int max, int pos) {
		int summ = 0;
		for (size_t i = 0; i < set.size(); i++)
			summ += set[i];
		if (summ >= min_val) return;

		bool isvalid = true;

		// check(2)
		for (size_t k = 0; k < set.size() / 2 - 1; k++) {
			int minsum = 0;
			int maxsum = 0;
			for (size_t m = 0; m < k + 3; m++)
				minsum += set[m];
			for (size_t m = 0; m < k + 2; m++)
				maxsum += set[set.size() - 1 - m];
			if (minsum <= maxsum) {
				isvalid = false;
				goto done;
			}
		}

		// check(1) S(A)=S(B) only possible when #A=#B (because (1))
		// minsize #=2
		for (size_t i = 0; i < indexset.size(); i++) {
			for (size_t l = i + 1; l < indexset.size(); l++) {
				int s1 = 0;
				int s2 = 0;
				for (size_t m = 0; m < indexset[i].size(); m++)
					s1 += set[indexset[i][m]];
				for (size_t m = 0; m < indexset[l].size(); m++)
					s2 += set[indexset[l][m]];
				if (s1 == s2) {
					isvalid = false;
					goto done;
				}
			}
		}

	done:

		if (isvalid) {
			std::cout << "got one ";
			for (size_t i = 0; i < set.size(); i++)
				std::cout << set[i] << " ";
			std::cout << "			" << summ << "\n";
			min_val = summ;
		}

		if (pos < (int)set.size()) {
			for (int i = 0; i < max - summ; i++) {
				auto newset = set;
				for (size_t l = pos; l < set.size(); l++)
					newset[l] += i;
				recursion(newset, max, pos + 1);
			}
		}
	}

	void genIndexSet(const int p, const int s, std::vector<std::vector<int>>& set) {
		size_t m = set.size();

		for (size_t i = 0; i < m; i++) {
			std::vector<int> newsubset = set[i];
			newsubset.push_back(p);
			if (newsubset.size() < floor(s / 2) + 1)
				set.push_back(newsubset);
		}

		// leere Menge
		std::vector<int> newsubset{ p };
		set.push_back(newsubset);

		if (p + 1 < s)
			genIndexSet(p + 1, s, set);
	}

	void Problem() {
		const int max = 269;
		min_val = max;
		std::vector<int> set = { 17,18,19,20,21,22,22 };
		int n = (int)set.size();

		genIndexSet(0, (int)n, indexset);
		for (auto it2 = indexset.begin(); it2 != indexset.end();)
			it2 = (it2->size() < 2) ? indexset.erase(it2) : it2 + 1;

		recursion(set, max, 0);
	};
}


//-----------------------------------------------------------------------------------------
namespace EULER105
{
	bool CheckSpecialSumSet(std::vector<int> set, std::vector<std::vector<int>>& p_indexsex) {
		for (size_t k = 0; k < set.size() / 2 - 1; k++) {
			int minsum = 0;
			int maxsum = 0;
			for (size_t m = 0; m < k + 3; m++)
				minsum += set[m];
			for (size_t m = 0; m < k + 2; m++)
				maxsum += set[set.size() - 1 - m];
			if (minsum <= maxsum)
				return false;
		}
		for (size_t i = 0; i < p_indexsex.size(); i++) {
			for (size_t l = i + 1; l < p_indexsex.size(); l++) {
				int s1 = 0;
				int s2 = 0;
				for (size_t m = 0; m < p_indexsex[i].size(); m++)
					s1 += set[p_indexsex[i][m]];
				for (size_t m = 0; m < p_indexsex[l].size(); m++)
					s2 += set[p_indexsex[l][m]];
				if (s1 == s2)
					return false;
			}
		}
		return true;
	}

	void sort(std::vector<int>& set) {
		for (size_t i = 0; i < set.size(); i++)
			for (size_t l = 0; l < set.size(); l++)
				if (set[l] > set[i]) std::swap(set[i], set[l]);
	}

	void Problem() {
		std::vector<std::vector<int>> multiindexset[13];

		for (size_t i = 7; i < 13; i++) {
			EULER103::genIndexSet(0, (int)i, multiindexset[i]);
			for (auto it2 = multiindexset[i].begin(); it2 != multiindexset[i].end();)
				it2 = (it2->size() < 2) ? multiindexset[i].erase(it2) : it2 + 1;
			std::cout << "set " << i << " size: " << multiindexset[i].size() << "\n";
		}

		std::fstream myfile("c:\\p105_sets.txt", std::ios_base::in);

		int a;
		std::vector<int>  test;
		char s;

		int total = 0;
		while (myfile >> a) {
			test.push_back(a);
			myfile.get(s);
			if (s != ',') {
				sort(test);
				if (CheckSpecialSumSet(test, multiindexset[test.size()])) {
					for (size_t i = 0; i < test.size(); i++)
						total += test[i];
				}
				test.clear();
			}
		}
		std::cout << total << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER106
{
	int factorial(int n) {
		return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
	}

	void genIndexSet(const int p, const int s, std::vector<std::vector<int>>& set) {
		int m = (int)set.size();

		for (int i = 0; i < m; i++) {
			std::vector<int> newsubset = set[i];
			newsubset.push_back(p);
			if (newsubset.size() < floor(s / 2))
				set.push_back(newsubset);
		}

		// leere Menge
		std::vector<int> newsubset{ p };
		set.push_back(newsubset);

		if (p + 1 < s)
			genIndexSet(p + 1, s, set);
	}

	bool disjunkt(const std::vector<int>& A, const std::vector<int>& B) {
		for (size_t i = 0; i < A.size(); i++)
			for (size_t l = 0; l < B.size(); l++)
				if (A[i] == B[l]) return false;
		return true;
	}

	void Problem() {
		// Special subset sums: meta-testing
		// https://projecteuler.net/problem=106

		const int n = 5;

		// 1) Generate all sorted sets A_i with 2<=#A_i<=floor(n/2) 

		std::vector<std::vector<int>> index_set;
		genIndexSet(0, n, index_set);
		for (auto it2 = index_set.begin(); it2 != index_set.end();)
			it2 = (it2->size() < 2) ? index_set.erase(it2) : it2 + 1;
		for (size_t i = 0; i < index_set.size(); i++)
			EULER105::sort(index_set[i]);

		// 2) Iterate through all possible combinatios of sets A_i, A_j with #A_i == #A_j

		int total_count = 0;

		for (size_t i = 0; i < index_set.size(); i++) {
			for (size_t l = i + 1; l < index_set.size(); l++) {
				if (index_set[i].size() == index_set[l].size()) {
					if (disjunkt(index_set[i], index_set[l])) {
						total_count++;
						int k_i[n];
						for (int r = 0; r < n; r++)
							k_i[r] = 0;
						for (size_t r = 0; r < index_set[i].size(); r++)
							for (int q = 1; q < index_set[i][r] + 1; q++)
								k_i[q] += 1;
						for (size_t r = 0; r < index_set[l].size(); r++)
							for (int q = 1; q < index_set[l][r] + 1; q++)
								k_i[q] -= 1;

						int pos = 0;
						int neg = 0;
						for (int r = 0; r < n; r++) {
							if (k_i[r] >= 0) pos++;
							if (k_i[r] <= 0) neg++;
						}
						if (!(pos == n || neg == n)) total_count++;
					}
				}
			}
		}

		std::cout
			<< "total subsets: " << index_set.size() << " "
			<< "tested subsetparis: " << total_count << " "
			<< "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER166
{
	int64_t total_count = 0;

	struct ts
	{
		int e[16];
		int s;
	};

	void iter166(ts x, int pos, int s)
	{
		for (int i = 0; i < 10; i++)
		{
			if (pos == 1) 			std::cout << i;

			ts n = x;
			n.e[pos] = i;

			if (pos == 3)
			{
				n.s = n.e[0] + n.e[1] + n.e[2] + n.e[3];
			}

			if (pos > 3 && pos < 8)
			{
				int k = pos - 4;
				if (n.e[0 + k] + n.e[4 + k] + 20 < n.s) goto stop;
				if (n.e[0 + k] + n.e[4 + k] > n.s) goto stop;
			}

			if (pos == 7)
			{
				int m = n.e[4] + n.e[5] + n.e[6] + n.e[7];
				if (n.s != m) goto stop;
			}

			if (pos > 7 && pos < 12)
			{
				int k = pos - 8;
				if (n.e[0 + k] + n.e[4 + k] + n.e[8 + k] + 10 < n.s) goto stop;
				if (n.e[0 + k] + n.e[4 + k] + n.e[8 + k] > n.s) goto stop;
			}


			if (pos == 11)
			{
				int m = n.e[8] + n.e[9] + n.e[10] + n.e[11];
				if (n.s != m) goto stop;
			}

			if (pos == 12)
			{
				//inverse diagonal check
				int m = n.e[3] + n.e[6] + n.e[9] + n.e[12];
				if (n.s != m) goto stop;
			}

			if (pos > 11 && pos < 16)
			{
				int k = pos - 12;
				if (n.e[0 + k] + n.e[4 + k] + n.e[8 + k] + n.e[12 + k] != n.s) goto stop;
			}

			if (pos == 15)
			{
				int m1 = n.e[12] + n.e[13] + n.e[14] + n.e[15];
				if (n.s != m1) goto stop;

				int m2 = n.e[0] + n.e[5] + n.e[10] + n.e[15];
				if (n.s != m2) goto stop;

				total_count++;

				/*for (int r = 0; r < 4; r++)
				{
				for (int u = 0; u < 4; u++)
				{
				std::cout << n.e[r * 4 + u] << " ";
				}
				std::cout << "\n";
				}
				std::cout << "   " << n.s << "\n";
				std::cout << "\n";*/
				goto stop;
			}


			if (pos < 16)
				iter166(n, pos + 1, s);

		stop:
			int u = 0;
		}
	}


	void Problem() {
		// Criss Cross
		// https://projecteuler.net/problem=166

		ts t;
		for (int i = 0; i < 16; i++)
			t.e[i] = 0;
		iter166(t, 0, 0);
		std::cout << "\n" << total_count << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER173
{
	void Problem() {
		// Using up to one million tiles how many different "hollow" square laminae can be formed ?
		// https://projecteuler.net/problem=173

		int32_t count = 0;
		int32_t n = (int32_t)1E6;

		for (int32_t c = (n + 4) / 4; c >= 3; c--) {
			int m = c - 2;
			while (m > 0 && c * c - m * m <= n) {
				count++;
				m -= 2;
			}
		}
		std::cout << count << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER074
{
	uint64_t x74[3 * 1000000];

	//-----------------------------------------------------------------------------
	uint32_t chain74(uint32_t z[6]) {
		static uint64_t f[] = { 1,1, 2, 6, 24, 120,720,5040, 40320, 262880 };

		uint64_t n = z[0] + z[1] * 10 + z[2] * 100 + z[3] * 1000 + z[4] * 10000 + z[5] * 100000;

		if (x74[n])
			return 1;
		return 0;
	}

	void rec74(uint32_t p, uint32_t z[6]) {
		for (int i = 0; i < 10; i++) {
			uint32_t nz[6];
			for (int k = 0; k < 6; k++)
				nz[k] = z[k];
			nz[p] = i;
			if (p < 5)
				rec74(p + 1, nz);
			else
				chain74(nz);
		}
	}

	void Problem() {
		// Digit factorial chains
		// https://projecteuler.net/problem=74

		uint32_t z[6];
		rec74(0, z);
	}
}



//-----------------------------------------------------------------------------------------
namespace EULER055
{

	//----------------------------------------------------------------------------
	// Big Int Class

	class BigInt {
		static const int32_t max_length = 19;
		int8_t x[max_length];

	public:
		BigInt() {
			for (int i = 0; i < max_length; i++) x[i] = 0;
		}
		BigInt(int64_t _x) {
			for (int i = 0; i < max_length; i++) x[i] = 0;
			*this = _x;
		}

		~BigInt() {}

		BigInt& operator=(int64_t _x) {
			int i = 0;
			do {
				x[i] = _x % 10;
				_x = _x - _x % 10;
				_x = _x / 10;
				i++;
			} while (_x != 0);
			return *this;
		}

		BigInt& operator+(BigInt& _x) {
			int over = 0;
			for (int i = 0; i < max_length; i++) {
				x[i] = x[i] + over;
				int under = (x[i] + _x.x[i]) % 10;
				over = (x[i] + _x.x[i] - under) / 10;
				x[i] = under;
			}
			return *this;
		}

		BigInt& operator*(BigInt& _x) {
			BigInt r;
			int over = 0;
			for (int i = 0; i < max_length; i++) {
				for (int l = 0; l < max_length; l++) {
					int under = (x[i] * _x.x[i]);
					under = under + over;
					under = under % 10;
					over = (x[i] * _x.x[i] - under) / 10;
					x[i] = under;
				}
			}
			*this = r;
			return *this;
		}

		bool operator==(BigInt& _x) {
			for (int i = 0; i < max_length; i++) {
				if (_x.x[i] != this->x[i]) return false;
			}
			return true;
		}

		void reverse() {
			for (int i = max_length - 1; i > 0; i--) {
				if (x[i] != 0) {
					for (int l = i; l > i / 2; l--)
						std::swap(x[l], x[i - l]);
					return;
				}
			}
		}

		BigInt DigitSum() {
			BigInt r;
			for (int i = 0; i < max_length; i++)
				r = r + (BigInt)x[i];
			return r;
		}

		void print() {
			for (int i = max_length - 1; i > 0; i--) {
				if (x[i] != 0) {
					for (int l = i; l >= 0; l--)
						std::cout << (int)x[l];
					return;
				}
			}
		}

	};

	void Problem() {
		// Lychrel numbers
		// https://projecteuler.net/problem=55

		int counter = 0;
		int max_ = 10000;

		for (int i = 1; i < max_; i++) {
			BigInt x = i;
			for (int k = 0; k < 50; k++) {
				BigInt revx = x;
				revx.reverse();
				x = x + revx;

				BigInt testrev = x;
				testrev.reverse();
				if (x == testrev) {

					std::cout << i << " after " << k << " iterations : ";
					x.print();
					std::cout << "\n";

					goto ok;
				}
			}
			counter++;
		ok:
			int zu = 0;
		}
		std::cout << "Lychrel Numbers below " << max_ << " : " << counter << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER056
{
	void Problem() {
		EULER055::BigInt x = 19;
		EULER055::BigInt y = 22;
		(x*y).print();
		std::cout << "\n";
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER150
{
	void Problem() {
		// Searching a triangular array for a sub-triangle having minimum-sum
		// https://projecteuler.net/problem=150

		int32_t x[1000][1000];

		int64_t row = 0;
		int64_t maxc = 0;
		int64_t c = 0;
		int64_t t = 0;
		for (int k = 1; k <= 500500; k++) {
			t = (615949 * t + 797807) % 1048576;
			x[row][c] = int32_t(t - 524288);

			c++;
			if (c > maxc) {
				row++;
				maxc++;
				c = 0;
			}
		}

		/*
		int32_t buffer[1000] = {0};
		for (int32_t z = 1000 - 1; z >= 0; z--) {
		//std::cout << z << "\n";
		for (int32_t s = 0; s < 1000 - z; s++)
		{
		buffer[z] += x[1000 - s - 1][1000 - z - s - 1];
		//std::cout << 1000 - s  -1 << " " << 1000 - z - s - 1 << "\n";
		}
		}
		*/

		int32_t total_min = INT32_MAX;

		/*

		int32_t buffer2[1000];
		for (int u=0;u<1000;u++)
		buffer2[u] = buffer[u];

		for (int32_t z = 1000 - 1; z > 0; z--) {
		for (int32_t s = 0; s < z; s++) {
		int32_t total = 0;
		int32_t total2 = 0;
		for (int32_t triSize = 1; triSize < z - s + 2; triSize++) {

		for (int z2 = 0; z2 < triSize; z2++) {
		total += x[z - z2][s + triSize - z2 - 1];
		if (total < total_min) total_min = total;
		//std::cout << z - z2 << ":" << s + triSize - z2 - 1  << " val: " << x[z - z2][s + triSize - z2 - 1] << "\n";
		}

		total2 += buffer2[1000 - triSize-s-(999-z)];
		if (total2 < total_min) total_min = total2;
		//buffer2[triSize - 1] -= x[z - triSize- 1][s + triSize];
		buffer2[1000 - triSize - s] -= x[z - triSize + 1][s];

		//std::cout << z - triSize + 1 << " " << s << "\n";

		//std::cout << total << " " << total2 << "\n";

		if (total != total2)
		{
		std::cout << "X\n";
		}
		}
		}

		for (int u = z; u>=0; u--)
		buffer[u] -= x[z][999-u];

		for (int u = 0; u<1000; u++)
		buffer2[u] = buffer[u];
		}
		*/

		for (int32_t z = 1000 - 1; z > 0; z--)
			for (int32_t s = 0; s < z; s++) {
				int32_t total = 0;
				for (int32_t triSize = 1; triSize < z - s + 2; triSize++) {
					for (int z2 = 0; z2 < triSize; z2++)
						total += x[z - z2][s + triSize - z2 - 1];
					if (total < total_min) total_min = total;
				}
			}

		std::cout << " min: " << total_min << "\n";

		/*
		for (int64_t triSize = 2; triSize < 1000; triSize++) {
		for (int z = 0; z < 1000 - triSize; z++) {
		for (int s = 0; s < z+1; s++) {
		//calculate triangle
		int64_t total = 0;

		for (int z2 = 0; z2 < triSize; z2++)
		for (int s2 = 0; s2 < z2+1; s2++)
		total += x[z+z2][s+s2];
		//std::cout << z + z2 << ":" << s + s2 << " val: " << x[z + z2][s + s2] << "\n";
		//std::cout << total << "<< total \n";
		if (total < total_min) total_min = total;
		}
		}
		std::cout << "triSize: " << triSize << "   max: " << total_min << "\n";
		}
		*/
	}
}


//-----------------------------------------------------------------------------------------
namespace EULER152
{
	bool Iterator(int index, double val) {
		if (val > 0.5f) return 0;
		static const double error = 0.00000000001f;
		if (abs(0.5f - val) < error) {
			std::cout << val << " hit\n";
			return 1;
		}

		if (index < 46) {
			if (Iterator(index + 1, val + 1.0f / (index*index))) {
				std::cout << index << " ";
				return 1;
			}
			if (Iterator(index + 1, val)) return 1;
		}

		return 0;
	}

	void Problem() {
		// Writing 1/2 as a sum of inverse squares

		Iterator(2, 0);

		std::cout << "fin\n";

	}
}


//-----------------------------------------------------------------------------
namespace EULER346 {
	void  Problem() {
		// https://projecteuler.net/problem=346
		
		std::cout << "-------------------------------------------------------------------------------\n";
		std::cout << "Problem 346, Strong Repunits\n\n" << 
			"The number 7 is special, because 7 is 111 written in base 2, and 11 written in base 6 " <<
			"(i.e. 710 = 116 = 1112).In other words, 7 is a repunit in at least two bases b > 1.\n" <<
			"We shall call a positive integer with this property a strong repunit. "<<
			"Find the sum of all strong repunits below 10^12.\n\n";
		
		// The strong repunits must be of the form x = sum(b^2,b=0..n) where the bases are  x-1 and b
		// according to the Goormaghtigh conjecture 31 and 8191 are the only doubles, therefor no set to 
		// keep track of doubles is needet

		const uint64_t limit = (uint64_t)1E12;
		uint64_t result = 1;		

		for (uint64_t b = 2; b < std::sqrt(4*limit-3)/2; b++) {
			uint64_t c(b*b), s(1 + b + c);			
			
			while (s < limit) {
				result += s;
				c *= b;
				s += c;
			}		
		}		

		// execution time without cout 1.36ms
		std::cout << "sum of the strong repunits below " << limit << " is " << result- 31 -8191 << "\n";
	}
}



//-----------------------------------------------------------------------------
namespace EULER348 {

	class PanlindromCounter 
	{
		static int64_t length;
		static int64_t count;
		static int64_t pal;
		static int64_t pos;

	public:
		static int64_t tick()
		{
			count++;
			if (count > 10)
			{
				if (length == pos)
				{
					pos = 0;
					pal = 0;
					length++;
					for (int i = 0; i < length+1; i++)
						pal += 1 * pow(10, i);

					std::cout << pal << "--\n";
				}
				else  
					pos++;
				count = 0;				
				
				
				//std::cout << pal << " <<- \n";
			}

			auto middle = (length + 1) / 2;
			auto p1 = middle - pos;
			auto p2 = middle + pos - (length % 2);

			pal += 1 * pow(10, p1);
			if (!((pos == 0) && !(length % 2)))
				pal	+= 1 * pow(10, p2);

			return pal;
		}
	};
	
	int64_t PanlindromCounter::length = 1;
	int64_t PanlindromCounter::count = 1;
	int64_t PanlindromCounter::pal = 0;
	int64_t PanlindromCounter::pos = 1;

	void  Problem() {
		// https://projecteuler.net/problem=348

		std::cout << "-------------------------------------------------------------------------------\n";
		std::cout << "Problem 348, Sum of a square and a cube\n\n" <<
			"Consider the palindromic numbers that can be expressed as the sum of a square and a cube, both greater than 1, in exactly 4 different ways.\n" <<
			"Find the sum of the five smallest such palindromic numbers.\n\n";

		// Iterate from through palindromic numbers upwards
		// got through sums of square and cube numbers till the number is reached

		PanlindromCounter p;
		uint64_t c;


		do {
			c = p.tick();
			std::cout << c << "\n";

		} while (c < 1600);

	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	auto start = std::chrono::system_clock::now();

	EULER348::Problem();

	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end - start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "execution time " << elapsed_seconds.count() << "\n";


	return 0;
}
