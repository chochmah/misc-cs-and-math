#include <stdio.h>
#include <iostream>

#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\Large Integer Class\LargeIntegerClass.h"


//-----------------------------------------------------------------------------
void euler056()
{
	intLarge<uint64_t> dsum;

	for (int i = 1; i < 100; i++)
		for (int l = 1; l < 100; l++)
		{
			auto z = intLarge<uint64_t>::digitSum(intLarge<uint64_t>::pow(i, l));
			if (z > dsum)
				dsum = z;
		}

	std::cout << "max sum: " << dsum << "\n";
}


int main()
{
	euler056();
	getchar();
}