#include "pch.h"
#include <iostream>

#include "LargeIntegerClass.h"

void calc(size_t max, std::vector<int> a) {
	lint z;
	z = 1;
	lint n;
	n = a.back();

	for (size_t i = 0; i < max-1; i++) {
		lint mul = (lint)a[max - i - 2] * n;		
		z += mul;
		std::swap(n, z);
	}
	std::swap(n, z);

	z.print();	
	n.print();
}

int main()
{
	//sqrt(2)
	{
		size_t n = 10;
		std::vector<int> input(n, 2);
		input[0] = 1;
		calc(n, input);
	}
	std::cout << "\n";

	// e
	{
		size_t n = 100;
		std::vector<int> input(n, 1);
		for (size_t i = 1; i < n / 3+1; i++)
				input[3 * i - 1] = 2 * i;			
		input[0] = 2;

		for (int i=0;i<input.size();i++)
			std::cout << input[i] << " ";
			
		std::cout << "\n";
		calc(n, input);
	}

}