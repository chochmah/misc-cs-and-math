#include <iostream>
#include <vector>

std::vector<long long int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<long long int>& list, const long long int upperBound) {
	natNumbers.resize(upperBound);
	for (long long int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (long long int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true) {
			long long int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (long long int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}


int main()
{
	long long int maximum = 1E8;

	CreatePrimeList(primes, maximum);	

	long long int count = 0;
	long long int p = 0;
	while (primes[p] < maximum/2) {
		long long int p2 = p;
		while (primes[p] * primes[p2] < maximum) {
			count++;
			p2++;
		};

		p++;
	};

	std::cout << count << "\n";
	std::getchar();
}