#include <iostream>
#include <vector>
#include <functional>
#include <sstream>

std::vector<std::vector<int>> nrs;
std::vector<std::function<int(int)>> funcs;

bool checkConnected(int x, int y) {
	std::stringstream ss1, ss2;
	ss1 << x;
	std::string lastNr = ss1.str();
	ss2 << y;
	std::string nextNr = ss2.str();
	return (lastNr[2] == nextNr[0] && lastNr[3] == nextNr[1]);
}

void recursion(std::vector<int> n, int last) {	
	int count = 0;
	for (int i = 4; i < 9; i++) {
		if (n[i] == 0)
			for (int k = 0; k < nrs[i].size(); k++) {
				if (checkConnected(n[last],nrs[i][k])) {
					auto n2 = n;
					n2[i] = nrs[i][k];
					recursion(n2, i);
				}
			}
		else
			count++;
	}
	if (count == 5) 
		if (checkConnected(n[last], n[3])) {
			int sum = 0;
			for (int i = 3; i < 9; i++) {
				sum += n[i];
				std::cout << n[i] << " ";
			}
			std::cout << " sum: " << sum << "\n";
		}		
}

int main() {	
	funcs.resize(9);	
	funcs[3] = ([](int n) { return n * (n + 1) / 2; });
	funcs[4] = ([](int n) { return n * n; });
	funcs[5] = ([](int n) { return n * (3 * n - 1) / 2; });
	funcs[6] = ([](int n) { return n * (2 * n - 1); });
	funcs[7] = ([](int n) { return n * (5 * n - 3) / 2; });
	funcs[8] = ([](int n) { return n * (3 * n - 2); });	
	
	nrs.resize(9);
	for (int i=3;i<9;i++)
		for (int n=0; funcs[i](n) < 10000;n++)
			if (funcs[i](n) >= 1000)
				nrs[i].push_back(funcs[i](n));

	for (int i=0;i<nrs[3].size();i++)
		recursion(std::vector<int>{0,0,0,nrs[3][i],0,0,0,0,0}, 3);

	getchar();
}