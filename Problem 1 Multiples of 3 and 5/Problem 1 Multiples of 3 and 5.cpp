#include <iostream>
#include <stdio.h>
#include <vector>

int main()
{
	std::cout <<
		"https://projecteuler.net/problem=1\n" <<
		"If we list all the natural numbers below 10 that are multiples\nof 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23\n" <<
		"Find the sum of all the multiples of 3 or 5 below 1000.\n\n";

	typedef unsigned int INT;
	const INT n = 1000;

	// 1. Find all multiples of 3 and 5 below a threshold n
	std::vector<INT> hits;
	for (int i = 0; i < n; i++)
		if (i % 3 == 0 || i % 5 == 0) hits.push_back(i);

	// 2. Sum all hits up
	INT sum = 0;
	for (auto h : hits)
	sum += h;

	std::cout << "The sum of all multiples of 5 and 3 up to " << n << " is: " << sum << "\n\n";
	getchar();
}
