// Problem 91 Right triangles.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

const unsigned int n = 50;

bool check2VOrthogonal(int x, int y, int a, int b) {
	return (x*a + y * b == 0);
}

int main() {
	unsigned int triangleCount = 0;
	for (int i = 1; i <= n; i++) 
		for (int l = 0; l <= n; l++) 
			for (int k = 0; k <= i; k++) 
				for (int m = l + (l == 0); m <= n; m++) 
					if (!(i == k && l == m))
						if (check2VOrthogonal(i, l, k, m) || check2VOrthogonal(i, l, i - k, l - m) || check2VOrthogonal(k, m, i - k, l - m)) 
							triangleCount++;
	std::cout << triangleCount << "\n"; 
}
