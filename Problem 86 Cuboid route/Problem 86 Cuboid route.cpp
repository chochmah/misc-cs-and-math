// Problem 86 Cuboid route.cpp
// 3 different approaches
//

#include "pch.h"
#include <iostream>
#include <algorithm>    // std::min


bool sqrtTable[5*2000*2000];

int solutionsPrimitive(int M) {
	int counter = 0;
	for (int i1 = 1; i1 <= M; i1++) {
		for (int i2 = i1; i2 <= M; i2++) {
			for (int i3 = i2; i3 <= M; i3++) {
				int S1 = i1*i1 + (i2 + i3)*(i2 + i3);
				int S2 = i3*i3 + (i2 + i1)*(i2 + i1);
				int S3 = i2*i2 + (i1 + i3)*(i1 + i3);
				int minimum = std::min(std::min(S1, S2), S3);
				if (sqrtTable[minimum]) {				
					counter++;
				}
			}
		}
	}
	return counter;
}

int solutionsPrimitiveConsecutive(int M) {
	int counter = 0;
	for (int i1 = 1; i1 <= M; i1++) {
		for (int i2 = i1; i2 <= M; i2++) {
			int i3 = M;
			int S1 = i1 * i1 + (i2 + i3)*(i2 + i3);
			int S2 = i3 * i3 + (i2 + i1)*(i2 + i1);
			int S3 = i2 * i2 + (i1 + i3)*(i1 + i3);
			int minimum = std::min(std::min(S1, S2), S3);
			if (sqrtTable[minimum]) {
				counter++;
			}
		}
	}
	return counter;
}


bool checkmin1(int a, int b, int c) {
	int s1 = (a + b)*(a + b) + c * c;
	int s2 = (a + c)*(a + c) + b * b;
	int s3 = (b + c)*(b + c) + a * a;
	if (s1 <= std::min(s2, s3))
		return 1;
	return 0;
}

bool checkmin2(int a, int b, int c) {
	int s1 = (a + b)*(a + b) + c * c;
	int s2 = (a + c)*(a + c) + b * b;
	int s3 = (b + c)*(b + c) + a * a;
	if (s3 <= std::min(s2, s1))
		return 1;
	return 0;
}

int solutionsRefined(int M) {
	int x = 0;
	int y = 0;
	int z = 0;

	int k = 1;
	do {
		int n = 1;
		do {
			int m = 1;
			do {

				x = k * (m*m - n * n);
				y = k * (2 * m*n);
				z = k * (m*m + n * n);

				if ((x < 2 * M && y < M)) {
					std::cout << x << " " << y << " " << z << "\n";
					for (int i = 1; i <= std::ceil((double)x / 2); i++)
					{
						int a = i;
						int b = x - i;
						int c = y;
						if (checkmin1(a, b, c))
							std::cout << "	" << i << " " << x - i << " " << y << " " << z << "\n";
					}
				}

				if ((x < M && y < 2 * M)) {
					std::cout << x << " " << y << " " << z << "\n";
					for (int i = 1; i <= std::ceil((double)y / 2); i++)
					{
						int a = x;
						int b = y;
						int c = y - i;
						if (checkmin2(a, b, c))
							std::cout << "	" << x << " " << y << " " << y - i << " " << z << "\n";
					}
				}


				m++;
			} while (x < 2 * M && y < 2 * M);
			n++;
		} while (x < 2 * M && y < 2 * M);
		k++;
	} while (x < 2 * M && y < 2 * M);

	return 0;
}

int main()
{
	for (int i = 0; i < 5 * 2000 * 2000; i++) {
		sqrtTable[i] = 0;
	}

	int iq = 0;
	do {
		sqrtTable[iq*iq] = 1;
		iq++;
	} while (iq*iq < 5 * 2000 * 2000);

	//std::cout << solutionsPrimitive(1818) << "\n";
	
	int total = 0;
	int i = 0;
	do 
	{
		i++;
		total += solutionsPrimitiveConsecutive(i);
		//std::cout << i << "	" << total << "\n";
	} while (total<1000000);
	std::cout << i << "\n";
	
	/*
	std::cout << solutionsPrimitive(10) << "\n";
	std::cout << solutionsRefined(10) << "\n";
	return 0;

	int lowp = 1800;
	int highp = 2000;
	
	do {
		int midp = (lowp + highp) / 2;
		int mid = solutionsPrimitive(midp);
		if (mid < 1000000)
			lowp = midp;
		else highp = midp;

		std::cout << midp << "	" << mid << "\n";
	} while (lowp!=highp);
	*/
}
