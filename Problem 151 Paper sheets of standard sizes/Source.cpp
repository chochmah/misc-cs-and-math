#include <stdio.h>
#include <iostream>
#include <vector>
#include <time.h>  

/*
	could be speed up massively by not using vector implementation but I'm to lazy
*/

double totalprob = 0;

void recursion(std::vector<uint16_t> e, double cprob, int depth) {
	if (depth >= 15) return;
	if (e.size() == 1)
		totalprob += cprob;	
	double prop = 1.0 / e.size();
	for (int i = 0; i < e.size(); i++) {
		uint16_t sheet = e[i];
		auto e2 = e;
		e2.erase(e2.begin() + i);
		if (sheet == 4)
			e2.push_back(5);
		if (sheet == 3) {
			e2.push_back(4);
			e2.push_back(5);
		}
		if (sheet == 2) {
			e2.push_back(3);
			e2.push_back(4);
			e2.push_back(5);
		}
		recursion(e2, cprob * prop, depth+1);
	}	
}


int main() {
	srand(time(NULL));
	std::cout.precision(6);
	std::vector<uint16_t> e{ 2,3,4,5 };	
	recursion(e, 1.0, 1);
	std::cout << totalprob << "\n";
	getchar();
	
	// Monte Carlo Method, wont find a solution in acceptable time
	/*
	double pickedA5Counter = 0;
	double weekCounter = 0;

	for (int k = 0; k < 1E6; k++)
	{
		std::vector<uint16_t> e{ 2,3,4,5 }; // envelop

		for (int i = 0; i < 14; i++) {
			// pick random sheet
			if (e.size() == 1)
				pickedA5Counter++;
			uint16_t pick = rand() % e.size();
			uint16_t sheet = e[pick];
			e.erase(e.begin() + pick);
			if (sheet == 4) {
				e.push_back(5);
			}
			if (sheet == 3) {
				e.push_back(4);
				e.push_back(5);
			}
			if (sheet == 2) {
				e.push_back(3);
				e.push_back(4);
				e.push_back(5);
			}
		}
		weekCounter++;		
	}	
	
	std::cout << weekCounter << " " << pickedA5Counter << " " << pickedA5Counter / weekCounter << "\n";
	*/
}