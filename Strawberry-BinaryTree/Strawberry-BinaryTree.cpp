// Strawberry-BinaryTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "CArvoreBinaria.h"

// test for operator overloading

class testc {
public:
	int x;

	bool operator<(testc& n) {
		std::cout << "base called\n";
		return (x < n.x);
	}
};


class testparent : public testc {
public:
	bool operator<(testparent& n) {
		std::cout << "child called\n";
		return false;
	}
};


int main() {

	// test for operator overloading
	testparent r1, r2;
	r1.x = 2;
	r2.x = 4;

	if (r1 < r2)
		std::cout << "child operator\n";

	if (r1.testc::operator<(r2))
		std::cout << "base operator\n";


	CArvoreBinaria tree;
	for (int i=0;i<130;i++)
		tree.insere(rand()%1000);
	tree.printTree();

	tree.SearchMinMax();

	std::cout << "Smallest non leaf element: " << tree.getMyExceptionFeed() << "\n";

	int r = tree.profundidade(547);
	if (r != -1)
		std::cout << "element at depth: " << r << "\n";

	std::vector<CNoArvore*> path;

	if (tree.pathDesDeRaiz(547, &path))
	{
		std::cout << "path: ";
		for (int i = 0; i < path.size(); i++)
			std::cout << path[i]->getVal() << " ";
		std::cout << "\n";
	}

    return 0;
}

