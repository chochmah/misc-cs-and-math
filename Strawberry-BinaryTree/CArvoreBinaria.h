//=============================================================================
// EDA 2014/15 ficha 9 (arvores binarias de pesquisa)
// Rui P. Rocha (2017.04.24) e Paulo Coimbra (2010.05.20, 2008.06.01)
//=============================================================================
// header file: CArvoreBinaria.h
//=============================================================================

#ifndef CArvoreBinaria_h
#define CArvoreBinaria_h

//=============================================================================
#include <iostream>
#include <vector>

using namespace std;


class CArvoreBinaria; // declarada aqui para poder ser designada como amiga

//=============================================================================
// classe CNoArvore - definicao - como na ficha 9
//=============================================================================
class CNoArvore {
private:
    int dados;
    CNoArvore *esq;     // ponteiro para subarvore esquerda
    CNoArvore *dir;     // ponteiro para subarvore dreita
    friend class CArvoreBinaria; // esta classe pode aceder a atributos private
public:
	int getVal() { return dados; }
};

//=============================================================================
// classe CArvoreBinaria - definicao - como na ficha 9
//=============================================================================
class CArvoreBinaria {
    CNoArvore *raiz;    // ponteiro para a raiz da arvore
    bool procuraRecursiva(CNoArvore*, int) const;
    void escrevePreOrdem(CNoArvore*) const;
    void insere(int, CNoArvore*);
    void destroiSubArvore(CNoArvore*);
public:
    CArvoreBinaria();
    ~CArvoreBinaria();
    bool procura(int) const;	
    void escrevePreOrdem() const;
    void insere(int);
    bool elimina(int);
    void destroi();
	
	void printTree() const;
	void SearchMinMax() const;
	int getMyExceptionFeed() const;
	int profundidade(int) const;
	bool pathDesDeRaiz(int item, std::vector<CNoArvore*> *cam) ;

private:
	void SearchMinMax(int*, int*, CNoArvore*) const;
	int getMyExceptionFeed(CNoArvore *root) const;
	void printTree(CNoArvore*) const;
	int  profundidade(CNoArvore *raiz, int item) const;

};

//=============================================================================
#endif
