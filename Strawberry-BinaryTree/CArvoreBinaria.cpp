#include "stdafx.h"

//=============================================================================
// EDA 2014/15 ficha 9 (arvores binarias de pesquisa)
// Rui P. Rocha (2017.04.24) e Paulo Coimbra (2010.05.20, 2008.06.01)
//=============================================================================
// source file: CArvoreBinaria.cpp
//=============================================================================

#include "CArvoreBinaria.h"

//=============================================================================
// classe CArvoreBinaria - codigo dos metodos como na folha 9 (e slides teoricas)
//=============================================================================

CArvoreBinaria::CArvoreBinaria() {  // Construtor
    raiz = NULL;
}
//=============================================================================

CArvoreBinaria::~CArvoreBinaria() {  // Destrutor
    destroi();    // metodo que elimina todos os nos
}
//=============================================================================

// metodo public para destruir todos os nos da arvore
void CArvoreBinaria::destroi(){
    destroiSubArvore(raiz); // chama o metodo private (recursivo)
    raiz = NULL;
}
//=============================================================================

// metodo private para destruir uma subarvore
void CArvoreBinaria::destroiSubArvore(CNoArvore *pArvore) {
    if (pArvore != NULL) {
        destroiSubArvore(pArvore->esq);
        destroiSubArvore(pArvore->dir);
        delete pArvore;
    }
}
//=============================================================================

// metodo public para pesquisar recursivamente toda a arvore
bool CArvoreBinaria::procura(int item) const {
    return procuraRecursiva(raiz, item);
}
//=============================================================================

// metodo private para pesquisar recursivamente numa subarvore
bool CArvoreBinaria::procuraRecursiva(CNoArvore *raiz, int item) const {
    // casos elementares
    if (raiz == NULL) return false;
    if (raiz->dados == item) return true;

    // caso geral
    if (raiz->dados < item)
        return procuraRecursiva(raiz->dir, item);
    else return procuraRecursiva(raiz->esq, item);
}
//=============================================================================

// metodo public para a travessia da arvore pre-ordem
void CArvoreBinaria::escrevePreOrdem() const {
        escrevePreOrdem(raiz);
        cout << endl;
}
//=============================================================================

// metodo private para a travessia recursiva da subarvore pre-ordem
void CArvoreBinaria::escrevePreOrdem(CNoArvore* raiz) const {
    if (raiz == NULL) return;
    cout << raiz->dados << endl;
    escrevePreOrdem(raiz->esq);
    escrevePreOrdem(raiz->dir);
}
//=============================================================================

// metodo public para fazer a insercao ordenada de um novo valor
void CArvoreBinaria::insere(int item) {
    if (raiz != NULL) insere(item, raiz); // chama o metodo private que faz o trabalho
    else {  // cria o primeiro no da arvore
        raiz = new CNoArvore;
        raiz->dados = item;
        raiz->esq = raiz->dir = NULL;
    }
}
//=============================================================================

// metodo private para fazer a insercao ordenada de um novo valor numa subarvore
void CArvoreBinaria::insere(int item, CNoArvore *raiz) {
    if (item < raiz->dados) {
        if (raiz->esq != NULL) insere(item, raiz->esq);
        else { // novo no e o primeiro no da subarvore esquerda
            raiz->esq = new CNoArvore;
            raiz->esq->dados = item;
            raiz->esq->esq = raiz->esq->dir = NULL;
        }
    }
    else if (item > raiz->dados) {
        if (raiz->dir != NULL) insere(item, raiz->dir);
        else { // novo no e o primeiro no da subarvore direita
            raiz->dir = new CNoArvore;
            raiz->dir->dados = item;
            raiz->dir->esq = raiz->dir->dir = NULL;
        }
    }
}
//=============================================================================

void CArvoreBinaria::printTree(CNoArvore *root) const {
	CNoArvore *left = root->esq;
	CNoArvore *right = root->dir;

	if (left) printTree(left);
	std::cout << root->dados;
	std::cout << " ";
	if (right) printTree(right);
}
//=============================================================================

void CArvoreBinaria::printTree() const {
	printTree(raiz);
	std::cout << "\n";
}
//=============================================================================


bool CArvoreBinaria::elimina(int item) {
    CNoArvore *pai, *aux, *filho, *sucessor;
    bool encontrado=false;

    // procura o no a eliminar
    pai = NULL;
    aux = raiz;
    while ( (aux != NULL) && (!encontrado) ) {
        if (aux->dados == item) encontrado = true;
        else if (item < aux->dados) {
            pai = aux;
            aux = aux->esq;
        }
        else {
            pai = aux;
            aux = aux->dir;
        }
    }
    // se for encontrado ?aux? aponta para o no
    // a eliminar e ?pai? aponta para o seu pai (se existir)

    if (encontrado) {
        // se no tem no maximo um filho
        if ( (aux->esq == NULL) || (aux->dir == NULL) ) {
            if (aux->esq != NULL)
                filho = aux->esq;
            else filho = aux->dir;
            if (pai == NULL) // caso especial: apaga raiz
                raiz = filho;
            else {           // atualiza ligacoes
                if (aux->dados < pai->dados) pai->esq = filho;
                else pai->dir = filho;
            }
            delete aux;
        }
        else {  // eliminar no com dois filhos
            // procura sucessor
            sucessor = aux->dir;
            pai = aux;
            while (sucessor->esq != NULL) {
                pai = sucessor;
                sucessor = sucessor->esq;
            }
            aux->dados = sucessor->dados; // troca val.
            if (sucessor == aux->dir) aux->dir = sucessor->dir;
            else // substitui pelo seu sucessor `a dir.
                pai->esq = sucessor->dir;
            delete sucessor;
        }
        return true;
    }
    return false;
}
//=============================================================================

int CArvoreBinaria::getMyExceptionFeed(CNoArvore *root) const {
	CNoArvore *left		= root->esq;
	CNoArvore *right	= root->dir;

	if (left)
	{
		int ret = getMyExceptionFeed(left);
		return  (ret == INT_MAX) ? root->dados : ret;
	}
	if (right)
	{
		int ret = getMyExceptionFeed(right);
		return  (ret == INT_MAX) ? root->dados : ret;
	}
	// we are at a leaf
	return INT_MAX;
}

int CArvoreBinaria::getMyExceptionFeed() const {
	if (!raiz || !raiz->esq)
		return INT_MAX;
	return getMyExceptionFeed(raiz);	
}
//=============================================================================

void CArvoreBinaria::SearchMinMax() const {
	int min = raiz->dados;
	int max = raiz->dados;
	SearchMinMax(&min, &max, raiz);
	std::cout << "min is: " << min << " max is: " << max << "\n";
}
//=============================================================================

void CArvoreBinaria::SearchMinMax(int *min, int *max, CNoArvore *root) const {
	CNoArvore *left = root->esq;
	CNoArvore *right = root->dir;

	if (left) SearchMinMax(min, max, left);
	if (root->dados < *min) *min = root->dados;
	if (root->dados > *max) *max = root->dados;
	if (right) SearchMinMax(min, max, right);	
}
//=============================================================================


int CArvoreBinaria::profundidade(int item) const {
	return profundidade(raiz, item);
}
//=============================================================================

int CArvoreBinaria::profundidade(CNoArvore *raiz, int item) const {
	if (raiz->dir != NULL)
	{
		int d = profundidade(raiz->dir, item);
		if (d != -1) 
			return d + 1;		
	}

	if (raiz->esq != NULL)
	{
		int r = profundidade(raiz->esq, item);
		if (r != -1)
			return r + 1;		
	}

	if (raiz->dados == item) return 0;	
	return -1;
}
//=============================================================================

bool CArvoreBinaria::pathDesDeRaiz(int item, std::vector<CNoArvore*> *cam)  {
	if (cam->empty())
		cam->push_back(raiz);

	if (cam->back()->dados == item)
		return 1;

	if (cam->back()->dir != NULL)
	{					
		cam->push_back((cam->back()->dir));
		if (pathDesDeRaiz(item, cam))
			return 1;
		cam->pop_back();
	}

	if (cam->back()->esq != NULL)
	{
		cam->push_back((cam->back()->esq));
		if (pathDesDeRaiz(item, cam))
			return 1;
		cam->pop_back();
	}	

	return 0;
}

//=============================================================================



int  T( int n)
{
	if (n <= 0) return 0;
	if (n == 1) return 1;
	return (T(n - 1) + T(n - 2)) / 2;
}