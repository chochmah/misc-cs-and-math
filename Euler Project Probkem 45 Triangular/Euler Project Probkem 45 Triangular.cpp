// Euler Project Probkem 45 Triangular.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
	long long int P = 0;
	long long int H = 0;
	long long int l = 0;
	for (long long int i = 1; i < 10E7; i++)
	{
		long long int T = i * (i + 1) / 2;				
		do
		{
			P = l * (3 * l - 1) / 2;			
			if (T == P)
			{
				long long int j = 0;
				do
				{
					j++;
					H = j * (2 * j - 1);
					if (H == T)					
						std::cout << i << " " << l << " " << j << " " << H << "\n";					
				} while (H < T);
			}
			l++;
		} while ( P < T );
		l--;
	}
}