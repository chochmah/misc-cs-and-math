/*
* File:   NFA.cpp
* Author: ceyhun
*
* Created on April 13, 2011, 5:40 PM
*/

#include "stdafx.h"

#include <stdlib.h>
#include <iostream>
#include <string>
#include <set>
#include <algorithm>

#include "NFA.h"

int NFA::stateIDsource = 0;

NFA::NFA() {
}

NFA::NFA(int initialState, vector<int> allStates, vector<int> acceptStates, vector<Edge> transitions) {
	this->initialState = initialState;
	this->allStates = allStates;
	this->acceptStates = acceptStates;
	this->transitions = transitions;
}

void NFA::print() {
	cout << "States : " << endl;
	printVector(allStates);
	cout << "Accepted States : " << endl;
	printVector(acceptStates);
	cout << "Transitions : " << endl;
	for (unsigned int i = 0; i<transitions.size(); ++i) {
		transitions[i].print();
	}
}

bool NFA::accepts(string str) {
	//checks whether this automaton accepts 's'. Implement this method
	//using given algorithm in project document.

	currentStates.clear();
	currentStates.push_back(initialState);
	for (string::iterator it = str.begin(); it != str.end(); ++it) {		
		vector<int> kleeneClosures;
		char c = *it;

		//Combine closures
		
		//std::cout << "before kleene: "; PrintVector(currentStates);
		for (vector<int>::iterator it = currentStates.begin(); it != currentStates.end(); ++it) {
			vector<int> kleeneClosure = epsilonClosure(*it);
			kleeneClosures.insert(kleeneClosures.end(), kleeneClosure.begin(), kleeneClosure.end());
		}
		kleeneClosures.push_back(initialState);

		//Find if Kleene Closure contains accept states
		for (vector<int>::iterator it = acceptStates.begin(); it != acceptStates.end(); ++it) {
			int state = *it;
			if (find(kleeneClosures.begin(), kleeneClosures.end(), state) != kleeneClosures.end()) {
				return true;
			}
		}

		//Recompute current states
		currentStates.clear();
		for (vector<Edge>::iterator it = transitions.begin(); it != transitions.end(); ++it) {
			Edge e = *it;
			if (e.getSymbol() == c) {
				for (vector<int>::iterator it = kleeneClosures.begin(); it != kleeneClosures.end(); ++it) {
					if (e.getSourceState() == *it) {
						currentStates.push_back(e.getDestinationState());
					}
				}
			}
		}

		for (vector<int>::iterator it = acceptStates.begin(); it != acceptStates.end(); ++it) {
			int state = *it;
			if (find(currentStates.begin(), currentStates.end(), *it) != currentStates.end()) {
				return true;
			}
		}
	}

	return false;
}

vector<int> NFA::epsilonClosure(int state) {
	//finds the set of states that are in epsilon closure of given 'state' within
	//this automaton. (set of states that can be reached from 'state' using
	//epsilon transitions)

	set<int> closure = { state };
	bool modified = true;
	while (modified) {
		modified = false;
		for (vector<Edge>::iterator it = transitions.begin(); it != transitions.end(); ++it) {
			Edge e = *it;
			if (e.isEpsilonTransition() && closure.find(e.getSourceState()) != closure.end() &&
				closure.find(e.getDestinationState()) == closure.end()) {

				modified = true;
				closure.insert(e.getDestinationState());
			}
		}
	}
	return vector<int>(closure.begin(), closure.end());
}

NFA NFA::singleSymbol(char c) {
	//implemented for you: singleSymbol

	int initialState = NFA::newState();
	int finalState = NFA::newState();

	//we have just two states here
	vector<int> allStates;
	allStates.push_back(initialState);
	allStates.push_back(finalState);

	//there is a single accept state,
	vector<int> acceptStates;
	acceptStates.push_back(finalState);

	//there is a single transition in this automaton, which takes initial state
	//to final state if input is given as the same as 'symbol' variable.
	Edge onlyOneTransition(initialState, finalState, c);
	vector<Edge> transitions;
	transitions.push_back(onlyOneTransition);

	//create an NFA with such properties.
	return NFA(initialState, allStates, acceptStates, transitions);
}

NFA NFA::unionOfNFAs(NFA& nfa1, NFA& nfa2) {

	//Create new initial state
	//int r_initialState = NFA::newState();

	//Remap Initial States for nfa2
	int r_initialState = nfa1.initialState;
	for (int i = 0; i < nfa2.transitions.size(); i++)
		if (nfa2.transitions[i].getSourceState() == nfa2.initialState)
			nfa2.transitions[i].setSourceState(r_initialState);

	// Merge state vectors
	vector<int> r_allStates;
	r_allStates.insert(r_allStates.end(), nfa1.allStates.begin(), nfa1.allStates.end());
	r_allStates.insert(r_allStates.end(), nfa2.allStates.begin(), nfa2.allStates.end());
	r_allStates.push_back(r_initialState);

	//Merge transitions
	vector<Edge> r_transitions;
	r_transitions.insert(r_transitions.end(), nfa1.transitions.begin(), nfa1.transitions.end());
	r_transitions.insert(r_transitions.end(), nfa2.transitions.begin(), nfa2.transitions.end());

	/*
	//Create and add new epsilon transitions
	Edge e1 = Edge(r_initialState, nfa1.initialState);
	Edge e2 = Edge(r_initialState, nfa2.initialState);
	r_transitions.push_back(e1);
	r_transitions.push_back(e2);
	*/

	//Merge accept states
	vector<int> r_acceptStates;
	r_acceptStates.insert(r_acceptStates.end(), nfa1.acceptStates.begin(), nfa1.acceptStates.end());
	r_acceptStates.insert(r_acceptStates.end(), nfa2.acceptStates.begin(), nfa2.acceptStates.end());

	return NFA(r_initialState, r_allStates, r_acceptStates, r_transitions);
}

NFA NFA::concatenate(NFA& nfa1, NFA& nfa2) {
	// Merge state vectors
	vector<int> r_allStates;
	r_allStates.insert(r_allStates.end(), nfa1.allStates.begin(), nfa1.allStates.end());
	r_allStates.insert(r_allStates.end(), nfa2.allStates.begin(), nfa2.allStates.end());

	//Remape initialState of NFA2 to sourceStates of NFA1
	for (int i = 0; i < nfa2.transitions.size(); i++)
	{
		if (nfa2.transitions[i].getSourceState() == nfa2.initialState)
			for (int l = 0; l < nfa1.acceptStates.size(); l++)
				if (l == 0)
					nfa2.transitions[i].setSourceState(nfa1.acceptStates[l]);
				else
					nfa2.transitions.push_back(Edge(nfa1.acceptStates[l], nfa2.transitions[i].getDestinationState(), nfa2.transitions[i].getSymbol()));
		if (nfa2.transitions[i].getDestinationState() == nfa2.initialState)
			for (int l = 0; l < nfa1.acceptStates.size(); l++)
				if (l == 0)
					nfa2.transitions[i].setDestinationState(nfa1.acceptStates[l]);
				else
					nfa2.transitions.push_back(Edge(nfa1.acceptStates[l], nfa2.transitions[i].getDestinationState(), nfa2.transitions[i].getSymbol()));
	}
	//TODO: check accepstates of nfa2
			
		

	//Merge transitions
	vector<Edge> r_transitions;
	r_transitions.insert(r_transitions.end(), nfa1.transitions.begin(), nfa1.transitions.end());
	r_transitions.insert(r_transitions.end(), nfa2.transitions.begin(), nfa2.transitions.end());

	/*
	//Add epsilon transitions to connect the two NFAs
	for (vector<int>::iterator it = nfa1.acceptStates.begin(); it != nfa1.acceptStates.end(); ++it) {
		Edge e = Edge(*it, nfa2.initialState);
		r_transitions.push_back(e);
	}
	*/

	return NFA(nfa1.initialState, r_allStates, nfa2.acceptStates, r_transitions);
}

NFA NFA::star(NFA& nfa) {

	int count = 0;
	for (vector<int>::iterator it = nfa.acceptStates.begin(); it != nfa.acceptStates.end(); ++it) {		
		for (int i=0;i<nfa.transitions.size();i++)
			if (nfa.transitions[i].getSourceState() == *it)				
				count++;			
		if (count) {
			Edge e = Edge(*it, nfa.initialState);
			nfa.transitions.push_back(e);
		}
		else 
		{		
			for (int i = 0; i < nfa.transitions.size(); i++)
				if (nfa.transitions[i].getDestinationState() == *it)
					nfa.transitions[i].setDestinationState(nfa.initialState);
		}
	}

	if (!count)	
		for (auto it2 = nfa.acceptStates.begin(); it2 != nfa.acceptStates.end();)
		{
			for (int l = 0; l < nfa.transitions.size(); l++)
				if (nfa.transitions[l].getDestinationState() == *it2)
				{
					++it2;
					continue;
				}		
			it2 = nfa.acceptStates.erase(it2);		
		}

	nfa.acceptStates.push_back(nfa.initialState);
	return nfa;
}

void printVector(const vector<int>& a) {
	cout << '[';
	for (unsigned int i = 0; i<a.size() - 1; ++i) {
		cout << a[i] << ", ";
	}
	cout << a.back() << ']' << endl;
}

void test() {
	cout << "Testing..." << endl;
	cout << endl;
	cout << "Trying to construct automaton that recognizes: " << endl;
	cout << "(a|b)*abb" << endl;

	NFA a = NFA::singleSymbol('a');	
	NFA b = NFA::singleSymbol('b');	
	NFA un = NFA::unionOfNFAs(a, b);	
	NFA star = NFA::star(un);		

	NFA anotherA = NFA::singleSymbol('a');	
	NFA anotherB = NFA::singleSymbol('b');
	NFA lastB = NFA::singleSymbol('b');

	NFA ab = NFA::concatenate(anotherA, anotherB);
	
	NFA abb = NFA::concatenate(ab, lastB);
	
	NFA result2 = NFA::concatenate(star, abb);	

	NFA a2 = NFA::singleSymbol('a');
	NFA b2 = NFA::singleSymbol('b');
	NFA un2 = NFA::unionOfNFAs(a2, b2);

	NFA star2 = NFA::star(un2);
	star2.print();
	NFA result = NFA::concatenate(result2, star2);

	result.print();

	string testString = "aaaaababaabb";

	if (result.accepts(testString)) {
		cout << "NFA accepts " << testString << endl;
	}
	else {
		cout << "NFA did not accept " << testString << endl;
		exit(-1);
	}

	cout << "All tests passed!" << endl;
	cout << "Here is the resulting NFA: " << endl;
	

}
