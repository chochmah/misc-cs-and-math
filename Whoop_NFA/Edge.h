/*
* File:   Edge.h
* Author: ceyhun
*
* Created on April 13, 2011, 6:32 PM
*/

#ifndef EDGE_H
#define	EDGE_H

#include <iostream>

using namespace std;

class Edge {

public:
	Edge();
	Edge(int, int, char);
	Edge(int, int);
	void print();

	inline int getSourceState() { return sourceState; }
	inline void setSourceState(const int state) { sourceState = state; }
	inline int getDestinationState() { return destinationState; }
	inline void setDestinationState(const int state) { destinationState = state; }
	inline char getSymbol() { return symbol; }

	inline bool isEpsilonTransition() { return symbol == EPSILON; }

private:
	int sourceState;
	int destinationState;
	char symbol;

	//instead of epsilon we have $, because epsilon is not in ascii table...
	static const char EPSILON = '$';

};

#endif	/* EDGE_H */
