/*
* File:   Edge.cpp
* Author: ceyhun
*
* Created on April 13, 2011, 6:32 PM
*/

#include "stdafx.h"

#include "Edge.h"

Edge::Edge() {
}

Edge::Edge(int source, int destination, char symbol) {
	this->sourceState = source;
	this->destinationState = destination;
	this->symbol = symbol;
}

Edge::Edge(int sourceState, int destinationState) {
	this->sourceState = sourceState;
	this->destinationState = destinationState;
	this->symbol = EPSILON;
}

void Edge::print() {
	cout << '(' << sourceState << ", " << destinationState << ", ";
	if (isEpsilonTransition()) {
		cout << "epsilon";
	}
	else {
		cout << "'" << symbol << "'";
	}
	cout << ')' << endl;
}
