/*
* File:   main.cpp
* Author: ceyhun
*
* Created on April 13, 2011, 5:40 PM
*/

#include <cstdlib>

#include "NFA.h"

#include "stdafx.h"

#include <stdio.h>
#include <iostream>

using namespace std;

// FD
void test();



static long int counter = 0;
static std::string key = ("TESTKEY321 99 11 Creeps in this petty pace from day to day");

static std::string EnCrypt2(const std::string enc)
{
	std::string result = "";

	for (int i = 0; i < enc.size(); i++)
	{
		char r = enc[i] ^ key[counter % ((key).length() / sizeof(char))];
		char c1 = (r & 0XF) + 0x61;
		char c2 = (r >> 4) + 0x61;
		result += c1 + c2;
		counter++;
	}

	return result;
}

static std::string DeCrypt2(const std::string enc)
{
	std::string result = "";

	for (int i = 0; i < enc.size(); i += 2)
	{
		char c1 = enc[i] - 0x61;
		char c2 = enc[i + 1] - 0x61;
		char r = c1 + (c2 << 4);
		//std::cout << (int)r << "	";
		r = r ^ key[(counter) % ((key).length() / sizeof(char))];
		//std::cout << (int)r << "	" << key[(counter) % (key.length() / sizeof(char))] << "\n";
		result += r;
		counter++;		
	}

	return result;
}

/*
*
*/
int main(int argc, char** argv) 
{
	
	std::cout << DeCrypt2("jdkchdhcegicocpfofobjehfpfpepbjenepc").c_str() << "\n";
	std::cout << DeCrypt2("bbdadagececbbfofhbgeoalacedbgbafabgbkbdejejfffcfdbefhbpfmfdbgfcf").c_str() << "\n";
	std::cout << DeCrypt2("ebpeabmbpajagbfbidkgcbggpaahbghaoheaihehghggghjgcghaccocmdddndjgbfncgbhdofocccibicgdndldmejbiehfhcmffhffccpfffmgmdhfcdibhejdogmclccdcggalbcbiapanbdgea").c_str() << "\n";

	//test();	
}

