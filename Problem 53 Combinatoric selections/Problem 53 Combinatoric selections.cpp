#include "pch.h"
#include <iostream>
#include <vector>

int main()
{
	size_t pyramid[105][105];
	bool large[105][105];
	for (int i = 0; i < 105; i++) 
		for (int l = 0; l < 105; l++)		
			pyramid[i][l] = 0;		

	// build pyramid, mark values in excess of one million
	pyramid[0][0] = 1;	

	for (int i = 1; i < 105; i++) {
		for (int l = 0; l < i; l++) {
			if (l == 0)		pyramid[i][0] = 1;
			if (l == i-1)	pyramid[i][i-1] = 1;
			if (large[i - 1][l - 1] || large[i - 1][l])
				large[i][l] = true;
			else
				if (pyramid[i - 1][l - 1] + pyramid[i - 1][l] > 1E6)
					large[i][l] = true;
				else
					large[i][l] = false;
			if (l != 0 && l != i - 1) pyramid[i][l] = pyramid[i - 1][l - 1] + pyramid[i - 1][l];
			
		}
	}

	int count = 0;
	for (int i = 0; i < 100+2; i++)
	{
		for (int l = 0; l < i; l++)
		{
			if (large[i][l])
				count++;
			if (large[i][l])
				std::cout << "x ";
			else
				std::cout << pyramid[i][l] << " ";			
		}		
		std::cout << "\n";
	}
	std::cout << "count = " << count << "\n";
}
