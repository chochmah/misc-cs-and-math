#include "pch.h"
#include <iostream>
#include <fstream>
#include <string>
#include <list>

int main()
{
	std::ifstream nameFileout;
	std::string s;
	nameFileout.open("p022_names.txt");
	std::getline(nameFileout, s);
	nameFileout.close();

	bool sw=1;
	std::list<std::string> names;
	std::string name;

	for (auto e : s) {
		if (e == '"') {
			if (!sw)			
				names.push_back(name);				
			else		
				name = "";
			sw = !sw;		
		}
		else
			name += e;
	}

	long int total = 0;
	long int count = 0;

	names.sort();
	for (auto e : names) {
		count++;
		int v = 0;
		for (auto c : e)
			v += c - 64;
		total += v * count;
		std::cout << e << " ";
	}

    std::cout << total; 
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
