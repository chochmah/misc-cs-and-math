#include <iostream>
#include <vector>

unsigned int calcKettenbruchentwicklung(const double d) {
	std::vector<double> alpha(1000);
	std::vector<double> c(1000);
	std::vector<double> b(1000);
	std::vector<double> a(1000);
	c[0] = 1;
	b[0] = 0;

	for (int i = 0; i < 999; i++) {		
		alpha[i] = (b[i] + sqrt(d)) / c[i];
		a[i] = floor(alpha[i]);
		b[i + 1] = a[i] * c[i] - b[i];
		c[i + 1] = (d - b[i + 1]* b[i + 1]) / c[i];

		for (int l = 1; l < i; l++) 
			if (alpha[i] == alpha[l])
			{
				std::cout << "sqrt("<<d<<") = [" << a[0] << ";(";
				for (int k = 1; k < i; k++) {
					std::cout << a[k];
					if (k!=i-1) std::cout << ",";
				}
				std::cout << ")] period = " << i -1 << "\n";

				return i - 1;
			}		
	}
	
	exit(1);
	return 0;
}


int main() {
	int count = 0;
	
	for (int i = 2; i <= 10000; i++)
		if (sqrt(i) != floor(sqrt(i)))
			if (calcKettenbruchentwicklung(i) % 2)
				count++;

	std::cout << count << "\n";
	getchar();
}