#include "pch.h"
#include <iostream>
#include <array>

const int d = 12000;

std::array< std::array<int, d + 1>, d + 1> field;

int main()
{
	for (int i = 1; i <= d; i++) {
		for (int l = 1; l <= i; l++) {
			field[i][l] = 0;
		}
	}

	int count = 0;
	for (int i = 1; i <= d; i++) {
		for (int l = 1; l <= i; l++) {
			if (field[i][l] == 0)
			{
				double r1 = (double)l / (double)i;
				if (r1 > 1.0f / 3.0f && r1 < 0.5f) {
					field[i][l] = 1;
					int r = 2;
					while (r*i <= d) {
						field[i*r][l*r] = 2;
						r++;
					}
					count++;
				}
			}
		}
	}
	/*
	for (int i = 1; i <= d; i++) {
		std::cout << i << "	";
		for (int l = 1; l <= i; l++) {
			std::cout << field[i][l] << " ";
		}
		std::cout << "\n";
	}*/

	std::cout << count <<"\n"; 
	std::cin;
}
