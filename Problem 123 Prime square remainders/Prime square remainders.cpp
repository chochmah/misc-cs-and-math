#include <iostream>
#include <vector>

std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound) {
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true) {
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}


int main(){
	/*
		solution is mostly analytical
		remainder is always 2*n*p_n
	*/
	CreatePrimeList(primes, 1E7);
	std::cout << primes.size() << " primes created \n";


	for (int k = 1; k < 1E7; k++)
	{
		unsigned long long int n = 2*k+1;
		unsigned long long int pn = primes[2*k +1 - 1];
		unsigned long long int solution = 2 * n*pn;		
		if (solution > 1E10) {
			std::cout << n << "	" << pn << "	solution " << solution << "\n";
			break;
		}
	}

	/*
	for (int k = 1; k < 100 + 1; k++)
	{
		int n = k;
		int pn = primes[k-1];

		unsigned long long int r1 = (pn - 1);
		for (int i = 0; i < n - 1; i++) {
			r1 = (r1 *(pn - 1)) % (pn*pn);
		}
		unsigned long long int r2 = (pn + 1);
		for (int i = 0; i < n - 1; i++) {
			r2 = (r2 *(pn + 1)) % (pn*pn);
		}

		unsigned long long int res = (r1 + r2) % (pn*pn);

		std::cout << "n=" << n << "	pn=" << pn << "	" << res << "\n";
	}
	*/
	getchar();
}