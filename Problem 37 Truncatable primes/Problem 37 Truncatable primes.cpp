// Problem 37 Truncatable primes.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>

#define maxp 10000

int sum = 0;

std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

void recursion(std::vector<int> n, int place)
{
	if (place >= n.size()) {		
		int val1 = 0;
		int val2 = 0;
		for (int k = 0; k < n.size(); k++) {			
			val1 = val1 * 10 + n[k];
			val2 = val2 + n[n.size()-1-k] * pow(10,k);

			if (!natNumbers[val1] || !natNumbers[val2])
				return;	
		}
		std::cout << val1<< "\n";
		sum += val1;
		return;
	}
		
	for (int a0 = 1; a0 < 10; a0++) {
		auto m = n;
		m[place] = a0;
		recursion(m, place + 1);
	}
}

int main()
{
	CreatePrimeList(primes, 1000000);
	for (int u = 2; u < 7; u++)
	{
		std::vector<int> beg(u, 1);
		recursion(beg, 0);
	}
	std::cout << "sum: " << sum << "\n";
}