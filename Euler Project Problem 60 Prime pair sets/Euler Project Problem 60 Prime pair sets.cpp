// Euler Project Problem 60 Prime pair sets.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>

const int			len				= 5;
const int			upperBound		= 9E7;
std::vector<int>	Mprimes;
std::vector<bool>	natNumbers(upperBound);

void CreatePrimeList(std::vector<int>& list)
{	
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	for (int i = 2; i < sqrt(upperBound); i++)	
		if (natNumbers[i] == true) {
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}
	
	for (int i = 2; i < upperBound; i++)	
		if (natNumbers[i]) list.push_back(i);	
}

void recursFkt(int counter, std::vector<int> input) {	
	if (input.size() > 1 && input.size() < len+1) {

		for (int m = 0; m < input.size(); m++) {
			for (int n = 0; n < input.size(); n++) {
				if (m != n) {
					std::string s = std::to_string(input[m]) + std::to_string(input[n]);
					int r = stoi(s);
					if (r > natNumbers.size())						
						exit(1);					
					if (!natNumbers[r])
						return;
				}
			}
		}
		if (input.size() == len) {
			int j = 0;
			for (auto o : input)
				std::cout << o << " ";
			for (auto o : input)
				j += o;			
			std::cout << " sum: " << j << "\n";
			exit(0);
		}
	}
	for (int i = 0; i < 1068; i++) {
		input.push_back(Mprimes[i]);
		recursFkt(counter, input);
		input.pop_back();
	}
}

int main() {
	CreatePrimeList(Mprimes);
	recursFkt(0, std::vector<int>(0));
}
