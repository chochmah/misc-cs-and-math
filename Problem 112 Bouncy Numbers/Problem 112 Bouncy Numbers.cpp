#include "pch.h"
#include <iostream>
#include <vector>
#include <string>

void BruteForce() {
	int c = 0;
	int n = 0;
	do {
		n++;
		std::string s = std::to_string(n);
		std::vector<int> v;
		for (int k = 0; k < s.size(); k++)
			v.push_back((int)s[k]);

		int count = 0;
		for (int k = 0; k < s.size() - 1; k++)
			if (v[k] <= v[k + 1])
				count++;
		if (count == s.size() - 1)
			c++;
		else {
			count = 0;
			for (int k = 0; k < s.size() - 1; k++)
				if (v[k] >= v[k + 1])
					count++;
			if (count == s.size() - 1)
				c++;
		}

	} while (n != c * 100);
	std::cout << n << " " << c << "\n";	
}

int main()
{
	BruteForce();
}
