#include <iostream>
#include <sstream>
#include <set>

int main() {
	// Brute Force approach 2
	std::set<unsigned long long int> nmrs;	
	for (int x = 2; x < 100; x++) {
		for (int y = 2; y < 100; y++) {
			unsigned long long int nmr = pow(x, y);
			unsigned long long int sum = 0;
			std::string s = std::to_string(nmr);
			for (unsigned char o : s)
				sum += o - 48;
			if (sum == x)
				nmrs.insert(nmr);				
		}
	}
	int count = 1;
	for (auto it = nmrs.begin(); it != nmrs.end(); ++it) {
		std::cout << count << "	" << *it << "\n";
		count++;
	}

	/* 
	// Brute Force approach 1
	long long int i = 11;
	int counter = 1;
	do {
		long long int sum = 0;
		std::string s = std::to_string(i);
		for (unsigned char o : s)
			sum += o - 48;
		long long int total = 1;
		
		long int cc = 0;
		do {
			total *= sum;
			cc++;
		} while (total>1 && total < i);
		
		if (total == i) {
			std::cout << counter << "	" << sum  << "^"<<cc << "	" << i << "\n";
			counter++;
		}
		i++;
	} while (counter < 31);
	*/
	getchar();
}