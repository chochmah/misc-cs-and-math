// Project Euler Problem 44 Pentagon Numbers.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
    for (int n=1;n<10E3;n++)
		for (int q = 1; q < n; q++) {
			double f1 = n * (3 * n - 1);
			double f2 = q * (3 * q - 1);
			double n1 = (1 + sqrt(1 + 12 * (f1 + f2))) / 6;
			double n2 = (1 + sqrt(1 + 12 * (f1 - f2))) / 6;
			if (floor(n1) == n1 && floor(n2) == n2)			
				std::cout << "Pj-Pk " << (long long int)(f1-f2)/2<< "\n";			
		}
}
