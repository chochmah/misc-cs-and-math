#include "pch.h"
#include <iostream>
#include <set>

double op(int o, double a, double b) {
	switch (o) {
	case 0: return a + b;
	case 1: return a - b;
	case 2: return a * b;
	case 3: {
		if (b == 0) std::cout << "ERROR" << "\n";
		return a / b;
	}
	}
}

int main()
{
	std::set<double> s;	

	int per[24][4] = {{0,1,2,3}, 
										{0,1,3,2}, 
										{0,2,1,3}, 
										{0,2,3,1}, 
										{0,3,1,2}, 
										{0,3,2,1}, 

										{1,0,2,3},
										{1,0,3,2},
										{1,2,0,3},
										{1,2,3,0},
										{1,3,0,2},
										{1,3,2,0},

										{2,0,1,3},
										{2,0,3,1},
										{2,1,0,3},
										{2,1,3,0},
										{2,3,0,1},
										{2,3,1,0},

										{3,0,2,1},
										{3,0,1,2},
										{3,2,0,1},
										{3,2,1,0},
										{3,1,0,2},
										{3,1,2,0}};
	
	
	for (int k0 = 1; k0 < 10; k0++) {
		for (int k1 = k0 + 1; k1 < 10; k1++) {
			for (int k2 = k1 + 1; k2 < 10; k2++) {
				for (int k3 = k2 + 1; k3 < 10; k3++) {
				
					int z[4] = { k0,k1,k2,k3};
					

					s.clear();

					for (int i = 0; i < 24; i++) {
						for (int i1 = 0; i1 < 4; i1++) {
							for (int i2 = 0; i2 < 4; i2++) {
								for (int i3 = 0; i3 < 4; i3++) {
									double sum = op(i1, z[per[i][0]], z[per[i][1]]);
									sum = op(i2, sum, z[per[i][2]]);
									sum = op(i3, sum, z[per[i][3]]);
									if (sum > 0 && std::floor(sum) == sum)
										s.insert(sum);

									sum = op(i1, z[per[i][0]], z[per[i][1]]);
									double sum2 = op(i2, z[per[i][2]], z[per[i][3]]);
									sum = op(i3, sum, sum2);
									if (sum > 0 && std::floor(sum) == sum)
										s.insert(sum);
								}
							}
						}
					}

					int maxnr=0;
					int lastnr = 0;
					int count = 1;
					for (auto e : s)
					{						
						if (e != count)
						{
							std::cout << "maxnr: " << lastnr << " size: ";							
							break;
						}
						lastnr = e;
						count++;
					}
					std::cout << s.size() << "	";
					std::cout << k0 << k1 << k2 << k3 << "\n";

					
				}
			}
		}
	}
	
}
