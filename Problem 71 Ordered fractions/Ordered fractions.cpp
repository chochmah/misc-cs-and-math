#include <iostream>
#include <vector>
#include <algorithm>

std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

bool reduced(const int x, const int y) {
	int i = 0;	
	do {		
		if (x % primes[i] == 0 && y % primes[i] == 0)
			return 0;
		i++;
	} while (primes[i] <= x / 2);
	return 1;
}


int main() {
	int dmax = 1E6;

	CreatePrimeList(primes, dmax);

	double lower = 2.0f / 5.0f;
	double upper = 3.0f / 7.0f;
	int nenner, zaehler;

	for (int d = 9; d <= dmax; d++) {
		double test_l = lower * d;
		double test_u = upper * d;	
		if (reduced(floor(test_u), d)) {
			if (floor(test_u) / d < upper && floor(test_u) / d > lower) {							
				nenner = floor(test_u);
				zaehler = d;				
				lower = floor(test_u) / d;
			}
		}		
	}
	
	std::cout << nenner << "//" << zaehler << "\n";
	getchar();
}