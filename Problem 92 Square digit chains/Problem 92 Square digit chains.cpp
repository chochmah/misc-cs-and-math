#include "pch.h"
#include <iostream>
#include <string>

int main() {
	long int count = 0;

	for (long int i = 1; i < 1E7; i++) {		
		long int r = i;
		
		do {			
			long int p = 0;
			std::string s = std::to_string(r);
			for (int k = 0; k < s.length(); k++)
				p += ((int)s[k] - 48)*((int)s[k] - 48);

			if (p == 1)
				break;
			if (p == 89)
			{
				count++;
				break;
			}
			r = p;
		} while (true);
	}
	std::cout << count << "\n";	
}