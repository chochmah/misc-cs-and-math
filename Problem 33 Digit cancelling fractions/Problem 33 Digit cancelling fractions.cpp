// Problem 33 Digit cancelling fractions.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
/*
	ba
	--
	ac
*/
	for (int a = 1; a < 10; a++)
		for (int b = 1; b < 10; b++)
			for (int c = 0; c < 10; c++)
				if (a!=b)
				if (double(b * 10 + a) / double(a * 10 + c) == double(b) / double(c))				
					std::cout << b << a << "/" << a << c << "\n";		
	// 16/64 * 26/65 * 19 / 95 * 49 / 98 = 1 / 100
}