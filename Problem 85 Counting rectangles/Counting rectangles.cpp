#include <iostream>

int main() {
	int saveSolution = 0;
	int savea, saveb;

	for (int a=1;a<2000;a++)
		for (int b = a; b < 2000; b++) {
			int solution = a * a*b*b + a * a*b + a * b*b + a * b + (0.25f - 1.0f)*a*b*(b + 1)*(a + 1);
			if (abs(2E6 - solution) <= abs(2E6 - saveSolution)) {				
				saveSolution = solution;
				savea = a;
				saveb = b;				
			}
		}

	std::cout << saveSolution << " " << savea * saveb << "\n";
	getchar();
}