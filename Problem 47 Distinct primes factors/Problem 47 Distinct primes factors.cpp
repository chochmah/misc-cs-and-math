/*
The first two consecutive numbers to have two distinct prime factors are :

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are :

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors each.What is the first of these numbers ? 
*/

#include "pch.h"
#include <iostream>
#include <vector>

void CreatePrimeList(std::vector<int>& list, const int upperBound) {
	std::vector<bool> natNumbers;
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true) {
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

unsigned int FactorizeNumber(unsigned int n, std::vector<int> &primes) {
	unsigned int count = 0;
	for (int i = 0; i < primes.size();i++) {
		bool first = true;
		do {
			if (n % primes[i] == 0) {
				n = n / primes[i];
				if (first) {
					count++;
					first = false;
				}
			}
			else break;
		} while (true);
		if (primes[i] > n)
			break;
	}
	return count;
}

int main() {
	// Brute Force approach	
	std::vector<int> primes;
	CreatePrimeList(primes, 1E6);
	
	unsigned int count = 0;

	for (int i = 2; i < 1E6; i++) {
		count = (FactorizeNumber(i, primes) == 4) ? count + 1 : 0;
		if (count == 4) {
			std::cout << i-3 << "\n";
			break;
		}
	}
}
