#include "pch.h"
#include <iostream>
#include <vector>
#include <set>

#define maxp 10000

int sum = 0;

std::vector<int> primes;
std::vector<bool> natNumbers;

void CreatePrimeList(std::vector<int>& list, const int upperBound)
{
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true)
		{
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

int main() {
	CreatePrimeList(primes, 10000);
	
	for (int i = 1000; i < 10000;i++) {		
		if (natNumbers[i]) {
			for (int l = 2; l < (10000 - i) / 2;l++) {
				if (natNumbers[i + l] && natNumbers[i + l * 2]) {
					std::set<int> s1,s2,s3;
					for (int k = 0; k < 4; k++) {
						s1.insert((i % (int)pow(10, k + 1) - i % (int)pow(10, k)) / (int)pow(10, k));
						s2.insert(((i + l) % (int)pow(10, k + 1) - (i + l) % (int)pow(10, k)) / (int)pow(10, k));
						s3.insert(((i + l * 2) % (int)pow(10, k + 1) - (i + l * 2) % (int)pow(10, k)) / (int)pow(10, k));
					}
					if (s1==s2 && s1 == s3)
						std::cout << i << " " << i+l << " " << i+l*2<<"\n";
				}
			}
		}
	}
}