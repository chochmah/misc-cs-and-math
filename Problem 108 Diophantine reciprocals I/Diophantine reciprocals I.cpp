#include <iostream>

int main() {	
	long int maxsolutions = 0;
	
	for (long long int n = 0; n < 293318625600+1; n = n+10) {
		long long int solutions = 0;
		for (long long int k = n + 1; k < n * n + n + 1; k++) {
			if (((n * n) % (k - n)) == 0) {
				long long int x = (n*n / (k - n)) + n;
				long long int y = k;
				if (x >= y)
				{
					solutions++;
					//std::cout << "1/" << x << " + 1/" << y << " = 1/" << n << "\n";
				}
				else
					break;
			}
		}

		if (solutions > maxsolutions) {
			maxsolutions = solutions;
			std::cout << n << "	" << maxsolutions << "\n";			
		}
	}

	std::cout << "solutions: " << maxsolutions << "\n";
	getchar();
}