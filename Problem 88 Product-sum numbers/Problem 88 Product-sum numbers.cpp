/*
	This took forever to solve. Found a non brute force approach that's more time and memory 
	consuming than the brute force approach from the euler website. 
*/

#include "pch.h"
#include <iostream>
#include <set>
#include <vector>

int minimum;

std::vector<std::vector<int>> prmfcs;

void primeFactors(int n, std::vector<int> &factors) {
	while (n % 2 == 0) {
		factors.push_back(2);
		n = n / 2;
	}
	for (int i = 3; i <= sqrt(n); i = i + 2) {
		while (n % i == 0) {
			factors.push_back(i);
			n = n / i;
		}
	}
	if (n > 2)
		factors.push_back(n);
}
int counter = 0;


bool recBinom(std::vector<int> nrs, std::vector<int> taken, int countDown,int total, int einsen)
{
	if (countDown > 0)
	{
		for (int i = 0; i < nrs.size(); i++)
		{
			std::vector<int> taken2 = taken;
			if (taken2[i] != 1)
			{
				taken2[i] = 1;
				if (recBinom(nrs, taken2, countDown - 1, total, einsen)) return 1;
				if (recBinom(nrs, taken, countDown - 1, total, einsen)) return 1;
			}
		}
	}
	else
	{
		int ad = 0;
		int mulfac = 1;
		int counter = 0;
		for (int i = 0; i < nrs.size(); i++)
		{
			if (!taken[i])
				ad += nrs[i];			
			else
			{
				mulfac *= nrs[i];
				counter++;
			}
		}
		ad += (einsen+counter-1) + mulfac;
		if (ad == total && ad < minimum)
		{
			int opcounter = 0;
			for (int i = 0; i < einsen + counter - 1; i++)
			{
				std::cout << "1 ";
				opcounter++;
			}

			for (int i = 0; i < nrs.size(); i++)
				if (!taken[i])
				{
					std::cout << nrs[i] << " ";
					opcounter += nrs[i];
				}
			std::cout << mulfac << " ";
			opcounter += mulfac;
			std::cout << "		" << opcounter << "\n";
			
			minimum = opcounter;

			//return 1;
		}
	}
	return 0;
}


void rec3(int k) {
	for (int u = k; u < 2 * k + 1; u++) {
		std::vector<int> fac;
		fac = prmfcs[u];
			
		int sum = k - fac.size();
		for (int i = 0; i < fac.size();i++)
			sum += fac[i];
		if (sum == u && sum < minimum)
		{
			int opcounter =0;
			
			for (int i = 0; i < k - fac.size(); i++)
			{
				std::cout << "1 ";
				opcounter++;
			}
				
			for (int i = 0; i < fac.size(); i++)
			{
				std::cout << fac[i] << " ";
				opcounter +=fac[i];
			}
			std::cout << "		" << opcounter << " " << sum << "\n";

			minimum = sum;

			return;
		}
		else
		{
			int remainder = u - sum;
			if (remainder > 0)
			{
				std::vector<int> taken(fac.size(), 0);
				if (recBinom(fac, taken, remainder + 1, u, k - fac.size()))
					return;
				// possiblenon trivial solution
				// fac.size() over remainder solutions;
				//std::cout << remainder << "\n";
			}
		}
	}
}

void PermutateVector(
	std::vector<char> infix, 
	std::vector<char> v, 
	std::vector<std::vector<char>>& result, 
	std::vector<std::vector<char>>& remainder) {
	if (infix.size() > 0) {
		result.push_back(infix);
		remainder.push_back(v);
	}

	for (int i = 0; i < v.size(); i++) {
		if (infix.size() == 0 || v[i] >= infix[infix.size() - 1])
		{
			auto newinfix = infix;
			newinfix.push_back(v[i]);

			std::vector<char> v2;
			for (int l = 0; l < v.size(); l++)
				if (i != l) 
					v2.push_back(v[l]);							

			PermutateVector(newinfix, v2, result,remainder);
		}
	}
}

int min;
std::vector<int> minsolution;



bool GeneratePermutationTable(
	std::vector<char> input, int depth,
	std::vector< std::vector<char>> result_vec,
	std::vector<std::vector<std::vector<char>>>& permutation_vec) 
{
	if (input.size() == 0) {
		permutation_vec.push_back(result_vec);
		return 1;
	}
	std::vector<std::vector<char>> ret, remainder;
	PermutateVector(std::vector<char>(0), input, ret, remainder);
	for (int i = 0; i < ret.size(); i++) {	
		if (result_vec.size() == 0 || ret[i] > result_vec[result_vec.size() - 1])
		{
			auto result_vec2 = result_vec;
			result_vec2.push_back(ret[i]);
			GeneratePermutationTable(remainder[i], depth + 1, result_vec2, permutation_vec);
		}
	}
	return 1;
}

std::vector< std::vector<std::vector<std::vector<char>>>> permutations;

bool calculate(std::vector<int>& pf, std::vector<std::vector<int>>& permutation_vec) {
	if (pf.size() > permutations.size()) {
		std::cout << "too big\n";
		exit(1);
	}

	auto& per_vec = permutations[pf.size()];

	for (int k = 0; k < per_vec.size(); k++) {
		std::vector<int> ret;
		for (int i = 0; i < per_vec[k].size(); i++) {
			int res = 1;
			for (int l = 0; l < per_vec[k][i].size(); l++) {
				res = res * pf[per_vec[k][i][l]-1];
			}
			ret.push_back(res);
		}
		permutation_vec.push_back(ret);
	}	
}


int main()
{
	/*
	{		
		{
			std::vector<int> pf{ 1,2,3 };
			std::vector<std::vector<std::vector<int>>> permutations;
			GeneratePermutationTable(pf, 0, std::vector<std::vector<int>>(), permutations);

			for (int i = 0; i < permutations.size(); i++) {
				for (int l = 0; l < permutations[i].size(); l++) {
					for (int k = 0; k < permutations[i][l].size(); k++)
						std::cout << permutations[i][l][k] << " ";
					std::cout << "|";
				}
				std::cout << "\n";
			}
			std::cout << "\n";
		}		
	}	
	return 1;
	*/
	
	std::vector<std::vector<std::vector<char>>> s_permutations;
	permutations.push_back(s_permutations);
	permutations.push_back(s_permutations);

	for (int k = 2; k < 14; k++) {
		std::vector<char> pf;
		for (int u = 0; u < k; u++)
			pf.push_back(u + 1);

		std::vector<std::vector<std::vector<char>>> s_permutations;
		GeneratePermutationTable(pf, 0, std::vector<std::vector<char>>(), s_permutations);
		permutations.push_back(s_permutations);
	}	

	for (int m = 0; m < permutations.size(); m++) {
		std::cout << permutations[m].size() << " ";
	}
	std::cout << "\n";
	
	
	/*
	for (int m = 0; m < permutations.size(); m++) {
		for (int i = 0; i < permutations[m].size(); i++) {
			for (int l = 0; l < permutations[m][i].size(); l++) {
				for (int k = 0; k < permutations[m][i][l].size(); k++)
					std::cout << (int)permutations[m][i][l][k] << " ";
				std::cout << "|";
			}
			std::cout << "\n";
		}
		std::cout << "------------\n";
	}
	
	return 1;
	*/

	/*
	{
		std::vector<int> pf{ 10,20,30 };
		std::vector<std::vector<int>> p;
		calculate(pf, p);		

		for (int i = 0; i < p.size(); i++) {
			for (int l = 0; l < p[i].size(); l++) {
				std::cout << p[i][l] << " ";
			}
			std::cout << "<\n";
		}
	}
	*/

	// Generate table with primefactors of 1..12000*2+1
	prmfcs.push_back(std::vector<int>(0));
	prmfcs.push_back(std::vector<int>(0));
	for (int i = 2; i < 12000 * 2 + 1; i++)
	{
		std::vector<int> fac;
		primeFactors(i, fac);
		prmfcs.push_back(fac);
	}

	std::set<int> total;

	for (int nmr = 12000; nmr>=2; nmr--)
	{

		std::vector<int> minlfvec;
		int minlf = nmr * 2 + 1;

		for (int k = nmr; k < nmr * 2 + 1; k++) {
			auto pf = prmfcs[k];
			if (pf.size() > 1) // Falls k prim keine Lösung
			{				
				std::vector<std::vector<int>> pers;
				calculate(pf, pers);
				for (int i = 0; i < pers.size(); i++)
				{
					int lf = 1;
					int rf = nmr - pers[i].size();		//off by one?
					for (int l = 0; l < pers[i].size(); l++) {
						lf = lf * pers[i][l];
						rf = rf + pers[i][l];
					}
					if (lf < minlf && lf == rf)
					{
						minlf = lf;
						minlfvec = pers[i];
						goto done;
					}
				}
			}
		}
		done:
		std::cout << nmr << ":	" << minlf << "	";
		for (int l = 0; l < minlfvec.size(); l++)
			std::cout << minlfvec[l] << " ";
		std::cout << "\n";

		total.insert(minlf);
	}

	long long int total_sum = 0;

	std::set<int>::iterator it = total.begin();
	while (it != total.end())
	{
		total_sum += *it;
		it++;
	}

	std::cout << total_sum << "\n";

	return 1;








	long long int sum = 0;

	for (int i = 2; i < 12+1; i++) {		
		//recursion1(std::vector<int>(i, 1), i*2+1);
	
		for (int i = 0; i < minsolution.size(); i++)
			std::cout << minsolution[i] << " ";
		std::cout << "\n";
	}

	return 1;

	//erster Versuch
	
	for (int k = 2; k <12+1; k++) {
		std::cout << "k=" << k << ":	";
		minimum = 2*k+1;
		rec3(k);
	}		
}
