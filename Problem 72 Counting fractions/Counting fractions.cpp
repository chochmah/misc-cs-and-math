#include <iostream>
#include <vector>
#include <tuple>

std::vector<bool> psive;
std::vector<int> plist;
std::vector<std::vector<int>> flist;


void CreatePrimeAndFactorList(std::vector<int>& primelist, std::vector<bool>& primesive, std::vector<std::vector<int>>& flist, const int upperBound) {
	primesive.resize(upperBound);
	flist.resize(upperBound);

	for (int i = 0; i < upperBound; i++)
		primesive[i] = true;
	primesive[0] = false;
	primesive[1] = false;
	for (int i = 2; i < (upperBound/2); i++)
		if (primesive[i] == true) {
			int count = i * 2;
			do {
				primesive[count] = false;
				flist[count].push_back(i);
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (primesive[i]) {
			primelist.push_back(i);
			flist[i].push_back(i);
		}
}

bool isReduceFraction(int n, int d) {
	for (int i = 0; i < flist[n].size(); i++) 
		if (std::find(flist[d].begin(), flist[d].end(), flist[n][i]) != flist[d].end())
			return false;
	return true;
}

int EulersTotientFunction(int n) {
	double ret = n;
	for (int i = 0; i < flist[n].size(); i++)
		ret = ret * (1.0f - 1.0f / (double)flist[n][i]);
	return (int) (ret);
}

std::tuple<int, int >ReduceFraction(int n, int d) {	
	for (int i = 0; i < flist[n].size();i++) {				
		if (std::find(flist[d].begin(), flist[d].end(), flist[n][i]) != flist[d].end()) {			
			auto n2 = n / flist[n][i];
			auto d2 = d / flist[n][i];			
			return ReduceFraction(n2, d2);
		}
	}
	return std::make_tuple(n, d);
}

int main() {	
	int max = 1000000+1;
	CreatePrimeAndFactorList(plist, psive, flist, max);
	
	long long int count = 0;
	for (int i = 2; i < max; i++)
		count += EulersTotientFunction(i);

	std::cout << count << "\n";
	getchar();
}