#include <stdio.h>
#include <iostream>

#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeIntegerClass.h"
#include "..\..\..\..\..\MagentaCLOUD\BitBucket\LargeIntegerClass\LargeIntegerClass\LargeIntegerLibrary\LargeFloatClass.h"


int main() {
	typedef intLarge<uint64_t> IntL;

	int sum = 0;

	for (IntL D = 2; D < 100; D = D + 1) {
		if (IntL::root(D)*IntL::root(D) != D) {			
			auto q = IntL::root(D * IntL::pow(10, 256));
			auto  s = q.AbsoluteConvertToVectorDigits();

			for (int i = s.size() - 1; i >= s.size() - 100; i--)
				sum += s[i];
		}
	}

	std::cout << sum << "\n";
	getchar();
}