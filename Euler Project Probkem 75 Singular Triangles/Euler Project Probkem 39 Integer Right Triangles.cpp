// Euler Project Probkem 75 Singular Triangles.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>


#define max(x,y) (x>y) ? x : y

const int mVal = 1000;

int main()
{
	std::vector<int> L(mVal,0);
	
	int count = 0;

	for (long int i = 1; i <= mVal; i++)
	{
		if (i % 1000==0) std::cout << i << "	" << count << "\n";
		for (long int l = max(1,(double)(1.0/sqrt(2)*i)); l < i; l++)
		{
			if (i + l> mVal)
				break;
			double t = sqrt(i*i - l * l);
			
			if (std::floor(t) && std::ceil(t) == t)
			{
				if (t>0 && i + l + t <= mVal)
				{
					L[i + l + (int)t]++;
					std::cout << i << " " << l << " " << (int)t << "	" << i + l + (int)t << "\n";
				}
			}
		}
	}

	int max=0;


	for (int i = 0; i < L.size(); i++)
	{
		int e = L[i];
		if (e > max)
		{
			std::cout <<i << " "<< e << "\n";
			max = e;
		}
	}
			
	std::cout << count << " <<\n";
}
