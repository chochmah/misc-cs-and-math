#include <stdio.h>
#include <iostream>

/*
	calc all right angled triangles with circumference below 1.500.000 and sort them into buckets 
	count buckets with one member

	Very slow (hours?) but works!
	Todo: Find a much faster solution!
*/

int main() {
	int total = 0;
	for (uint64_t i = 5; i < 1500000; i++) {
		int count = 0;
		for (uint64_t a = 1; a < ceil((double)i/2); a++) {
			//for (uint64_t b=1; b< a;b++) {
			double b = (2.0*(double)i*(double)a - (double)i*(double)i) / (2.0*(double)a - 2.0 * (double)i);
			if (a < b && b>0 && floor(b) == b)
			{
				uint64_t c = i-a-b;				
				if (a*a + b*b == c*c)
				{
					if (count != 0) goto multiples;
					//std::cout << i << "	" << a << " " << b << " " << c << "\n";
					count++;
				}				
			}
		}
		if (count == 1)
			total++;
	multiples:
		int u = 0;
	}

	std::cout << "total: " << total << "\n";

	getchar();
}