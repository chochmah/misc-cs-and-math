#include <iostream>
#include <vector>
#include <sstream>

std::vector<int> primes, primes2;
std::vector<bool> natNumbers;
int total = 0;

void CreatePrimeList(std::vector<int>& list, const int upperBound) {
	natNumbers.resize(upperBound);
	for (int i = 0; i < upperBound; i++)
		natNumbers[i] = true;
	natNumbers[0] = false;
	natNumbers[1] = false;
	for (int i = 2; i < sqrt(upperBound); i++)
		if (natNumbers[i] == true) {
			int count = i * 2;
			do {
				natNumbers[count] = false;
				count += i;
			} while (count < upperBound);
		}

	for (int i = 2; i < upperBound; i++)
		if (natNumbers[i]) list.push_back(i);
}

void recursion(int index, std::vector<int> s) {
	bool ziffern[10] = {0,0,0,0,0,0,0,0,0,0};
	for (int i = 0; i < s.size(); i++) {
		std::string st = std::to_string(s[i]);
		for (int k = 0; k < st.size(); k++) {
			ziffern[st[k] - 48] = 1;
		}
	}

	int count = 0;
	for (int i = 1; i < 10; i++) 
		if (ziffern[i]) count++;
	if (count == 9) {
		//for (int i = 0; i < s.size(); i++)
		//	std::cout << s[i] << " ";
		//std::cout << "\n";
		total++;
	}


	for (int i = index; i < primes.size(); i++) {
		std::vector<int> new_s = s;	

		std::string st = std::to_string(primes[i]);
		if (count + st.size() > 9)
			break;

		for (int k = 0; k < st.size(); k++) {
			if (ziffern[st[k] - 48] == 1)
				goto next;
		}		
		new_s.push_back(primes[i]);
		recursion(i + 1, new_s);
	next:
		int u = 0;
	}
}

int main() {	
	// create prime list
	CreatePrimeList(primes2, 100000000);
	std::cout << "Size of Prime List: " << primes2.size() << "\n";
	
	// clear prime list of primes with recurring digits and primes with zeros.
	for (int i = 0; i < primes2.size(); i++) {
		bool ziffern[10] = { 1,0,0,0,0,0,0,0,0,0 };
		std::string st = std::to_string(primes2[i]);
		for (int k = 0; k < st.size(); k++) {
			if (ziffern[st[k] - 48] == 1) 
				goto nxt;
			ziffern[st[k] - 48] = 1;
		}
		primes.push_back(primes2[i]);
	nxt:		
		int u = 0;
	}
	std::cout << "Size of filtered Prime List: " << primes.size() << "\n";

	// find admissible prime sets
	recursion(0, std::vector<int>());

	std::cout << "Sets found: " << total << "\n";
	getchar();
}