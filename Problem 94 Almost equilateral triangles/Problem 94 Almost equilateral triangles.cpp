
#include "pch.h"
#include <iostream>

typedef long long unsigned int lint;
const lint max_circum = 1000000000;

int main() {	
	lint total_circumference=0;	
	lint triagle_circum, m, n;

	n = 1;		
	do {
		m = n + 1;
		do {
			lint a = 1 * (m*m - n * n);
			lint b = 1 * (2 * m * n);
			lint c = 1 * (m*m + n * n);				
			triagle_circum = 2 * a + 2 * c;

			if (2 * a == c - 1 || 2 * a == c + 1)
			{
				lint triagle_circum2 = 2 * a + 2 * c;
				if (triagle_circum2 < max_circum)
				{
					total_circumference += triagle_circum2;
					std::cout << " | " << a << " " << c << " " << triagle_circum2 << "\n";
				}
			}

			if (2 * b == c - 1 || 2 * b == c + 1)
			{
				lint triagle_circum2 = 2 * b + 2 * c;
				if (triagle_circum2 < max_circum)
				{
					total_circumference += triagle_circum2;
					std::cout << " X " << b << " " << c << " " << triagle_circum2 << "\n";
				}
			}

			m++;
		} while (triagle_circum < max_circum);
		n++;			
	} while (!(m==n+2 && triagle_circum >= max_circum)); // not like this
		
	std::cout << total_circumference << "\n";
}