#include <iostream>

inline long long int m_mul(const long long int x, const long long int y) {
	return (x * y) % (long long int)1E8;
}

inline long long int m_pow(long long int x, long long int y) {		
	long long int z=1;

	do {
		if (y % 2) {
			z = m_mul(z, x);
			y--;
		} else {
			x = m_mul(x, x);
			y = y / 2;
		}		
	} while (y > 0);
	return z;
}

long long int hyper_pow(long long int x, const long long int y) {	
	long long int m = x;
	for (int i = 1; i < y; i++)		
		m = m_pow(x, m);			
	return m;
}

int main()
{
	//auto y = m_pow(3, 3);

	auto y = hyper_pow(1777,1855);
	std::cout << y << "\n";

	std::getchar();
}