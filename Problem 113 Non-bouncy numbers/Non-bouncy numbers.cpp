#include <iostream>

int count = 0;

void recursion1(int stelle, int n, int maxsize) {
	if (stelle == maxsize) {  
		count++;
		return;
	}	
	for (int i = n; i < 10; i++) {
		recursion1(stelle + 1, i, maxsize);
	}
}

void recursion2(int stelle, int n, int maxsize) {
	if (stelle == maxsize) {
		count++;
		return;
	}
	for (int i = 0; i <=n; i++) {
		recursion2(stelle + 1, i, maxsize);
	}
}


int main() {	
	
	double l = 1;
	double count2 = 0;
	double count3 = 0;

	for (int i = 1; i <= 10; i++) {
		double q = 11 - i;
		std::cout << l*i << "*" << q << "\n";
		count2 += l*i * q;
	}
	count2 -= 1;
	std::cout << count2 << "\n";
	std::cout << "\n";
	for (int i = 1; i <= 10; i++) {
		double q = 12 - i;
		std::cout << l * i << "*" << q << "\n";
		count3 += l * i * q;
	}
	count3 += 9-2;
	std::cout << count3 << "\n";
	getchar();
	
	std::cout << count2 + count3 - 9*l << "\n";

	getchar();
}