// Euler Project Problem 24 Lexicographic Permut.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
	int i, s;
	int x[9] = {1,2,3,4,5,6,7,8,9 };
	int len = 9;

	for (int u = 0; u < 362880-1; u++) {//9!-1 // 3628800-1 
		//place +
		for (int i = 1; i < 8; i++) {
			//place = 
			for (int l = i + 1; l < 9; l++) {
				int m1 = 0;
				for (int k = 0; k < i; k++)
					m1 += x[k] * pow(10, i - k - 1);
				int m2 = 0;
				for (int k = i; k < l; k++)
					m2 += x[k] * pow(10, l - k-1);				
				int m3 = 0;
				for (int k = l; k < 9; k++)
					m3 += x[k] * pow(10, 9 - k-1);

				if (m1 * m2 == m3) {
					std::cout << i << " " << l << "		";
					std::cout << m1 << " * " << m2 << " = " << m3 << "\n";
				}
			}
		}

		for (s = len - 2; s >= 0; s--)
			if (x[s] < x[s + 1])
				break;

		int m = s + 1;
		for (i = s + 1; i < len; i++)
			if (x[i] > x[s] && x[i] < x[m])
				m = i;
		std::swap(x[s], x[m]);

		//sort
		for (int k = s + 1; k < len - 1; k++)
			for (int q = s + 1; q < len - 1; q++)
				if (x[q] > x[q + 1])
					std::swap(x[q], x[q + 1]);
	}
}
