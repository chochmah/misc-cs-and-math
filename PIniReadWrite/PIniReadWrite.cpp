#include "PIniReadWrite.h"

#include <stdexcept>
#include <iostream>
#include <fstream>


namespace PIniRW
{	
	//----------------------------------------------------------------------------
	PIniReadWrite::PIniReadWrite(std::string fileName_)
	{
		ini.SetUnicode();		
		fileName = std::pair<std::string, bool>(fileName_, true);
		ini.LoadFile(fileName.first.c_str());
	}

	//----------------------------------------------------------------------------
	PIniReadWrite::PIniReadWrite()
	{
		ini.SetUnicode();
		fileName = std::pair<std::string, bool>("", false);
	}

	//----------------------------------------------------------------------------
	void PIniReadWrite::SetFileName(std::string fileName_)
	{
		fileName = std::pair<std::string, bool>(fileName_, true);
		ini.LoadFile(fileName.first.c_str());
	}

	//----------------------------------------------------------------------------
	std::string PIniReadWrite::ReadString(std::string b, std::string c, std::string def) const
	{		
		return(ini.GetValue(b.c_str(), c.c_str(), def.c_str()));
	}
	
	bool PIniReadWrite::WriteString(std::string Section, std::string keyName, std::string key)
	{
		ini.SetValue(Section.c_str(), keyName.c_str(), key.c_str());
		ini.SaveFile(fileName.first.c_str());
		return true;
	}

	//----------------------------------------------------------------------------
	int PIniReadWrite::ReadInteger(std::string b, std::string c, int def) const
	{		
		return std::stoi(ReadString(b,c,std::to_string(def)));
	}

	bool PIniReadWrite::WriteInteger(std::string b, std::string c, int a)
	{
		ini.SaveFile(fileName.first.c_str());
		auto ret = WriteString(b,c, std::to_string(a));
		return ret;
	}

	//----------------------------------------------------------------------------
	double PIniReadWrite::ReadFloat(std::string b, std::string c, double def) const
	{
		return std::stoi(ReadString(b, c, std::to_string(def)));
	}

	bool PIniReadWrite::WriteFloat(std::string b, std::string c, double a)
	{
		return WriteString(b, c, std::to_string(a));
	}

	//----------------------------------------------------------------------------
	bool PIniReadWrite::ReadBoolean(std::string b, std::string c, bool def) const
	{
		return std::stoi(ReadString(b, c, std::to_string(def)));
	}

	bool PIniReadWrite::WriteBoolean(std::string b, std::string c, bool a)
	{
		return WriteString(b, c, std::to_string(a));
	}


}