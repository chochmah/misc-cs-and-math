/*
	Small and simple threadsave windows Ini-file ReadWrite Library

	Peterhans Hendel 2019
*/

#pragma once

#include <string>

#include "SimpleIni.h"

namespace PIniRW
{
	//----------------------------------------------------------------------------
	class PIniReadWrite
	{
	private:
		//----------------------------------------------------------------------------		
		std::pair<std::string, bool>	fileName;		
		CSimpleIniA ini;

	public:
		PIniReadWrite(std::string fileName);
		PIniReadWrite();
		void SetFileName(std::string);


		bool WriteString	(std::string, std::string, std::string);
		bool WriteInteger	(std::string, std::string, int);
		bool WriteFloat		(std::string, std::string, double);
		bool WriteBoolean	(std::string, std::string, bool);

		
		std::string ReadString	(std::string, std::string, std::string) const;
		bool		ReadBoolean	(std::string, std::string, bool) const;
		int			ReadInteger	(std::string, std::string, int) const;
		double		ReadFloat	(std::string, std::string, double) const;
	};
}
